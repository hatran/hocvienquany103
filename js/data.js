var data = [
 {
   "STT": 1,
   "Name": "BỆNH VIỆN QUÂN Y 103",
   "address": "Nguyễn Quyền, xã Võ Cường, thành phố Bắc Ninh, tỉnh Bắc Ninh",
   "Longtitude": 21.166744,
   "Latitude": 106.065994
 },
 {
   "STT": 2,
   "Name": "Bệnh viện Y học cổ truyền và Phục Hồi Chức Năng Bắc Ninh",
   "address": "Phường Thị Cầu,  thành phố Bắc Ninh, tỉnh Bắc Ninh",
   "Longtitude": 21.1904795,
   "Latitude": 106.0781833
 },
 {
   "STT": 3,
   "Name": "Bệnh viện Lao và bệnh Phổi tỉnh Bắc Ninh",
   "address": "Thanh Phương, phường Vũ Ninh, thành phố Bắc Ninh, tỉnh Bắc Ninh",
   "Longtitude": 21.1839,
   "Latitude": 106.055999
 },
 {
   "STT": 4,
   "Name": "Bệnh viện Mắt tỉnh Bắc Ninh",
   "address": "Phường Hạp Lĩnh, thành phố Bắc Ninh, tỉnh Bắc Ninh",
   "Longtitude": 21.132873,
   "Latitude": 106.076494
 },
 {
   "STT": 5,
   "Name": "Bệnh viện Phong và Da liễu Bắc Ninh",
   "address": "Quả Cảm, xã Hoà Long, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.208104,
   "Latitude": 106.051051
 },
 {
   "STT": 6,
   "Name": "Bệnh viện Tâm thần Bắc Ninh",
   "address": "Đồi Búp Lê khu 1, phường Thị Cầu, thành phố Bắc Ninh, tỉnh Bắc Ninh",
   "Longtitude": 21.189194,
   "Latitude": 106.08702
 },
 {
   "STT": 7,
   "Name": "Bệnh viện Sản - Nhi Bắc Ninh",
   "address": "Huyền Quang, phường Đại Phúc, thành phố Bắc Ninh, tỉnh Bắc Ninh",
   "Longtitude": 21.169213,
   "Latitude": 106.068426
 },
 {
   "STT": 8,
   "Name": "Bệnh viện Đa khoa huyện Gia Bình",
   "address": "Xã Gia Định, huyện Gia Bình, tỉnh Bắc Ninh",
   "Longtitude": 21.053063,
   "Latitude": 106.181588
 },
 {
   "STT": 9,
   "Name": "Bệnh viện Đa khoa huyện Tiên Du",
   "address": "Đường tỉnh 276, xã Lũng Sơn, huyện Tiên Du, tỉnh Bắc Ninh",
   "Longtitude": 21.139532,
   "Latitude": 106.021639
 },
 {
   "STT": 10,
   "Name": "Bệnh viện Đa khoa huyện Yên Phong",
   "address": "Ngân Cầu, thị trấn Chờ, huyện Yên Phong, tỉnh Bắc Ninh ",
   "Longtitude": 21.199102,
   "Latitude": 105.953945
 },
 {
   "STT": 11,
   "Name": "Bệnh viện Đa khoa huyện Quế Võ",
   "address": "Quốc lộ 18 phố Mới, xã Phố Mới, huyện Quế Võ, tỉnh Bắc Ninh",
   "Longtitude": 21.154474,
   "Latitude": 106.159329
 },
 {
   "STT": 12,
   "Name": "Bệnh viện Đa khoa thị xã Từ Sơn",
   "address": "Minh Khai, khu đô thị Đồng Nguyên, xã Đồng Nguyên, thị xã Từ Sơn, tỉnh Bắc Ninh",
   "Longtitude": 21.123982,
   "Latitude": 105.972164
 },
 {
   "STT": 13,
   "Name": "Bệnh viện Đa khoa huyện Thuận Thành",
   "address": "Xã Gia Đông, huyện Thuận Thành, tỉnh Bắc Ninh",
   "Longtitude": 21.037309,
   "Latitude": 106.086937
 },
 {
   "STT": 14,
   "Name": "Bệnh viện Đa khoa huyện Lương Tài",
   "address": "Phượng Giáo, thị trấn Thứa, huyện Lương Tài, tỉnh Bắc Ninh",
   "Longtitude": 21.023428,
   "Latitude": 106.208115
 },
 {
   "STT": 15,
   "Name": "Trung tâm Kiểm soát bệnh tật tỉnh",
   "address": "Đường Nguyễn Quyền, phường Võ Cường,  thành phố Bắc Ninh, tỉnh Bắc Ninh",
   "Longtitude": 21.166296,
   "Latitude": 106.067997
 },
 {
   "STT": 16,
   "Name": "Trung tâm Y tế thành phố Bắc Ninh",
   "address": "Đường Lê Phụng Hiểu, phường Vệ An,  thành phố Bắc Ninh, tỉnh Bắc Ninh\n\n",
   "Longtitude": 21.183901,
   "Latitude": 106.056
 },
 {
   "STT": 17,
   "Name": "Trung tâm Y tế huyện Lương Tài",
   "address": "Thị trấn Thứa, huyện Lương Tài, tỉnh Bắc Ninh",
   "Longtitude": 21.0234276,
   "Latitude": 106.2081247
 },
 {
   "STT": 18,
   "Name": "Trung tâm Y tế huyện Thuận Thành",
   "address": "Lạc Long Quân, xã Gia Đông, huyện Thuận Thành, tỉnh Bắc Ninh",
   "Longtitude": 21.0371045,
   "Latitude": 106.0873549
 },
 {
   "STT": 19,
   "Name": "Trung tâm Y tế huyện Gia Bình",
   "address": "Đông Bình, xã Xuân Lai, huyện Gia Bình, tỉnh Bắc Ninh",
   "Longtitude": 21.0552264,
   "Latitude": 106.186552
 },
 {
   "STT": 20,
   "Name": "Trung tâm Y tế thị xã Từ Sơn",
   "address": "KP Hoàng Quốc Việt, phường Đông Ngàn, thị xã Từ Sơn, tỉnh Bắc Ninh",
   "Longtitude": 21.1171637,
   "Latitude": 105.9631924
 },
 {
   "STT": 21,
   "Name": "Trung tâm Y tế huyện Tiên Du",
   "address": "113 Nguyễn Đăng Đạo, Thị Trấn Lim, Huyện Tiên Du, tỉnh Bắc Ninh",
   "Longtitude": 21.1393728,
   "Latitude": 106.0215318
 },
 {
   "STT": 22,
   "Name": "Trung tâm Y tế huyện Yên Phong",
   "address": "Đường Không Tên, thị trấn Chờ, huyện Yên Phong, tỉnh Bắc Ninh",
   "Longtitude": 21.1995367,
   "Latitude": 105.9553295
 },
 {
   "STT": 23,
   "Name": "Trung tâm Y tế huyện Quế Võ",
   "address": "Thị trấn Phố Mới, huyện Quế Võ, tỉnh Bắc Ninh",
   "Longtitude": 21.157279,
   "Latitude": 106.151517
 },
 {
   "STT": 24,
   "Name": "Bệnh viện mắt Sông Cầu",
   "address": "143 đường Hoàng Quốc Việt, phường Thị Cầu, thành phố Bắc Ninh, tỉnh Bắc Ninh\n\n",
   "Longtitude": 21.193715,
   "Latitude": 106.085282
 },
 {
   "STT": 25,
   "Name": "Bệnh viện Đa khoa tư nhân Kinh Bắc",
   "address": "310 Trần Hưng Đạo, phường Đại Phúc,  thành phố Bắc Ninh, tỉnh Bắc Ninh",
   "Longtitude": 21.1667239,
   "Latitude": 106.0658549
 },
 {
   "STT": 26,
   "Name": "Bệnh viện Đa khoa Thành An Thăng Long",
   "address": "144 Nguyễn Văn Cừ, phường Ninh Xá,  thành phố Bắc Ninh, tỉnh Bắc Ninh",
   "Longtitude": 21.1745833,
   "Latitude": 106.054348
 }
];