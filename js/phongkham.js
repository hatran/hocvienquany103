var dataphongkham = [
 {
   "STT": 1,
   "Name": "Phòng khám Đa khoa Tâm Phúc",
   "address": "Số 99 Ngô Tất Tố, Kinh Bắc, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1747034,
   "Latitude": 106.0578334
 },
 {
   "STT": 2,
   "Name": "Phòng khám Đa khoa Nhân Đức",
   "address": "Khu Lâm Làng, phường Vân Dương, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.14966,
   "Latitude": 106.1037006
 },
 {
   "STT": 3,
   "Name": "Phòng khám Đa khoa Thuận An",
   "address": "Thôn Cầu Đào, xã Nhân Thắng, huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0714056,
   "Latitude": 106.2261624
 },
 {
   "STT": 4,
   "Name": "Phòng khám Đa khoa Tuấn Thành",
   "address": "Châu Cầu, thị trấn Phố Mới, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1205135,
   "Latitude": 106.2500076
 },
 {
   "STT": 5,
   "Name": "Phòng khám Đa khoa Thiện Nhân",
   "address": "Thị trấn Phố Mới, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1534803,
   "Latitude": 106.1536828
 },
 {
   "STT": 6,
   "Name": "Phòng khám Đa khoa Ngã Tư Hồ",
   "address": "Thị trấn Hồ, huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0568543,
   "Latitude": 106.0904721
 },
 {
   "STT": 7,
   "Name": "Phòng khám Đa khoa Tâm Đức",
   "address": "Phố Dâu, huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0437995,
   "Latitude": 106.0757749
 },
 {
   "STT": 8,
   "Name": "Phòng khám Đa khoa Hà Nội - Bắc Ninh",
   "address": "Đường 295, xã Hoàn Sơn, huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.1975926,
   "Latitude": 105.9555758
 },
 {
   "STT": 9,
   "Name": "Phòng khám Đa khoa Kinh Bắc - Hà Nội",
   "address": "Số 9 lô 14, Khu công nghiệp Tân Hồng Hoàn Sơn, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1132376,
   "Latitude": 105.979979
 },
 {
   "STT": 10,
   "Name": "Phòng khám Đa khoa Phúc An",
   "address": "Phường Đình Bảng, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.0994022,
   "Latitude": 105.9494248
 },
 {
   "STT": 11,
   "Name": "Phòng khám Đa khoa Khu công nghiệp Yên Phong",
   "address": "Lô CN20 Khu công nghiệp Yên Phong 1 , huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.1931173,
   "Latitude": 106.0077533
 },
 {
   "STT": 12,
   "Name": "Phòng khám Đa khoa Quang Việt",
   "address": "Ngã 4 Bưu điện, phố Chờ, trị trấn Chờ, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.196205,
   "Latitude": 105.956298
 },
 {
   "STT": 13,
   "Name": "Phòng chẩn trị Y học cổ truyền - Công ty TNHH Y Dược TAVATA",
   "address": "Thông Roi Sóc, xã Phù Chẩn, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1196529,
   "Latitude": 105.9623161
 },
 {
   "STT": 14,
   "Name": "Phòng khám Đa khoa Khu công nghiệp Quế Võ",
   "address": "Thôn Thái Bảo, xã Nam Sơn, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1407887,
   "Latitude": 106.093297
 },
 {
   "STT": 15,
   "Name": "Phòng khám Chuyên khoa Sản - Bác sĩ Sức",
   "address": "Đường Bình Than, khu Khả Lễ, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1610722,
   "Latitude": 106.054341
 },
 {
   "STT": 16,
   "Name": "Phòng khám Đa khoa - Trung tâm Y tế  Lương Tài",
   "address": "Thị trấn Thứa, huyện Lương Tài, Bắc Ninh",
   "Longtitude": 21.0222876,
   "Latitude": 106.2080897
 },
 {
   "STT": 17,
   "Name": "Phòng chẩn trị Y học cổ truyền - Bác sĩ Thịnh",
   "address": "Thôn Đông Du Núi, xã Đào Viên, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1184411,
   "Latitude": 106.1905816
 },
 {
   "STT": 18,
   "Name": "Phòng chẩn trị Y học cổ truyền Phúc Tâm Đường",
   "address": "Thôn Mao Dộc, xã Phượng Mao, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1543426,
   "Latitude": 106.1367842
 },
 {
   "STT": 19,
   "Name": "Phòng khám Chuyên khoa Phục hồi chức năng An Sinh",
   "address": "Khu phố Cao Lâm, phường Đình Bảng, thị xã Từ Sơn,Bắc Ninh",
   "Longtitude": 21.1106794,
   "Latitude": 105.9509991
 },
 {
   "STT": 20,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Việt Đức",
   "address": "Thôn Hoài Thượng, xã Liên Bảo, huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.1302752,
   "Latitude": 106.0245772
 },
 {
   "STT": 21,
   "Name": "Phòng khám Đa khoa 108A",
   "address": "Số 302, phố Đông Côi, thị trấn Hồ, huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0378323,
   "Latitude": 106.0919988
 },
 {
   "STT": 22,
   "Name": "Phòng khám Đa khoa Tuệ Tĩnh",
   "address": "Đường TS 5, KCN Tiên Sơn, xã Hoài Sơn, huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.1212343,
   "Latitude": 105.9856062
 },
 {
   "STT": 23,
   "Name": "Phòng khám Đa khoa Trọng Tôn",
   "address": "24 Trần Lựu, Khu 5, phường Thị Cầu, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1968462,
   "Latitude": 106.0893896
 },
 {
   "STT": 24,
   "Name": "Phòng khám Đa khoa Thành Đô",
   "address": "Số 248, đường Trần Hưng Đạo, phường Đại Phúc, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1790489,
   "Latitude": 106.0687277
 },
 {
   "STT": 25,
   "Name": "Phòng khám Đa khoa Hoàn Mỹ",
   "address": "469 Nguyễn Trãi, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1673239,
   "Latitude": 106.0646226
 },
 {
   "STT": 26,
   "Name": "Phòng khám Đa khoa Nhân Đức",
   "address": "Khu Lãm Làng, phường Vân Dương, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1511312,
   "Latitude": 106.1044863
 },
 {
   "STT": 27,
   "Name": "Phòng khám Đa khoa Bắc Hà",
   "address": "Số 294 đường nguyễn Trãi, Khu Bồ Sơn, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1714336,
   "Latitude": 106.0623647
 },
 {
   "STT": 28,
   "Name": "Phòng khám Đa khoa Tư nhân Bình Thắng",
   "address": "144, Phố Mới, thị trấn Chờ, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.1917744,
   "Latitude": 105.9560966
 },
 {
   "STT": 29,
   "Name": "Phòng khám Đa khoa Khu công nghiệp Yên Phong",
   "address": "Khu công nghiệp ,Yên Phong, Bắc Ninh",
   "Longtitude": 21.2045781,
   "Latitude": 105.9533501
 },
 {
   "STT": 30,
   "Name": "Phòng khám Đa khoa Y cao Hà Nội II",
   "address": "Đường 286 Thị trấn Chờ, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1958719,
   "Latitude": 105.9490464
 },
 {
   "STT": 31,
   "Name": "Phòng khám Đa khoa Hữu Phúc",
   "address": "Thông Đông Yên, Xã Đông Phong, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.1883842,
   "Latitude": 106.0103857
 },
 {
   "STT": 32,
   "Name": "Phòng khám Đa khoa Tâm An",
   "address": "thôn Đông Yên, xã Đông Phong, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.1897272,
   "Latitude": 106.0116806
 },
 {
   "STT": 33,
   "Name": "Phòng khám Đa khoa Phong Hoa",
   "address": "Thị Phố Mới, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1534803,
   "Latitude": 106.1536828
 },
 {
   "STT": 34,
   "Name": "Phòng khám Đa khoa Hữu Phúc 2",
   "address": "Giang Liễu, Phương Liễu, Quế Võ, Bắc Ninh",
   "Longtitude": 21.1618475,
   "Latitude": 106.1272201
 },
 {
   "STT": 35,
   "Name": "Phòng khám Đa khoa An Khang",
   "address": "Khu chợ Trung tâm, thị trấn Phố Mới, Quế Võ, Bắc Ninh",
   "Longtitude": 21.1518977,
   "Latitude": 106.1501206
 },
 {
   "STT": 36,
   "Name": "Phòng khám Đa khoa Trí Đức",
   "address": "Đông Sơn, xã Việt Đoàn, huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.1178311,
   "Latitude": 106.0287512
 },
 {
   "STT": 37,
   "Name": "Phòng khám Đa khoa Y Cao Hà Nội III",
   "address": "số nhà 134 đường Hai Bà Trưng, thị trấn Lim, huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.1446713,
   "Latitude": 106.021467
 },
 {
   "STT": 38,
   "Name": "Phòng khám Đa khoa 132",
   "address": "Phù Lộc, xã Phù Chẩn, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.0855597,
   "Latitude": 105.9729255
 },
 {
   "STT": 39,
   "Name": "Phòng khám Đa khoa tư nhân Phố Viềng",
   "address": "Phố Viềng, phường Đồng Nguyên, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1200532,
   "Latitude": 105.9625736
 },
 {
   "STT": 40,
   "Name": "Phòng khám Đa khoa Việt Đức",
   "address": "Đường Lê Quang Đạo, phường Đông Ngàn, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1150942,
   "Latitude": 105.9581418
 },
 {
   "STT": 41,
   "Name": "Phòng khám Đa khoa 108 A",
   "address": "địa chỉ số 302, phố Đông Côi, thị trấn Hồ, huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0403912,
   "Latitude": 106.0926556
 },
 {
   "STT": 42,
   "Name": "Phòng khám Đa khoa Tâm Đức ",
   "address": "Phố Dâu, Thanh Khương, Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0356025,
   "Latitude": 106.0405485
 },
 {
   "STT": 43,
   "Name": "Phòng khám Đa khoa tư nhân Y cao Hà Nội",
   "address": "Khu chợ Nội cũ thị trấnGia Bình, Huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0527482,
   "Latitude": 106.1746859
 },
 {
   "STT": 44,
   "Name": "Phòng khám Đa khoa ",
   "address": "Đường Gia Định, thị trấn Gia Bình, huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0540878,
   "Latitude": 106.1786787
 },
 {
   "STT": 45,
   "Name": "Phòng khám Đa khoa Bạch Mai",
   "address": "thị trấn Thứa, huyện Lương Tài, Bắc Ninh",
   "Longtitude": 21.0222876,
   "Latitude": 106.2080897
 },
 {
   "STT": 46,
   "Name": "Phòng khám đa khoa - Trung tâm kiểm soát bệnh tật tỉnh Bắc Ninh",
   "address": "phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1644992,
   "Latitude": 106.0552006
 },
 {
   "STT": 47,
   "Name": "Phòng khám Chuyên khoa Răng",
   "address": " 74 Lê Văn Thịnh, phường Suối Hoa, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1835241,
   "Latitude": 106.0728676
 },
 {
   "STT": 48,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "566 đường Nguyễn Trãi, Bò Sơn, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1665083,
   "Latitude": 106.0645306
 },
 {
   "STT": 49,
   "Name": "Phòng Chẩn trị Y học cổ truyền Thiên Chương",
   "address": "69 Bà Chúa Kho, phường Vũ Ninh,thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.198316,
   "Latitude": 106.083179
 },
 {
   "STT": 50,
   "Name": "Phòng khám Bác sĩ Hà Thị Nhã",
   "address": "159 Thiên Đức, phường Vệ An,thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1867607,
   "Latitude": 106.0595193
 },
 {
   "STT": 51,
   "Name": "Phòng khám Chuyên khoa Răng",
   "address": "số 4, Nguyễn Du, phường Ninh Xá,thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1786862,
   "Latitude": 106.0584783
 },
 {
   "STT": 52,
   "Name": "Phòng khám Chuyên khoa Sản",
   "address": "Số 90,Ng. Đăng, phường Suối Hoa,thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1831983,
   "Latitude": 106.0698741
 },
 {
   "STT": 53,
   "Name": "Phòng khám Chuyên khoa da liễu",
   "address": "626 Thiên Đức, phường Vạn An,thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.190416,
   "Latitude": 106.0548366
 },
 {
   "STT": 54,
   "Name": "Phòng khám Chuyên khoa ",
   "address": "247 Nguyễn văn Cừ,phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1725918,
   "Latitude": 106.0525757
 },
 {
   "STT": 55,
   "Name": "Phòng khám Chuyên khoa Nội (Ngoài giờ )",
   "address": "Xuân Ổ B, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1594781,
   "Latitude": 106.0397713
 },
 {
   "STT": 56,
   "Name": "Phòng khám Chuyên khoa ngoại thẩm mỹ - Ngoài giờ",
   "address": "540 Nguyễn Trãi, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1663188,
   "Latitude": 106.0647583
 },
 {
   "STT": 57,
   "Name": "Phòng khám Chuyên khoa Nội tổng hợp - Ngoài giờ ",
   "address": "72A Thành Bắc, phường Ninh Xá,thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1783393,
   "Latitude": 106.056599
 },
 {
   "STT": 58,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "541, phường Đáp Cầu,thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.2010943,
   "Latitude": 106.0955955
 },
 {
   "STT": 59,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "47 Trần Lựu, phường Thị Cầu, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1969127,
   "Latitude": 106.0887704
 },
 {
   "STT": 60,
   "Name": "Dịch vụ Y tế chăm sóc răng miệng",
   "address": "189 Lý Thường Kiệt, phường Thị Cầu,thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.203773,
   "Latitude": 106.0906671
 },
 {
   "STT": 61,
   "Name": "Phòng khám Chuyên khoa Răng hàm mặt - Ngoài giờ ",
   "address": "353 Nguyễn văn Cừ, khu Hòa Đình, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1701233,
   "Latitude": 106.0492987
 },
 {
   "STT": 62,
   "Name": "Phòng khám tư nhân răng",
   "address": "496 Nguyễn Trãi, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1673393,
   "Latitude": 106.0643828
 },
 {
   "STT": 63,
   "Name": "Phòng khám tư nhân nhi khoa",
   "address": "54A, Khu 10, phường Đại Phúc, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1739477,
   "Latitude": 106.0693764
 },
 {
   "STT": 64,
   "Name": "Phòng khám tư nhân Tai mũi họng ",
   "address": "32 Nguyễn Cao, phường Ninh Xá, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1785891,
   "Latitude": 106.0651912
 },
 {
   "STT": 65,
   "Name": "Phòng khám Chuyên khoa Nội - Ngoài giờ ",
   "address": "302 Lý Thường Kiệt, Khu 6 , phường Thị Cầu, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.2042469,
   "Latitude": 106.0908232
 },
 {
   "STT": 66,
   "Name": "Phòng khám Chuyên khoa Nhi - Ngoài giờ ",
   "address": "181 Lý Thái Tông, phường Suối Hoa, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1810054,
   "Latitude": 106.0756105
 },
 {
   "STT": 67,
   "Name": "Phòng khám Chuyên khoa phụ sản - Ngoài giờ ",
   "address": "98 Nguyễn văn Cừ, phường Ninh Xá, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1752445,
   "Latitude": 106.0555108
 },
 {
   "STT": 68,
   "Name": "Phòng khám Chuyên khoa Nhi (Ngoài giờ)",
   "address": "Số 4 Nguyễn Chiêu Huấn, phường Tiền An, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1815277,
   "Latitude": 106.0662223
 },
 {
   "STT": 69,
   "Name": "Phòng khám sản khoa (Ngoài giờ)",
   "address": "261 Trần Hưng Đạo, phường Tiền An, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.17938,
   "Latitude": 106.068239
 },
 {
   "STT": 70,
   "Name": "Cơ sở dịch vụ y tế chăm sóc nha khoa",
   "address": "Xuân Ổ A, khu Hòa Đình, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1644992,
   "Latitude": 106.0552006
 },
 {
   "STT": 71,
   "Name": "Phòng khám tư nhân sản phụ khoa cộng đồng",
   "address": "Số 22, Lý Nhân Tông,Khu đô thị HUD, phường Ninh Xá, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1721897,
   "Latitude": 106.0578971
 },
 {
   "STT": 72,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "168 đường Lê Phụng Hiển, P, Vệ An, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1856211,
   "Latitude": 106.0528259
 },
 {
   "STT": 73,
   "Name": "Phòng khám chuyên khoa Răng",
   "address": "5 Trần Hưng Đạo, phường Tiền An, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1814633,
   "Latitude": 106.063725
 },
 {
   "STT": 74,
   "Name": "Phòng khám tư nhân Chuyên khoa Mắt(Ngoài giờ hành chính )",
   "address": "26 Ninh Xá 3, phường Ninh Xá,thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1768169,
   "Latitude": 106.0625483
 },
 {
   "STT": 75,
   "Name": "Phòng Chẩn trị Y học cổ truyền(Ngoài giờ )",
   "address": "183 Lý Thái Tông,phường Suối Hoa, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1809354,
   "Latitude": 106.0755676
 },
 {
   "STT": 76,
   "Name": "Phòng khám tư nhân Tai mũi họng ",
   "address": "79 Trần Lựu, phường Thị Cầu, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1969914,
   "Latitude": 106.0918049
 },
 {
   "STT": 77,
   "Name": "Phòng khám Chuyên khoa Răng hàm mặt - Toàn Thục Ngoài giờ",
   "address": "173 Hoàng Quốc Việt, phường Thị Cầu, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1935436,
   "Latitude": 106.0870863
 },
 {
   "STT": 78,
   "Name": "Phòng Y tế cơ quản lý Bất động sản Sông Hồng",
   "address": "lô 1.5 h, xã Nam Sơn, Khu công nghiệp Quế Võ, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1581144,
   "Latitude": 106.114287
 },
 {
   "STT": 79,
   "Name": "Phòng khám Chuyên khoa Nha khoa Thục Toàn Vệ An",
   "address": "Số 154 đường Lê Phụng Hiểu, phường Vệ An, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1843295,
   "Latitude": 106.0554113
 },
 {
   "STT": 80,
   "Name": "Phòng khám Chuyên khoa Răng hàm mặt Ngoài giờ",
   "address": "97A Suối Hoa, phường Vũ Ninh, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1831983,
   "Latitude": 106.0698741
 },
 {
   "STT": 81,
   "Name": "Phòng khám Chuyên khoa Nội (Ngoài giờ )",
   "address": "341 Trần Hưng Đạo, phường Đại Phúc, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1786624,
   "Latitude": 106.0699109
 },
 {
   "STT": 82,
   "Name": "Phòng khám Chuyên khoa Nội Việt Tiệp - Ngoài giờ (h chính )",
   "address": "Sơn Đông, xã Nam Sơn, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.141189,
   "Latitude": 106.0930395
 },
 {
   "STT": 83,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "442 Nguyễn Trãi, Bò Sơn, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1684546,
   "Latitude": 106.0638486
 },
 {
   "STT": 84,
   "Name": "Phòng khám Chuyên khoa Tai mũi họng ",
   "address": "Số 24, tổ 3, Khu Hòa Đình, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1684776,
   "Latitude": 106.0496172
 },
 {
   "STT": 85,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "89 Nguyễn Du, Ninh Xá, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1784776,
   "Latitude": 106.0586904
 },
 {
   "STT": 86,
   "Name": "Phòng khám Chuyên khoa XQ - SA - XN",
   "address": "164 đường Vũ Ninh, Vũ Ninh, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1904816,
   "Latitude": 106.0786656
 },
 {
   "STT": 87,
   "Name": "Cơ sở dịch vụ Y tế chăm sóc Răng",
   "address": "Phố Và, phường Hạp Lĩnh, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.142413,
   "Latitude": 106.0736783
 },
 {
   "STT": 88,
   "Name": "Phòng khám Nha Khoa Hà Nội",
   "address": "388 Nguyễn Trãi, phường Võ Cường , thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1695317,
   "Latitude": 106.0633462
 },
 {
   "STT": 89,
   "Name": "Phòng khám Chuyên khoa Phụ sản Anh Khoa - Ngoài giờ",
   "address": "114 đường Bình Than, phường Đại Phúc, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1707721,
   "Latitude": 106.0744541
 },
 {
   "STT": 90,
   "Name": "Phòng khám Chuyên khoa Sản 108 - Ngoài giờ",
   "address": "550 Nguyễn Trãi, Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1659736,
   "Latitude": 106.0648817
 },
 {
   "STT": 91,
   "Name": "Cơ sở Cơ sở Nha khoa - Hàng ngày",
   "address": "Số 7, Khu 10, phường Đáp Cầu, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.2026299,
   "Latitude": 106.0948815
 },
 {
   "STT": 92,
   "Name": "Cơ sở chăm sóc Nha khoa Tâm Đức",
   "address": "Xuân Ổ A, khu Hòa Đình, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1644992,
   "Latitude": 106.0552006
 },
 {
   "STT": 93,
   "Name": "Phòng khám Chuyên khoa Nội Chí Thành",
   "address": "492 Nguyễn Trãi, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1674093,
   "Latitude": 106.0642988
 },
 {
   "STT": 94,
   "Name": "Cơ sở Dịch vụ Y tế Nha khoa",
   "address": "287 Thiên Đức, phường Vệ An, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1881056,
   "Latitude": 106.057665
 },
 {
   "STT": 95,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt( ngoài giờ)",
   "address": " đường Âu Cơ, Kính Bắc,thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1901857,
   "Latitude": 106.0671081
 },
 {
   "STT": 96,
   "Name": "Phòng khám Chuyên khoa Mắt ( ngoài giờ)",
   "address": "165 Lê Phụng Hiểu, Niềm Xá, Kinh Bắc, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1808111,
   "Latitude": 106.0568861
 },
 {
   "STT": 97,
   "Name": "Phòng khám Chuyên khoa Mắt ( ngoài giờ)",
   "address": "Đường 286, phường Vạn An, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1940365,
   "Latitude": 106.046699
 },
 {
   "STT": 98,
   "Name": "Cơ sở chăm sóc Nha khoa ngoài công lập Ngô Thị Dung",
   "address": "110 Bình Than, Khu 3, phường Đại Phúc, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1708464,
   "Latitude": 106.0745273
 },
 {
   "STT": 99,
   "Name": "Phòng khám Răng hàm mặt Bác sĩ Ngô Quang Trung",
   "address": "thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1781766,
   "Latitude": 106.0710255
 },
 {
   "STT": 100,
   "Name": "Bệnh xá Công an tỉnh Bắc Ninh",
   "address": "Đường Nguyễn Văn Cừ, phường Ninh Xá, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1764823,
   "Latitude": 106.0570669
 },
 {
   "STT": 101,
   "Name": "Phòng khám Chuyên khoa Răng hàm mặt ( hàng ngày)",
   "address": "239 Lý Thường Kiệt, Khu 6, phường Thị Cầu, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.2042564,
   "Latitude": 106.0908266
 },
 {
   "STT": 102,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Lê Thái Tổ, khu Bồ Sơn, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1663635,
   "Latitude": 106.0490278
 },
 {
   "STT": 103,
   "Name": "Phòng Chẩn trị Y học cổ truyền - Hội Đông Y tỉnh",
   "address": "Số 215 Ngô Gia Tự, phường Suối Hoa, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1885611,
   "Latitude": 106.0728306
 },
 {
   "STT": 104,
   "Name": "Cơ sở Dịch vụ Kính Thuốc - Huệ",
   "address": "Số 20ngox 459 Hoàng Quốc Việt, Khu 10 Đáp Cầu, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1994723,
   "Latitude": 106.0946161
 },
 {
   "STT": 105,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Đường 286 phường Vạn An, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1940365,
   "Latitude": 106.046699
 },
 {
   "STT": 106,
   "Name": "Cơ sở Cơ sở Răng - Miệng",
   "address": "573 Trần Hưng Đạo, phường Tiền An, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1789516,
   "Latitude": 106.0690012
 },
 {
   "STT": 107,
   "Name": "Phòng khám Chuyên khoa Răng hàm mặt Nguyễn Tuyến",
   "address": "92-94 Nguyễn Trãi, phường Ninh Xá, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1761034,
   "Latitude": 106.0599947
 },
 {
   "STT": 108,
   "Name": "Phòng khám nha khoa Huy Hoàng",
   "address": "170 đường Bình Than, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1675366,
   "Latitude": 106.065336
 },
 {
   "STT": 109,
   "Name": "Phòng khám Chuyên khoa Tai mũi họng ",
   "address": "Đường Võ Cường 3, khu Bồ Sơn, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1644992,
   "Latitude": 106.0552006
 },
 {
   "STT": 110,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "360A Hoàng Quốc Việt, phường Thị Cầu, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1954853,
   "Latitude": 106.0926165
 },
 {
   "STT": 111,
   "Name": "Phòng khám Răng giả Quang Huy",
   "address": "Phố Và, phường Hạp Lĩnh, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.142413,
   "Latitude": 106.0736783
 },
 {
   "STT": 112,
   "Name": "Phòng khám Chuyên khoa Nội Đức Thành Long",
   "address": "51B đường Huyền Quang, phường Ninh Xá, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.176019,
   "Latitude": 106.0641528
 },
 {
   "STT": 113,
   "Name": "Phòng khám Chuyên khoa Nội số I Vũ Ninh",
   "address": "Xóm Đền, Thanh Sơn, phường Vũ Ninh, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.2058178,
   "Latitude": 106.0844887
 },
 {
   "STT": 114,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Số 492 Nguyễn Trãi, Bò Sơn, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1674093,
   "Latitude": 106.0642988
 },
 {
   "STT": 115,
   "Name": "Phòng khám Chuyên khoa nội",
   "address": "140 Phố Vũ, phường Đại Phúc, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1738006,
   "Latitude": 106.0787142
 },
 {
   "STT": 116,
   "Name": "Cơ sở Cơ sở Răng - Miệng Thục Toàn II",
   "address": "Phố Ba Huyện, phường Khắc Niệm, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1314843,
   "Latitude": 106.0580066
 },
 {
   "STT": 117,
   "Name": "Cơ sở chăm sóc Răng miệng",
   "address": "135A Hoàng Quốc Việt, Khu I phường Thị Cầu, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1915806,
   "Latitude": 106.0861355
 },
 {
   "STT": 118,
   "Name": "Phòng khám Chuyên khoa Sản - Phụ khoa Bác sĩ Ly",
   "address": "Kim Đôi, Kim Chân, thành phố Bắc Ninh , Bắc Ninh",
   "Longtitude": 21.1988132,
   "Latitude": 106.1117851
 },
 {
   "STT": 119,
   "Name": "Phòng khám Chuyên khoa Phụ - Sản 56",
   "address": "Số 56 Bình Than, phường Đại Phúc, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1720816,
   "Latitude": 106.0746824
 },
 {
   "STT": 120,
   "Name": "Cơ sở Dịch vụ Răng giả Hàm giả Việt phong",
   "address": "Số 459 Nguyễn Trãi, Bồ Sơn, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1680486,
   "Latitude": 106.0641141
 },
 {
   "STT": 121,
   "Name": "Phòng Chẩn trị Y học cổ truyền Bắc Hà",
   "address": "Số 58 đường bình than phường Võ Cường Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1719293,
   "Latitude": 106.074775
 },
 {
   "STT": 122,
   "Name": "Phòng khám Chuyên khoa Phụ sản Minh Thúy",
   "address": "Số 60 đường KB 58, phường Kinh Bắc, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1881139,
   "Latitude": 106.0654875
 },
 {
   "STT": 123,
   "Name": "Trạm Y tế Công ty Cổ phần - Tổng công ty may Đáp Cầu",
   "address": "Khu VI, phường Thị Cầu thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1969959,
   "Latitude": 106.0904471
 },
 {
   "STT": 124,
   "Name": "Công ty TNHH MEDLATEC",
   "address": "Khu Khả Lễ, Đường Bình Than, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1644992,
   "Latitude": 106.0552006
 },
 {
   "STT": 125,
   "Name": "Phòng khám Chuyên khoa Nội Hữu Phúc 3 - Siêu âm - Xét nghiệm",
   "address": "Thôn Sơn Đông, xã Nam Sơn, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1330408,
   "Latitude": 106.0735704
 },
 {
   "STT": 126,
   "Name": "Phòng khám Chuyên khoa Nội - Siêu âm",
   "address": "Thôn Quả Cảm, xã Hòa Long, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.207329,
   "Latitude": 106.0523457
 },
 {
   "STT": 127,
   "Name": "Phòng khám Chuyên khoa Răng hàm mặt Thu Thái",
   "address": "Thôn Sơn Đông, xã Nam Sơn, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1330408,
   "Latitude": 106.0735704
 },
 {
   "STT": 128,
   "Name": "Phòng khám Chuyên khoa Răng hàm mặt",
   "address": "Số 170 đường Lê Phụ Hiểu , phường Vệ an, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1828931,
   "Latitude": 106.0596092
 },
 {
   "STT": 129,
   "Name": "Cơ sở Dịch vụ Y tế Làm răng giả",
   "address": "Đường kinh Dương Vương, phường Vũ Ninh, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1859333,
   "Latitude": 106.0788366
 },
 {
   "STT": 130,
   "Name": "Phòng khám Chuyên khoa Phụ sản - KHHGĐ",
   "address": "Phố và, phường Hạp Lĩnh, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.142413,
   "Latitude": 106.0736783
 },
 {
   "STT": 131,
   "Name": "Phòng khám Chuyên khoa răng hàm mặt EDS",
   "address": "Số 498 đường Nguyễn Trãi, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1708975,
   "Latitude": 106.0626898
 },
 {
   "STT": 132,
   "Name": "Phòng khám Chuyên khoa Nội Bùi Đình Tú",
   "address": "Số 09 Trần Lựu, khu 5, phường Thị Cầu, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1967503,
   "Latitude": 106.0879328
 },
 {
   "STT": 133,
   "Name": "Phòng khám Chuyên khoa Tai - Mũi - Họng Thành Lê",
   "address": "Đường 286, khu Yên Mẫn, phường Kinh Bắc, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1892614,
   "Latitude": 106.0590399
 },
 {
   "STT": 134,
   "Name": "Phòng khám chẩn đoán hình ảnh Y cao Bắc Ninh",
   "address": "Số nhà 560 đường Nguyễn Trãi, phường Võ cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.128636,
   "Latitude": 106.0763849
 },
 {
   "STT": 135,
   "Name": "Phòng khám Chuyên khoa Da Liễu Thu Cúc",
   "address": "địa chỉ tầng 3 số nhà 115, đường Nguyễn Gia Thiều, phường Suối Hoa, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1848996,
   "Latitude": 106.0710503
 },
 {
   "STT": 136,
   "Name": "Phòng khám Chuyên khoa Nhi Bác sĩ Kiên",
   "address": "Địa chỉ tầng 1, số nhà 170, đường Bình Than, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1675366,
   "Latitude": 106.065336
 },
 {
   "STT": 137,
   "Name": "Phòng khám Chuyên khoa Răng hàm mặt Thanh Vân",
   "address": "Địa chỉ tầng 1, số nhà 145, đường Hồ Ngọc Lân, phường Kinh Bắc, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1876024,
   "Latitude": 106.0651918
 },
 {
   "STT": 138,
   "Name": "Phòng khám Chuyên khoa Răng hàm mặt Thịnh Đức",
   "address": "73 Nguyễn Cao, phường Ninh Xá, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1768787,
   "Latitude": 106.0628485
 },
 {
   "STT": 139,
   "Name": "Phòng khám Chuyên khoa Tai mũi họng Bác sĩ Tiến",
   "address": "số nhà 512, đường Nguyễn Trãi, phường Bồ Sơn, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1684759,
   "Latitude": 106.0639373
 },
 {
   "STT": 140,
   "Name": "Phòng khám Chuyên khoa U bướu Hạnh Phúc",
   "address": "Khu đa cấu, phường Nam Sơn, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1424416,
   "Latitude": 106.1183999
 },
 {
   "STT": 141,
   "Name": "Phòng khám Chuyên khoa Nội Duy Long",
   "address": "Khu đa cấu, phường Nam Sơn, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1424416,
   "Latitude": 106.1183999
 },
 {
   "STT": 142,
   "Name": "Phòng khám Chuyên khoa Mắt Bác sĩ Hằng",
   "address": "Khu Tiên Xá, phường Hạp Lĩnh, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1391877,
   "Latitude": 106.0753369
 },
 {
   "STT": 143,
   "Name": "Phòng khám Chuyên khoa Nhi Bác sĩ Tuấn ",
   "address": "Số 50 đường Bình Than, phường Đại Phúc, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1720789,
   "Latitude": 106.0747194
 },
 {
   "STT": 144,
   "Name": "Phòng khám Chuyên khoa Phụ sản 58",
   "address": "Số 58 đường Trần Lựu, Khu 5, phường Thị Cầu, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1672299,
   "Latitude": 106.0774283
 },
 {
   "STT": 145,
   "Name": "Phòng khám Chuyên khoa răng hàm mặt Thục Toàn Vệ An",
   "address": "154 đường Lê Phụng Hiểu, phường Vệ An, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1843295,
   "Latitude": 106.0554113
 },
 {
   "STT": 146,
   "Name": "Medlatec Bắc Ninh",
   "address": "Đường Bình Than, khu Khã Lễ, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.167262,
   "Latitude": 106.0645425
 },
 {
   "STT": 147,
   "Name": "Phòng khám Chuyên khoa Nội 108 Bắc Ninh",
   "address": "Khu đô thị Nam Võ Cường, phường Võ CƯờng, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1626069,
   "Latitude": 106.0654275
 },
 {
   "STT": 148,
   "Name": "Phòng khám Bác sỹ Huy Tùng",
   "address": "Đường Đại Phúc 2, khu 10, phường Đại Phúc, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1624028,
   "Latitude": 106.0805514
 },
 {
   "STT": 149,
   "Name": "Phòng khám Chuyên khoa Tai mũi họng Đan Nhi",
   "address": "Phố Nguyễn Tự Cường, phường Kinh Bắc, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1888172,
   "Latitude": 106.0638342
 },
 {
   "STT": 150,
   "Name": "Phòng khám Metalab",
   "address": "179 Nguyễn Văn Cừ, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1734923,
   "Latitude": 106.0538782
 },
 {
   "STT": 151,
   "Name": "Phòng khám Chuyên khoa Phụ sản Bác sĩ Sức",
   "address": "Đường Bình Than, khu Khã Lễ, phường Võ Cường, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.167262,
   "Latitude": 106.0645425
 },
 {
   "STT": 152,
   "Name": "Phòng khám Chuyên khoa Nhi Bạn hữu trẻ em",
   "address": "2 Nguyễn Long Bảng, phường Đại Phúc, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1712509,
   "Latitude": 106.0740978
 },
 {
   "STT": 153,
   "Name": "Phòng khám Chuyên khoa Phụ sản Thiên Đức",
   "address": "55 đường Thiên Đức, phường Kinh Bắc, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1839601,
   "Latitude": 106.0629849
 },
 {
   "STT": 154,
   "Name": "Công ty dược phẩm Bắc Ninh",
   "address": "21 đường Nguyễn Văn Cừ, phường Ninh Xá, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1790838,
   "Latitude": 106.061935
 },
 {
   "STT": 155,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "738 đường Thiên Đức, phường Vệ An, thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1921172,
   "Latitude": 106.0525181
 },
 {
   "STT": 156,
   "Name": "Phòng khám Chuyên khoa Nhi - Lê Văn Nam",
   "address": "Số 24, đường Hai Bà Trưng, phường Suối Hoa, thành phố hường Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1833418,
   "Latitude": 106.0731316
 },
 {
   "STT": 157,
   "Name": "Phòng khám Nội số 56",
   "address": "Phố Mới, Thị trấn Chờ, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.1970939,
   "Latitude": 105.9561647
 },
 {
   "STT": 158,
   "Name": "Phòng khám Chuyên khoa Tai mũi họng ",
   "address": "132, Phố Chờ, thị trấn Chờ,huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.1970939,
   "Latitude": 105.9561647
 },
 {
   "STT": 159,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "thị trấn Chờ ( khu BVĐK), Yên Phong, Bắc Ninh",
   "Longtitude": 21.199086,
   "Latitude": 105.9542074
 },
 {
   "STT": 160,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Đông Bích, Đông Thọ, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1707054,
   "Latitude": 105.9384099
 },
 {
   "STT": 161,
   "Name": "Phòng khám Chuyên khoa Nha khoa Hà Nội",
   "address": "Chợ Núi, Yên phụ, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.1991766,
   "Latitude": 105.9261417
 },
 {
   "STT": 162,
   "Name": "Cơ sở Nha khoa Nam Thắng",
   "address": "thị trấn Chờ,Yên Phong, Bắc Ninh",
   "Longtitude": 21.1978889,
   "Latitude": 105.9494248
 },
 {
   "STT": 163,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Nguyệt Cầu, Tam Giang, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1941429,
   "Latitude": 105.9582372
 },
 {
   "STT": 164,
   "Name": "Phòng khám Chuyên khoa Nội Đặng Đình Hiển",
   "address": "Quan Độ, Văn Môn, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1666948,
   "Latitude": 105.9273956
 },
 {
   "STT": 165,
   "Name": "Phòng khám Đinh Văn Trại",
   "address": "Phú Mẫn, thị trấn Chờ , Yên Phong, Bắc Ninh",
   "Longtitude": 21.1980089,
   "Latitude": 105.9495106
 },
 {
   "STT": 166,
   "Name": "Phòng Chẩn trị Y học cổ truyền Hùng Lan Đường",
   "address": "Thôn Thọ Đức, xã Tam Đa, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.2421024,
   "Latitude": 106.0249329
 },
 {
   "STT": 167,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Đường 286 xã Yên Phụ, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.1987165,
   "Latitude": 105.9277296
 },
 {
   "STT": 168,
   "Name": "Cơ sở chăm sóc Răng giả Trần Bộ",
   "address": "Đại Lâm, Tam Đa, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2106744,
   "Latitude": 106.0346163
 },
 {
   "STT": 169,
   "Name": "Nha khoa Bá Phong",
   "address": "Ngô Nội, Trung Nghĩa, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1885945,
   "Latitude": 105.9595802
 },
 {
   "STT": 170,
   "Name": "Phòng khám Da liễu - Lazer thẩm mỹ",
   "address": "Phố Mới, thị trấn Chờ, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1970939,
   "Latitude": 105.9561647
 },
 {
   "STT": 171,
   "Name": "Phòng khám Chuyên khoa Siêu âm Trí Đức I",
   "address": "Thượng Thôn, Đông Tiến, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2158557,
   "Latitude": 105.9696205
 },
 {
   "STT": 172,
   "Name": "Phòng khám Chuyên khoa Siêu âm Quý lưỡng",
   "address": "Khu đô thị mới thị trấn Chờ, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.1916093,
   "Latitude": 105.9564155
 },
 {
   "STT": 173,
   "Name": "Phòng khám Chuyên khoa Siêu âm Bình An",
   "address": "Khu đô thị mới thị trấn Chờ, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.1916093,
   "Latitude": 105.9564155
 },
 {
   "STT": 174,
   "Name": "Phòng khám Công ty Samsung Electronic Vietnam",
   "address": "Khu công nghiệp Yên Phong, xã Yên Trung, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.2145001,
   "Latitude": 105.9748077
 },
 {
   "STT": 175,
   "Name": "Phòng khám Sản phụ khoa 286 Hà Nội",
   "address": "Đường 286 Ngô Nội, Trung Nghĩa, Yên Phong, Bắc Ninh",
   "Longtitude": 21.196205,
   "Latitude": 105.956298
 },
 {
   "STT": 176,
   "Name": "Cơ sở Nha khoaTrần việt",
   "address": "Thôn Ngô xá, Long châu, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.1920145,
   "Latitude": 105.9942055
 },
 {
   "STT": 177,
   "Name": "Cơ sở Răng giả Vũ Tình",
   "address": "Thôn nghiêm xá, Thị trấn chờ Yên Phong, Bắc Ninh",
   "Longtitude": 21.188969,
   "Latitude": 105.955559
 },
 {
   "STT": 178,
   "Name": "Phòng Chẩn trị Y học cổ truyền Phạm Ngọc Vinh",
   "address": "Thôn Lương Cầm, xã Dũng Liệt, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2469267,
   "Latitude": 105.9957504
 },
 {
   "STT": 179,
   "Name": " Phòng khám Chuyên khoa Y học cổ truyền Bác sĩ Nô Văn Kỳ",
   "address": "Phố Chờ, thị trấn Chờ, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1970939,
   "Latitude": 105.9561647
 },
 {
   "STT": 180,
   "Name": "Phòng khám Chuyên khoa Nội - Siêu Âm",
   "address": "Thôn Bình An, xã Đông Thọ, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1765686,
   "Latitude": 105.9415913
 },
 {
   "STT": 181,
   "Name": "Phòng khám Nha khoa Nam Việt YP",
   "address": "Khu đô thị mới thị trấn chờ, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1916093,
   "Latitude": 105.9564155
 },
 {
   "STT": 182,
   "Name": "Phòng khám  Duy Luyện",
   "address": "Số 03 phố mới thị trấn chờ Yên Phong, Bắc Ninh",
   "Longtitude": 21.1970939,
   "Latitude": 105.9561647
 },
 {
   "STT": 183,
   "Name": "Phòng khám  Thanh Lịch",
   "address": "Thôn Đônónuất, Đông thọ Yên Phong, Bắc Ninh",
   "Longtitude": 21.1692977,
   "Latitude": 105.9457996
 },
 {
   "STT": 184,
   "Name": " Phòng khám Siêu âm Lê Hoa",
   "address": "Thôn Ngô Nội, Trung Nghĩa Yên phong, Bắc Ninh",
   "Longtitude": 21.1885945,
   "Latitude": 105.9595802
 },
 {
   "STT": 185,
   "Name": "Phòng khám Chuyên khoa Phuản Duy Vũ",
   "address": "Thôn ấp đồn Yên trung, Yên Phong, Bắc Ninh",
   "Longtitude": 21.205393,
   "Latitude": 105.998216
 },
 {
   "STT": 186,
   "Name": "Phòng khám Y khoa và Thẩm mỹ Thu Cúc - Sev",
   "address": "địa chỉ tầng 2, tòa nhà Netwrk, khu công nghiệp Yên Phong I, xã Yên Trung, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.197773,
   "Latitude": 105.994464
 },
 {
   "STT": 187,
   "Name": "Phòng khám Chuyên khoa Nội 2 Thanh Tùng",
   "address": "thôn Ấp Đồn, xã Yên Trung, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.2202724,
   "Latitude": 105.9876149
 },
 {
   "STT": 188,
   "Name": "Phòng khám Chuyên khoa Nội Ngô Xá",
   "address": "Thôn Ngô Xá, xã Long Châu, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.1920145,
   "Latitude": 105.9942055
 },
 {
   "STT": 189,
   "Name": "Phòng khám Công ty TNHH Samsung Display Việt Nam",
   "address": "Khu công Nghiệp Yên Phong, xã Yên Trung, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.2145001,
   "Latitude": 105.9748077
 },
 {
   "STT": 190,
   "Name": "Phòng khám Minh Huê",
   "address": "Thôn Trần Xá, xã Yên Trung, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.209834,
   "Latitude": 105.986279
 },
 {
   "STT": 191,
   "Name": "Phòng khám Mạnh Hùng",
   "address": "Thị trấn Chờ, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.1978889,
   "Latitude": 105.9494248
 },
 {
   "STT": 192,
   "Name": "Phòng khám Chuyên khoa Sao Khuê",
   "address": "thôn Yên Lãng, xã Yên Trung, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.2092511,
   "Latitude": 105.9936476
 },
 {
   "STT": 193,
   "Name": "Phòng khám Chuyên khoa Tiến Đạt",
   "address": "thôn Chi Long, xã Long Châu, huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.1981764,
   "Latitude": 106.0100842
 },
 {
   "STT": 194,
   "Name": "Phòng khám Lương y Nguyễn Ngọc Khoa",
   "address": "Khu 5, Thị trấn Phố Mới, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1483315,
   "Latitude": 106.1563303
 },
 {
   "STT": 195,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Xuân Bình, xã Đại Xuân, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1873922,
   "Latitude": 106.125015
 },
 {
   "STT": 196,
   "Name": "Phòng khám Chuyên khoa Răng hàm mặt",
   "address": "Từ Phong, xã Cách Bi, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1279659,
   "Latitude": 106.1852958
 },
 {
   "STT": 197,
   "Name": "Phòng khám Chuyên khoa Nội tổng hợp",
   "address": "An Đặng, xã Cách Bi, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1229729,
   "Latitude": 106.1845606
 },
 {
   "STT": 198,
   "Name": "Phòng khám Chuyên khoa Nội tổng hợp",
   "address": "thôn Lầy, xã Đào Viên, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1148879,
   "Latitude": 106.2080897
 },
 {
   "STT": 199,
   "Name": "Phòng khám Chuyên khoa Tai mũi họng ",
   "address": "Khu 3, Phố Mới, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1489952,
   "Latitude": 106.1551531
 },
 {
   "STT": 200,
   "Name": "Phòng Y học cổ truyền chuyên phong thấp",
   "address": "Khu 3, thị trấn Phố Mới, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1491141,
   "Latitude": 106.1551809
 },
 {
   "STT": 201,
   "Name": "Phòng khám Quảng Sinh Đường",
   "address": "Hữu Bằng, xã Ngọc Xá, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1350096,
   "Latitude": 106.2242676
 },
 {
   "STT": 202,
   "Name": "Phòng khám Bích Phương",
   "address": "Đông Du, xã Đào Viên, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1184411,
   "Latitude": 106.1905816
 },
 {
   "STT": 203,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Nghiêm Xá, xã Việt Hùng, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1515379,
   "Latitude": 106.1661804
 },
 {
   "STT": 204,
   "Name": "Cơ sở dịch vụ Y tế ",
   "address": "Đông Du, xã Đào Viên, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1184411,
   "Latitude": 106.1905816
 },
 {
   "STT": 205,
   "Name": "Phòng khám tư nhân chuyên răng Huyên Cường",
   "address": "Do Nha, xã Phương Liễu, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1732871,
   "Latitude": 106.1079848
 },
 {
   "STT": 206,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Khu 3, Thị trấn Phố Mới, Quế Võ, Bắc Ninh",
   "Longtitude": 21.1491141,
   "Latitude": 106.1551809
 },
 {
   "STT": 207,
   "Name": "Phòng khám Chuyên khoa Nội tổng hợp",
   "address": "Quế Tân, xã Quế Tân, Quế Võ, Bắc Ninh",
   "Longtitude": 21.1659561,
   "Latitude": 106.1921545
 },
 {
   "STT": 208,
   "Name": "Phòng khám Trang Đạt",
   "address": "Thị Phố Mới, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1534803,
   "Latitude": 106.1536828
 },
 {
   "STT": 209,
   "Name": "Cơ sở chăm sóc Nha khoa",
   "address": "Thôn Lựa, Việt Hùng, Quế Võ, Bắc Ninh",
   "Longtitude": 21.1478183,
   "Latitude": 106.1672991
 },
 {
   "STT": 210,
   "Name": "Cơ sở chăm sóc Răng miệng Trang Đạt",
   "address": "Thôn Dền, Đào Viên, Quế Võ, Bắc Ninh",
   "Longtitude": 21.1380669,
   "Latitude": 106.1819172
 },
 {
   "STT": 211,
   "Name": "Cơ sở chăm sóc Nha khoa",
   "address": "Thôn Lựa, Việt Hùng, Quế Võ, Bắc Ninh",
   "Longtitude": 21.1478183,
   "Latitude": 106.1672991
 },
 {
   "STT": 212,
   "Name": "Phòng khám Tâm Đức",
   "address": "Thôn Đỉnh, thị trấn Phố mới, Quế Võ, Bắc Ninh",
   "Longtitude": 21.1486329,
   "Latitude": 106.1595734
 },
 {
   "STT": 213,
   "Name": "Phòng khám Trồng Răng Đông Thành",
   "address": "Thị trấn Phố Mới, Quế Võ, Bắc Ninh",
   "Longtitude": 21.1534803,
   "Latitude": 106.1536828
 },
 {
   "STT": 214,
   "Name": "Phòng khám  An nhân Đường",
   "address": "Công Cối, Đại Xuân, Quế Võ, Bắc Ninh",
   "Longtitude": 21.1865246,
   "Latitude": 106.1205834
 },
 {
   "STT": 215,
   "Name": "Phòng Chẩn trị Y học cổ truyền Ba Dân",
   "address": "Khu 2 thị trấn Phố Mới, Quế Võ, Bắc Ninh",
   "Longtitude": 21.1534226,
   "Latitude": 106.1518371
 },
 {
   "STT": 216,
   "Name": "Phòng khám Trí Đức",
   "address": "Thôn Đồng Sài, xã Phù Lãng, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1497181,
   "Latitude": 106.2167805
 },
 {
   "STT": 217,
   "Name": "Phòng khám Chuyên khoa Nhi - Siêu âm tổng quát Sơn Hương",
   "address": "Số 57 Khu I thị trấn Phố Mới, Quế Võ, Bắc Ninh",
   "Longtitude": 21.1489952,
   "Latitude": 106.1551531
 },
 {
   "STT": 218,
   "Name": "Phòng khám Chuyên khoa Da liễu",
   "address": "Thôn Đông Du, Đào Viên, Quế Võ, Bắc Ninh",
   "Longtitude": 21.1154094,
   "Latitude": 106.2064648
 },
 {
   "STT": 219,
   "Name": "Phòng khám Chuyên khoa ",
   "address": "Nghiêm Thôn, thị trấn Phố Mới, Quế Võ, Bắc Ninh",
   "Longtitude": 21.1493102,
   "Latitude": 106.1522984
 },
 {
   "STT": 220,
   "Name": "Phòng Chẩn trị Y học cổ truyền ",
   "address": "Nghiêm Xá, Việt Hùng, Quế Võ, Bắc Ninh",
   "Longtitude": 21.1515379,
   "Latitude": 106.1661804
 },
 {
   "STT": 221,
   "Name": "Nha khoa Long Hương",
   "address": "Từ Phong , Cách Bi, Quế Võ, Bắc Ninh",
   "Longtitude": 21.1279659,
   "Latitude": 106.1852958
 },
 {
   "STT": 222,
   "Name": "Phòng Răng Minh Khương",
   "address": "Khu 4  Thị Trấn Phố Mới, Quế Võ, Bắc Ninh",
   "Longtitude": 21.1572739,
   "Latitude": 106.1514204
 },
 {
   "STT": 223,
   "Name": "Phòng khám Nội An Bình",
   "address": "Thôn Mão Trung, Xã Phượng Mao, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.154276,
   "Latitude": 106.14721
 },
 {
   "STT": 224,
   "Name": "Phòng khám Chuyên khoa CĐHA Quế Võ",
   "address": "DĐường 18, thôn Đỉnh, thị trấn Phố Mới, Quế Võ, Bắc Ninh",
   "Longtitude": 21.1529967,
   "Latitude": 106.1535921
 },
 {
   "STT": 225,
   "Name": "Phòng khám Chuyên khoa ",
   "address": "Khu 3 Thịnh Cầu, thị trấn Phố Mới, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1489952,
   "Latitude": 106.1551531
 },
 {
   "STT": 226,
   "Name": "Phòng khám Chuyên khoa ",
   "address": "thôn Do Nha, xã Phương Liễu, Quế Võ, Bắc Ninh",
   "Longtitude": 21.1732871,
   "Latitude": 106.1079848
 },
 {
   "STT": 227,
   "Name": "Phòng khám Chẩn trị Y học cổ truyền Bác sĩ Thịnh",
   "address": "thôn Đông Du Núi, xã Đào Viên, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1184411,
   "Latitude": 106.1905816
 },
 {
   "STT": 228,
   "Name": "Phòng Chẩn trị Y học cổ truyền Phúc Tâm Đường",
   "address": "thôn Mao Dộc, xã Phượng Mao, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1543426,
   "Latitude": 106.1367842
 },
 {
   "STT": 229,
   "Name": "Phòng khám Chuyên khoa Sản Vuông Tròn",
   "address": "thôn Trung, xã Phượng Mao, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1504392,
   "Latitude": 106.1448601
 },
 {
   "STT": 230,
   "Name": "Phòng khám Chuyên khoa CĐHA Hải Sơn",
   "address": "thôn Do Nha, xaã phương Liễu, huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1732871,
   "Latitude": 106.1079848
 },
 {
   "STT": 231,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Thôn Tam Tảo,Phú Lâm, huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.1598491,
   "Latitude": 106.0059782
 },
 {
   "STT": 232,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Núi Móng, xã Hoàn Sơn, huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.1253797,
   "Latitude": 106.00304
 },
 {
   "STT": 233,
   "Name": "Phòng khám Chuyên khoa Phụ - Sản",
   "address": "143 Ng.Đăng Đạo, thị trấn Lim, huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.1356352,
   "Latitude": 106.021446
 },
 {
   "STT": 234,
   "Name": "Phòng khám Nội Nhi Duy Hưng",
   "address": "Thôn Đông, Hoàn Sơn, huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.1032348,
   "Latitude": 106.0074474
 },
 {
   "STT": 235,
   "Name": "Phòng khám Chuyên khoa Nội tổng hợp",
   "address": "Duệ Đông, Thị trấn Lim, huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.1372778,
   "Latitude": 106.0147932
 },
 {
   "STT": 236,
   "Name": "Phòng khám Chuyên khoa Phụ - Sản",
   "address": " Quán Trắng, thôn Lũng Sơn, thị trấn Lim , huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.1445775,
   "Latitude": 106.0258126
 },
 {
   "STT": 237,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Chi Đống, Tân Chi, Tiên Du, huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.0784832,
   "Latitude": 106.0838582
 },
 {
   "STT": 238,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Chi Đống, Tân Chi, Tiên Du, Bắc Ninh",
   "Longtitude": 21.0784832,
   "Latitude": 106.0838582
 },
 {
   "STT": 239,
   "Name": "Phòng khám Vũ Lợi",
   "address": "Lộ Bao, Nội Duệ, huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.1178311,
   "Latitude": 106.0287512
 },
 {
   "STT": 240,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Thôn Lương, Tri Phương, huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.0816438,
   "Latitude": 106.006533
 },
 {
   "STT": 241,
   "Name": "Cơ sở chăm sóc Răng miệng",
   "address": "Long Khám, Việt Đoàn, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1162413,
   "Latitude": 106.0302205
 },
 {
   "STT": 242,
   "Name": "Cơ sở chăm sóc Răng miệng Việt Đức",
   "address": "Đường Nguyễn Đăng Đạo, thị trấn Lim, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1437252,
   "Latitude": 106.0190497
 },
 {
   "STT": 243,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Thôn Ngô Xá, xã Phật tích, Tiên Du, Bắc Ninh",
   "Longtitude": 21.0965381,
   "Latitude": 106.0243433
 },
 {
   "STT": 244,
   "Name": "Cơ sở Cơ sở Nha khoa",
   "address": "Chợ Sơn, xã Việt Đoàn, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1096804,
   "Latitude": 106.0366384
 },
 {
   "STT": 245,
   "Name": "Phòng khám Nam Mỹ",
   "address": "Chợ Bựu, xã Liên Bão, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1292205,
   "Latitude": 106.0266387
 },
 {
   "STT": 246,
   "Name": "Phòng khám Đức Tính",
   "address": "Hộ Vệ, Lạc Vệ, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1215158,
   "Latitude": 106.0754676
 },
 {
   "STT": 247,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Thôn Lương, xã Tri Phương, Tiên Du, Bắc Ninh",
   "Longtitude": 21.083449,
   "Latitude": 106.0044783
 },
 {
   "STT": 248,
   "Name": "Phòng Chẩn trị Y học cổ truyền Tiên Hoài Đường",
   "address": "Thôn Hoài Trung, xã Liên Bão, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1336397,
   "Latitude": 106.0301206
 },
 {
   "STT": 249,
   "Name": "Cơ sở chăm sóc Răng Miệng Ngọc Truyền",
   "address": "Thôn Tứ Chi, xã Tân, huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.0958141,
   "Latitude": 106.0794652
 },
 {
   "STT": 250,
   "Name": "Phòng khám Khánh Hòa",
   "address": ", Bắc Ninh",
   "Longtitude": 21.121444,
   "Latitude": 106.1110501
 },
 {
   "STT": 251,
   "Name": "Phòng khám Phạm Dũng",
   "address": "Xóm 2, thôn Lương, xã Tri Phương, huyên Tiên Du, Bắc Ninh",
   "Longtitude": 21.0816438,
   "Latitude": 106.006533
 },
 {
   "STT": 252,
   "Name": "Phòng khám bác sỹ Quý",
   "address": "Thôn Rền, xã Cảnh Hưng, huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.0846853,
   "Latitude": 106.0302688
 },
 {
   "STT": 253,
   "Name": "Phòng khám Bác sỹ An",
   "address": "Đồng Chuông, thị trấn Lim, huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.1414542,
   "Latitude": 106.0216016
 },
 {
   "STT": 254,
   "Name": "Phòng khám Bác sĩ Hường",
   "address": "Thôn Đình Cả, xã Nội Duệ, huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.1388856,
   "Latitude": 106.0063115
 },
 {
   "STT": 255,
   "Name": "Phòng khám Việt Đức",
   "address": "Thôn Hoài Thượng, xã Liên bão, huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.1302752,
   "Latitude": 106.0245772
 },
 {
   "STT": 256,
   "Name": "Phòng khám Bình Minh",
   "address": "Khu phố Trung Hòa, phường Đình Bảng, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1053781,
   "Latitude": 105.952789
 },
 {
   "STT": 257,
   "Name": "Bài thuốc gia truyền Ninh mỹ đường thang",
   "address": "124 Minh Khai, phường Đông ngàn, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1219222,
   "Latitude": 105.9666704
 },
 {
   "STT": 258,
   "Name": "Bài Thuốc gia truyền Cao dán mụn nhọt ông Lang Chọi",
   "address": "113 Minh Khai, phường Đông ngàn thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1215472,
   "Latitude": 105.9662574
 },
 {
   "STT": 259,
   "Name": "Bài thuốc gia truyền Các chứng đau mắt thể hạ nhiệt và phong nhiệt",
   "address": "121 Minh Khai, phường Đông ngàn thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1216174,
   "Latitude": 105.9664159
 },
 {
   "STT": 260,
   "Name": "Bài thuốc gia truyền Vạn hòa đường thang",
   "address": "119 Minh Khai, phường Đông ngàn thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1216242,
   "Latitude": 105.9663619
 },
 {
   "STT": 261,
   "Name": "Bài thuốc gia truyền Trung hòa đường thang",
   "address": "111 Minh Khai, phường Đông ngàn thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1215618,
   "Latitude": 105.966248
 },
 {
   "STT": 262,
   "Name": "Bài thuốc gia truyền Thống kinh phụ nữ thể huyết nhiệt",
   "address": "214 Trần phú, phường Đông ngàn thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1179338,
   "Latitude": 105.9594609
 },
 {
   "STT": 263,
   "Name": "Dịch vụ Y tế ",
   "address": "Đa Hội. xã Châu Khê thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1184735,
   "Latitude": 105.9318012
 },
 {
   "STT": 264,
   "Name": "Phòng khám Sức khỏe Hà Quỳnh",
   "address": "Dương Lôi, xã Tân Hồng thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1142835,
   "Latitude": 105.9739977
 },
 {
   "STT": 265,
   "Name": "Dịch vụ Y tế Cơ sở SK",
   "address": "Thọ Môn, phường Đình Bảng thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1079771,
   "Latitude": 105.9515051
 },
 {
   "STT": 266,
   "Name": "Phòng Chẩn trị Y học cổ truyền Đào Nguyên",
   "address": "Khu Phố Đình, phường Đình Bảng thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1086496,
   "Latitude": 105.9521059
 },
 {
   "STT": 267,
   "Name": "Phòng khám Chuyên khoa phụ sản",
   "address": "Khu phố Đại Đình, phường Đồng Kỵ thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.108448,
   "Latitude": 105.955729
 },
 {
   "STT": 268,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Thọ Môn, phường Đình Bảng thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1079771,
   "Latitude": 105.9515051
 },
 {
   "STT": 269,
   "Name": "Dịch vụ Y tế ",
   "address": "Tiên Bào, Phù Khê thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.136085,
   "Latitude": 105.9369413
 },
 {
   "STT": 270,
   "Name": "Phòng khám Chuyên khoa nội",
   "address": "Khu phố Song Tháp, phường Châu Khê thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.121077,
   "Latitude": 105.926764
 },
 {
   "STT": 271,
   "Name": "Phòng khám Chuyên khoa phụ sản",
   "address": "100 Lê Hồng Phong, phường Đông Ngàn thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1156528,
   "Latitude": 105.9611748
 },
 {
   "STT": 272,
   "Name": "Phòng khám sản phụ khoa",
   "address": "Số 1, Khu công nghiệp ITD, phường Trang Hạ, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1288596,
   "Latitude": 105.9519986
 },
 {
   "STT": 273,
   "Name": "Phòng khám Khám chữa bệnh bằng Bài thuốc gia truyền ",
   "address": "104 Trần Phú, phường Đông Ngàn thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1191476,
   "Latitude": 105.9617078
 },
 {
   "STT": 274,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Phố Viềng, phường Đồng Nguyên thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1200532,
   "Latitude": 105.9625736
 },
 {
   "STT": 275,
   "Name": "Phòng khám Chuyên khoa Răng hàm mặt",
   "address": "Đ. Tô Hiến Thành, phố Hạ, phường Đình Bảng thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.0994022,
   "Latitude": 105.9494248
 },
 {
   "STT": 276,
   "Name": "Cơ sở dịch vụ Y tế ",
   "address": "Hưng Phúc, xã Tương Giang thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1380973,
   "Latitude": 105.9912874
 },
 {
   "STT": 277,
   "Name": "Phòng khám Chuyên khoa nội ( Ngoài giờ)",
   "address": "80A Khu công nghiệp ITD, phường Trang Hạ thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1288596,
   "Latitude": 105.9519986
 },
 {
   "STT": 278,
   "Name": "Phòng khám Bắc Hà",
   "address": "Tiến Bào, xã Phù Khê thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.136085,
   "Latitude": 105.9369413
 },
 {
   "STT": 279,
   "Name": "Phòng khám Chuyên khoa phụ sản - Ngoài giờ",
   "address": "Phố Mới, Tương Giang thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1404954,
   "Latitude": 105.987179
 },
 {
   "STT": 280,
   "Name": "Dịch vụ chăm sóc Nha khoa",
   "address": "Hương Mạc, thị xã Từ sơn, Bắc Ninh",
   "Longtitude": 21.1584646,
   "Latitude": 105.9376756
 },
 {
   "STT": 281,
   "Name": "Phòng khám Chuyên khoa Mắt - Hàng ngày",
   "address": "Số 13 Trần Phú, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1200452,
   "Latitude": 105.9636001
 },
 {
   "STT": 282,
   "Name": "Phòng khám Chuyên khoa Mắt - Ngoài giờ",
   "address": "Khu dân cư số I, phố Lê Hồng Phong, phường Đông Ngàn, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1208464,
   "Latitude": 105.9643076
 },
 {
   "STT": 283,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 24, Ngõ3 Minh Khai, thị xã Từ sơn, Bắc Ninh",
   "Longtitude": 21.1208582,
   "Latitude": 105.9667788
 },
 {
   "STT": 284,
   "Name": "Phòng khám Nha khoa Hồng Vân",
   "address": "Xuân Thụ, phường Đồng Nguyên, thị xã Từ sơn, Bắc Ninh",
   "Longtitude": 21.1319011,
   "Latitude": 105.9729255
 },
 {
   "STT": 285,
   "Name": "Phòng khám Số 5",
   "address": "Phố Mới, phường Đồng Nguyên, thị xã Từ sơn, Bắc Ninh",
   "Longtitude": 21.1215745,
   "Latitude": 105.9609428
 },
 {
   "STT": 286,
   "Name": "Phòng khám Hồng Phúc",
   "address": "Số 321 Phố Thịnh Lang, phường Đình Bảng, thị xã Từ sơn, Bắc Ninh",
   "Longtitude": 21.1113384,
   "Latitude": 105.9500563
 },
 {
   "STT": 287,
   "Name": "Phòng khám Minh Tâm",
   "address": "Phố Trịnh Xá, phường Châu Khê, thị xã Từ sơn, Bắc Ninh",
   "Longtitude": 21.1137402,
   "Latitude": 105.9290821
 },
 {
   "STT": 288,
   "Name": "Phòng khám Bác sĩ Vũ Thị Vân Anh - ngoài giờ",
   "address": "Số 7, A4 Khu công nghiệp Đồng Kỵ, phường Đồng Kỵ, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1389299,
   "Latitude": 105.9450188
 },
 {
   "STT": 289,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Số 13 Trần Phú, phường Đông Ngàn, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1200452,
   "Latitude": 105.9636001
 },
 {
   "STT": 290,
   "Name": "Phòng khám Chuyên khoa Nội – Nhi ( Ngoài giờ)",
   "address": "314 Minh Khai, phường Đông Ngàn, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1237044,
   "Latitude": 105.9696935
 },
 {
   "STT": 291,
   "Name": "Cơ sở Dịch vụ Y tế Ngô Ngọc Chương",
   "address": "Xã Tam Sơn, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1446421,
   "Latitude": 105.9746519
 },
 {
   "STT": 292,
   "Name": "Phòng khám Chuyên khoa Da liễu",
   "address": "17C phố Chùa Dận, phường Đình Bảng, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1091643,
   "Latitude": 105.9426601
 },
 {
   "STT": 293,
   "Name": "Phòng khám Chuyên khoa Hoàn Mỹ",
   "address": "Số 384 Minh Khai, phường Đông Ngàn, Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1244184,
   "Latitude": 105.9709177
 },
 {
   "STT": 294,
   "Name": "Phòng khám Sản - Phụ khoa",
   "address": "Số 69 Phố Mới, phường Đồng Nguyên, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1319011,
   "Latitude": 105.9729255
 },
 {
   "STT": 295,
   "Name": "Phòng Y tế CT TNHH điện Tử Fortes",
   "address": "Khu công nghiệp VSIP Bắc Ninh, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.0940358,
   "Latitude": 105.9653268
 },
 {
   "STT": 296,
   "Name": "Phòng khám Chuyên khoa Ngoại Hải Dương",
   "address": "Phố Tân Thành, phường Đồng Kỵ, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1389299,
   "Latitude": 105.9450188
 },
 {
   "STT": 297,
   "Name": "Phòng khám Chuyên khoa Răng hàm mặt Ngọc Quý",
   "address": "Khu phố Thanh Bình, phường Đồng Kỵ, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1389299,
   "Latitude": 105.9450188
 },
 {
   "STT": 298,
   "Name": "Phòng khám Chuyên khoa Thu Trang",
   "address": "Lô B09, khu qui chế phường Đông Ngàn, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1156528,
   "Latitude": 105.9611748
 },
 {
   "STT": 299,
   "Name": "Phòng khám Chuyên khoa Tai mũi họng 62 Đồng Nguyên",
   "address": "Tầng 1, số nhà 430, phố Mới, phường Đồng Nguyên, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1319011,
   "Latitude": 105.9729255
 },
 {
   "STT": 300,
   "Name": "Phòng khám Chuyên khoa Ngoại 62 Đồng Nguyên",
   "address": "Tầng 2, số nhà 430, phố Mới, phường Đồng nguyên, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1319011,
   "Latitude": 105.9729255
 },
 {
   "STT": 301,
   "Name": "Phòng khám Chuyên khoa Răng hàm mặt Vân Anh",
   "address": "địa chỉ mới Tầng 2 tòa nhà Minh Ngọc, khu phố Minh Khai, phường Đông Ngàn, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1235872,
   "Latitude": 105.9698017
 },
 {
   "STT": 302,
   "Name": "Phòng khám Chuyên khoa CĐHA Kim Thiều",
   "address": "Khu công nghiệp TM - DV Kim Thiều, xã Hương Mạc, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1525384,
   "Latitude": 105.9297057
 },
 {
   "STT": 303,
   "Name": "Phòng khám Chuyên khoa CĐHA bác sỹ Nhâm",
   "address": "Phường Đông Ngàn, thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1156528,
   "Latitude": 105.9611748
 },
 {
   "STT": 304,
   "Name": "Phòng khám Đào Nguyên II",
   "address": "Khu phố Đình Bảng, phường Đình Bảng, thị xã Từ Sơn, tỉnh Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1053781,
   "Latitude": 105.952789
 },
 {
   "STT": 305,
   "Name": "Phòng khám Chuyên khoa Tai mũi họng Hoa Ngọc Lan",
   "address": "Công Hà, xã Hà Mãn, huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0306683,
   "Latitude": 106.0327659
 },
 {
   "STT": 306,
   "Name": "Phòng khám Chuyên khoa Nội ",
   "address": "Tam Á, xã Gia Đông, huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0314807,
   "Latitude": 106.0684267
 },
 {
   "STT": 307,
   "Name": "Phòng khám Nội tỏng hợp Phố Khám",
   "address": "Phố Khám, xã Gia Bình huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.034647,
   "Latitude": 106.0836736
 },
 {
   "STT": 308,
   "Name": "Cơ sở Dịch vụ Y tế Nha khoa Bình Hằng",
   "address": "Thanh Bình, Xuân Lâm huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0370826,
   "Latitude": 106.0207272
 },
 {
   "STT": 309,
   "Name": "Phòng khám Tú Quyên",
   "address": ": Phố Mới, Thị trấn Hồ huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0510725,
   "Latitude": 106.087998
 },
 {
   "STT": 310,
   "Name": "Phòng khám Chuyên khoa Ngoại Quỳnh Hiền",
   "address": "Đông Côi, Thị trấn Hồ huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0404213,
   "Latitude": 106.0882675
 },
 {
   "STT": 311,
   "Name": "Phòng khám Siêu âm Thu Hương 408",
   "address": "Phố Đông Côi huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0378323,
   "Latitude": 106.0919988
 },
 {
   "STT": 312,
   "Name": "Phòng khám Chuyên khoa Ngoại Trịnh Vân",
   "address": "Phố Đông Côi, huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0378323,
   "Latitude": 106.0919988
 },
 {
   "STT": 313,
   "Name": "Cơ sở Dịch vụ Y tế ",
   "address": "Chợ Dâu,Thanh Khương, huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0360813,
   "Latitude": 106.0404142
 },
 {
   "STT": 314,
   "Name": "Cơ sở Dịch vụ Y tế Nha khoa Bình Hằng",
   "address": "Phố Khám, Gia Đông, huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0437995,
   "Latitude": 106.0757749
 },
 {
   "STT": 315,
   "Name": "Phòng khám Việt Trung",
   "address": "Ngọ Xá, Hoài Thượng, huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0675215,
   "Latitude": 106.1073753
 },
 {
   "STT": 316,
   "Name": "Cơ sở chăm sóc Nha khoa Minh Thưởng",
   "address": "Phố Dâu, Thanh Khương, Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0356025,
   "Latitude": 106.0405485
 },
 {
   "STT": 317,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Lạc Thổ Nam, thị trấn Hồ, Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0615628,
   "Latitude": 106.0871128
 },
 {
   "STT": 318,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Thanh Hoài, Thanh Khương, Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0412143,
   "Latitude": 106.0500574
 },
 {
   "STT": 319,
   "Name": "Cơ sở chăm sóc Răng miệng",
   "address": "Doãn Thượng, Xuân Lâm, Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0131546,
   "Latitude": 106.0144259
 },
 {
   "STT": 320,
   "Name": "Cơ sở chăm sóc Răng hàm mặt An Bình",
   "address": "Thôn Chợ, xã An Bình, Thuận Thành, Bắc Ninh",
   "Longtitude": 21.044012,
   "Latitude": 106.075809
 },
 {
   "STT": 321,
   "Name": "Cơ sở chăm sóc Răng hàm mặt Phố Vàng",
   "address": "Phố Vàng, Nghĩa Đạo, Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0041738,
   "Latitude": 106.1189389
 },
 {
   "STT": 322,
   "Name": "Cơ sở chăm sóc Răng miệng Giáp Huyền",
   "address": " Cửu Yên, Ngũ Thái, Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0207725,
   "Latitude": 106.0368326
 },
 {
   "STT": 323,
   "Name": "Phòng khám Thành Bắc",
   "address": "Số 5 khu Bến Hồ, Thị trấn Hồ, huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.068066,
   "Latitude": 106.0867977
 },
 {
   "STT": 324,
   "Name": " Phòng khám Chuyên khoa Răng hàm mặt Diệp Đức",
   "address": "số 146 phố Đông Côi, thị trấn Hồ, huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0378323,
   "Latitude": 106.0919988
 },
 {
   "STT": 325,
   "Name": "Phòng khám Chuyên khoa Ngoại Bác sĩ Hùng",
   "address": "Số 402, phố Đông Côi, thị trấn Hồ, huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0378323,
   "Latitude": 106.0919988
 },
 {
   "STT": 326,
   "Name": " Phòng khám Chuyên khoa Răng hàm mặt Đại Phong",
   "address": "Phố Công Hà-Hà Mãn, huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0317013,
   "Latitude": 106.0360979
 },
 {
   "STT": 327,
   "Name": "Phòng khám Chuyên khoa Mắt Bác sĩ Nga",
   "address": "Phố Khám, thôn Ngọc Khám, xã Gia Đông, huyênh Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0371315,
   "Latitude": 106.0857822
 },
 {
   "STT": 328,
   "Name": "Phòng khám Bác sỹ Lê Ngọc Nghĩa",
   "address": "Phố Khám, xã Gia Đông, huyênh Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0371315,
   "Latitude": 106.0857822
 },
 {
   "STT": 329,
   "Name": "Phòng khám Chuyên khoa Phụ - Sản",
   "address": "90 Cao Lỗ Vương- thị trấnGB, huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0980738,
   "Latitude": 106.2828481
 },
 {
   "STT": 330,
   "Name": "Cơ sở chăm sóc Răng miệng",
   "address": "Cầu Đào, Nhân Thắng, huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0627456,
   "Latitude": 106.2308862
 },
 {
   "STT": 331,
   "Name": "Phòng khám Chuyên khoa Phụ - Sản",
   "address": "163 đường Bình Than, thị trấn Gia Bình, huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0551884,
   "Latitude": 106.1872588
 },
 {
   "STT": 332,
   "Name": "Cơ sở Dịch vụ Y tế răng hàm giả",
   "address": "Thị trấn Gia Bình, huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0540878,
   "Latitude": 106.1786787
 },
 {
   "STT": 333,
   "Name": "Nha khoa Hoàng Dương",
   "address": "Thị trấn Gia Bình, huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0540878,
   "Latitude": 106.1786787
 },
 {
   "STT": 334,
   "Name": "Cơ sở chăm sóc Á Châu Nha khoa",
   "address": "Thị trấn Gia Bình, huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0540878,
   "Latitude": 106.1786787
 },
 {
   "STT": 335,
   "Name": "Phòng khám Thủ đô",
   "address": "Bảo Tháp, Đông Cứu Gia Bình,huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.071961,
   "Latitude": 106.1808844
 },
 {
   "STT": 336,
   "Name": "Cơ sở chăm sóc Răng miệng Trần Sự",
   "address": "thôn Nhân Hữu, xã Nhân Thắng, huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0695235,
   "Latitude": 106.2322569
 },
 {
   "STT": 337,
   "Name": "Cơ sở chăm sóc Nha khoa",
   "address": "Phố Bùng, xã Bình Dương, huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0925564,
   "Latitude": 106.2461185
 },
 {
   "STT": 338,
   "Name": " Cơ sở TR Thanh Đạm",
   "address": "Phố Ngụ, Nhân Thắng, Gia Bình, Bắc Ninh",
   "Longtitude": 21.0627456,
   "Latitude": 106.2308862
 },
 {
   "STT": 339,
   "Name": "Phòng khám Chuyên khoa Răng hàm mặt Bác sỹ Khánh",
   "address": "71 đường Bình Than, thị trấn Gia Bình, huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0551884,
   "Latitude": 106.1872588
 },
 {
   "STT": 340,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "thị trấn Gia Bình, huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0540878,
   "Latitude": 106.1786787
 },
 {
   "STT": 341,
   "Name": "Phòng khám Chuyên khoa sản Hoàng Linh",
   "address": "Đường Lê Văn Thịnh, thị trấn Gia Bình, Bắc Ninh",
   "Longtitude": 21.0599284,
   "Latitude": 106.1726495
 },
 {
   "STT": 342,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Thôn Nội Phú, thị trấn Gia Bình, huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0522001,
   "Latitude": 106.1707015
 },
 {
   "STT": 343,
   "Name": "Cơ sở chăm sóc Răng miệng Thái Hoàn",
   "address": "Thôn Bùng, xã Bình Dương, Gia Bình, Bắc Ninh",
   "Longtitude": 21.0915455,
   "Latitude": 106.2470659
 },
 {
   "STT": 344,
   "Name": "Phòng khám Y học cổ truyền Bảo Minh Đường",
   "address": "106 Nguyễn Văn Cừ, thị trấn Gia Bình, huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0540878,
   "Latitude": 106.1786787
 },
 {
   "STT": 345,
   "Name": "Phòng khám Chuyên khoa Nội Bác sĩ Sơn",
   "address": "Cầu Đào, xã Nhân Thắng, huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0627456,
   "Latitude": 106.2308862
 },
 {
   "STT": 346,
   "Name": "Phòng khám Chuyên khoa Sản Đức Lợi",
   "address": "thôn Đoan Bái, xã Đại Bái, huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0504283,
   "Latitude": 106.1419201
 },
 {
   "STT": 347,
   "Name": "Phòng khám Chuyên khoa Sản Phương Anh",
   "address": "thôn Yên Việt, xã Đông Cứu, huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0764073,
   "Latitude": 106.2169139
 },
 {
   "STT": 348,
   "Name": "Phòng khám Chuyên khoa Nội - Nhi ",
   "address": "Đạo Sử, Thị trấn Thứa, huyện Lương Tài, Bắc Ninh",
   "Longtitude": 21.0154239,
   "Latitude": 106.1964643
 },
 {
   "STT": 349,
   "Name": "Phòng khám Chuyên khoa Phụ - Sản",
   "address": "Ngọc Quan, Lâm Thao,huyện Lương Tài, Bắc Ninh",
   "Longtitude": 20.9734027,
   "Latitude": 106.1676508
 },
 {
   "STT": 350,
   "Name": "Phòng khám Chuyên khoa Nội Tổng hợp",
   "address": "An Mỹ, xã Mỹ Hương, huyện Lương Tài, Bắc Ninh",
   "Longtitude": 21.0328935,
   "Latitude": 106.2625114
 },
 {
   "STT": 351,
   "Name": "Phòng khám Chuyên khoa Nội Tổng hợp",
   "address": "Lạng Dương, Phú lương, huyện Lương Tài, Bắc Ninh",
   "Longtitude": 20.9992609,
   "Latitude": 106.207526
 },
 {
   "STT": 352,
   "Name": "Cơ sở răng giả Vũ Hiếu",
   "address": "Ngọc Quan, Lâm Thao, huyện Lương Tài, Bắc Ninh",
   "Longtitude": 20.9734027,
   "Latitude": 106.1676508
 },
 {
   "STT": 353,
   "Name": "Phòng khám Chuyên khoa Răng hàm mặt",
   "address": "Kim Đào, Thị trấn Thứa, huyện Lương Tài, Bắc Ninh",
   "Longtitude": 21.0096154,
   "Latitude": 106.1970601
 },
 {
   "STT": 354,
   "Name": "Cơ sở chăm sóc Răng miệng",
   "address": "Tân Dân, thị trấn Thứa, huyện Lương Tài, Bắc Ninh",
   "Longtitude": 21.018034,
   "Latitude": 106.2034088
 },
 {
   "STT": 355,
   "Name": "Phòng Chẩn trị Y học cổ truyền - Siêu âm",
   "address": "Cường Tráng,Lường Tài, Bắc Ninh",
   "Longtitude": 21.0449808,
   "Latitude": 106.2794293
 },
 {
   "STT": 356,
   "Name": "Phòng khám siêu âm Đăng Hữu",
   "address": "Phượng Giáo, thị trấn Thứa, huyện Lương Tài, Bắc Ninh",
   "Longtitude": 21.0216092,
   "Latitude": 106.2043232
 },
 {
   "STT": 357,
   "Name": "Phòng Chẩn trị Y học cổ truyền",
   "address": "Phú Trên, Phú Hòa, huyện Lương Tài, Bắc Ninh",
   "Longtitude": 21.0531924,
   "Latitude": 106.225003
 },
 {
   "STT": 358,
   "Name": "Phòng khám Chuyên khoa Phụ - Sản",
   "address": "Thôn Giang, thị trấn Thứa, Lương tài, Bắc Ninh",
   "Longtitude": 21.0178738,
   "Latitude": 106.2035805
 },
 {
   "STT": 359,
   "Name": "Cơ sở chăm sóc Răng miệng Tâm Đức",
   "address": "Xã Trung Kênh, huyện Lương Tài, Bắc Ninh",
   "Longtitude": 21.0417946,
   "Latitude": 106.2881384
 },
 {
   "STT": 360,
   "Name": "Cơ sở chăm sóc nha khoa Hà Nội",
   "address": "Thôn Hữu Ái, xã Tân Lãng, huyện Lương Tài, Bắc Ninh",
   "Longtitude": 21.0250026,
   "Latitude": 106.1868658
 },
 {
   "STT": 361,
   "Name": "Cơ sở chăm sóc Răng miệng Thiện Mỹ",
   "address": "Thôn Giàng, Thị trấn Thứa, Lương Tài, Bắc Ninh",
   "Longtitude": 21.0178738,
   "Latitude": 106.2035805
 },
 {
   "STT": 362,
   "Name": "Phòng khám Chuyên khoa Nội Bác sỹ Nguyệt",
   "address": "Thôn Thiên Đức, xã Trung Chính, huyện Lương Tài, Bắc Ninh",
   "Longtitude": 21.0121749,
   "Latitude": 106.2227968
 },
 {
   "STT": 363,
   "Name": "Phòng khám Chuyên khoa Nội 108",
   "address": "số 419, đường Hàm Thuyên, thị trấn Thứa, huyện Lương Tài, Bắc Ninh",
   "Longtitude": 21.0222876,
   "Latitude": 106.2080897
 },
 {
   "STT": 364,
   "Name": "Phòng khám Chuyên khoa Ngoại Chợ Đò - An Thịnh",
   "address": "Cường Tráng, xã An Thịnh, huyện Lương Tài, Bắc Ninh",
   "Longtitude": 21.0458558,
   "Latitude": 106.2751656
 },
 {
   "STT": 365,
   "Name": "Phòng khám Chuyên khoa Mắt Bác sĩ Điệp",
   "address": "Số 139 đường Hàn Thuyên, thị trấn Thứa, huyện Lương Tài, Bắc Ninh",
   "Longtitude": 21.0246575,
   "Latitude": 106.2112105
 },
 {
   "STT": 366,
   "Name": "Phòng khám Chuyên khoa Nội Việt Hương",
   "address": "Thôn Lĩnh Mai, xã Quảng Phú, huyện Lương Tài, Bắc Ninh",
   "Longtitude": 21.0121263,
   "Latitude": 106.1667653
 },
 {
   "STT": 367,
   "Name": "Phòng khám Chuyên khoa sản Bác sĩ Đạo",
   "address": "285 đường Hàn Thuyên, thị trấn Thứa, huyện Lương Tài, Bắc Ninh",
   "Longtitude": 21.0246575,
   "Latitude": 106.2112105
 },
 {
   "STT": 368,
   "Name": "Phòng khám Chuyên khoa Nội Việt Pháp",
   "address": "thôn Ngọc Thượng, xã Phú Hòa, huyện Lương Tài, tỉnh Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.0278149,
   "Latitude": 106.2404468
 },
 {
   "STT": 369,
   "Name": "Phòng Chẩn trị Y học cổ truyền Hương hảo",
   "address": "thôn Cường Tráng, xã An Thịnh, huyện Lương Tài, tỉnh Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.0278149,
   "Latitude": 106.2404468
 },
 {
   "STT": 370,
   "Name": "Phòng khám Chuyên khoa Y học cổ truyền Tuệ Tĩnh",
   "address": "thôn Cường Tráng, xã An Thịnh, huyện Lương Tài, tỉnh Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.0278149,
   "Latitude": 106.2404468
 }
];