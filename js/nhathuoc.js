var datanhathuoc = [
 {
   "STT": 1,
   "Name": "Quầy thuốc",
   "address": "Mai Cương, Cách Bi, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1182667,
   "Latitude": 106.1808844
 },
 {
   "STT": 2,
   "Name": "Quầy thuốc",
   "address": "Giang Liễu, Phương Liễu, Huyện Huyện Quế Võ,Tỉnh Bắc Ninh",
   "Longtitude": 21.1618475,
   "Latitude": 106.1272201
 },
 {
   "STT": 3,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Vân Dương, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1605068,
   "Latitude": 106.0963513
 },
 {
   "STT": 4,
   "Name": "Quầy thuốc Đăng Tuyết 1",
   "address": "Thôn Ô Cách, xã Đông Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 5,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Guột, Việt Hùng, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1398296,
   "Latitude": 106.1823549
 },
 {
   "STT": 6,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Hữu Bằng, Ngọc Xá, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1350096,
   "Latitude": 106.2242676
 },
 {
   "STT": 7,
   "Name": "Nhà thuốc Trần Văn Kế",
   "address": "Yên Phụ, Yên Phong, Bắc Nnh",
   "Longtitude": 21.1943571,
   "Latitude": 105.9259271
 },
 {
   "STT": 8,
   "Name": "Quầy số 21 ",
   "address": "Số 564 Đường Thiên Đức, Phường  Kinh Bắc, Thành phố Bắc Ninh",
   "Longtitude": 21.1895636,
   "Latitude": 106.0559231
 },
 {
   "STT": 9,
   "Name": "Quầy thuốc",
   "address": "Chợ Nội Doi, Đại Xuân, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1940697,
   "Latitude": 106.1237403
 },
 {
   "STT": 10,
   "Name": "Quầy số 16 ",
   "address": "18 Trần Lựu, Khu 5, Phường  Thị Cầu, Thành phố Bắc Ninh",
   "Longtitude": 21.1968592,
   "Latitude": 106.0891839
 },
 {
   "STT": 11,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Giang Liễu, Phương Liễu, Huyện Quế Võ,Tỉnh Bắc Ninh",
   "Longtitude": 21.1618475,
   "Latitude": 106.1272201
 },
 {
   "STT": 12,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Phấn Trung, Phù Lãng, Tỉnh Bắc Ninh",
   "Longtitude": 21.1487633,
   "Latitude": 106.2463305
 },
 {
   "STT": 13,
   "Name": "Đại lý số 26 Chi nhánh Gia Bình",
   "address": "Lãng Ngâm, Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0801007,
   "Latitude": 106.1492721
 },
 {
   "STT": 14,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "Phố Cống Hà, Hà Mãn, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0317013,
   "Latitude": 106.0360979
 },
 {
   "STT": 15,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": "Mẫn Xá, Văn Môn, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1701728,
   "Latitude": 105.930961
 },
 {
   "STT": 16,
   "Name": "Quầy thuốc",
   "address": "Mao Lại, Phượng Mao, Huyện Quế Võ",
   "Longtitude": 21.150714,
   "Latitude": 106.1375107
 },
 {
   "STT": 17,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Châu Phong, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1360355,
   "Latitude": 106.2580983
 },
 {
   "STT": 18,
   "Name": "Quầy thuốc Quang Thu",
   "address": "Mẫn Xá, Văn Môn, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1701728,
   "Latitude": 105.930961
 },
 {
   "STT": 19,
   "Name": "Nhà thuốc Huyền Ngọc",
   "address": "Chợ Núi, Yên Phụ,Yên Phong, Bắc Ninh",
   "Longtitude": 21.1991766,
   "Latitude": 105.9261417
 },
 {
   "STT": 20,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Thôn Đỉnh,Thị trấn Phố Mới, Bắc Ninh",
   "Longtitude": 21.1486329,
   "Latitude": 106.1595734
 },
 {
   "STT": 21,
   "Name": "Quầy thuốc CTCPDP Bắc Ninh",
   "address": "Phố Và, Hạp Lĩnh, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.137099,
   "Latitude": 106.0765097
 },
 {
   "STT": 22,
   "Name": "Quầy thuốc Ngọc Mai",
   "address": "Thọ Khê, Đông Thọ, yên Phong, Bắc ninh",
   "Longtitude": 21.1648059,
   "Latitude": 105.9545653
 },
 {
   "STT": 23,
   "Name": "Quầy thuốc Yên Linh",
   "address": "Thôn Thái Bảo, xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1455414,
   "Latitude": 106.1088452
 },
 {
   "STT": 24,
   "Name": "Quầy thuốc",
   "address": "Quầy thuốc Khánh Linh thuộc CTCP dược phẩm Thăng Hoa, Đại tự, Nam Sơn",
   "Longtitude": 40.7181785,
   "Latitude": -73.9943225999999
 },
 {
   "STT": 25,
   "Name": "Quầy thuốc Dũng Huyền",
   "address": "Long Châu, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1881286,
   "Latitude": 105.9905529
 },
 {
   "STT": 26,
   "Name": "Quầy thuốc số 42",
   "address": " Đương Xá, Phường  Van An, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1966033,
   "Latitude": 106.0493162
 },
 {
   "STT": 27,
   "Name": "Quầy thuốc",
   "address": "Đối diện cửa UTỉnh Bắc NinhD xã Phương Liễu, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.163438,
   "Latitude": 106.12575
 },
 {
   "STT": 28,
   "Name": "Quầy thuốc CND Tiên Du",
   "address": "Tam Tảo, Phú Lâm, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1598491,
   "Latitude": 106.0059782
 },
 {
   "STT": 29,
   "Name": "Quầy thuốc Thanh Trà ",
   "address": "Khu Phố Thượng, Đình Bảng, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1082992,
   "Latitude": 105.9520301
 },
 {
   "STT": 30,
   "Name": "Quầy thuốc Thu Phương",
   "address": "Đường 286 Hòa Tiến, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2003352,
   "Latitude": 105.911406
 },
 {
   "STT": 31,
   "Name": "Quầy thuốc",
   "address": "Thiị trấn Phố Mới, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1534803,
   "Latitude": 106.1536828
 },
 {
   "STT": 32,
   "Name": "Nhà thuốc Đồng Kỵ",
   "address": "Đồng Kỵ, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1399547,
   "Latitude": 105.9494248
 },
 {
   "STT": 33,
   "Name": "Đại lý Nguyễn Thị Hương",
   "address": " Đông Yên, Đông Phong, Yên Phong",
   "Longtitude": 21.1883842,
   "Latitude": 106.0103857
 },
 {
   "STT": 34,
   "Name": "Đại lý Thuốc Mỹ Linh",
   "address": "Thôn Yên Lãng, Yên Trung, Yên Phong",
   "Longtitude": 21.2092511,
   "Latitude": 105.9936476
 },
 {
   "STT": 35,
   "Name": "Đại lý thuốc Dương Thị Dung",
   "address": "Cầu Giưã, Yên Phụ, Yên Phong",
   "Longtitude": 21.1985614,
   "Latitude": 105.9273956
 },
 {
   "STT": 36,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": " Đức Lân,Yên Phụ, Yên Phong",
   "Longtitude": 21.1908065,
   "Latitude": 105.9251928
 },
 {
   "STT": 37,
   "Name": "Quầy CTTNHHDP Ngọc Lân",
   "address": "Trần Phú, Đông Ngàn, Thị xã Từ SơnTỉnh Bắc Ninh",
   "Longtitude": 21.1168619,
   "Latitude": 105.957686
 },
 {
   "STT": 38,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": "số 95 Thị trấn Chờ, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1988265,
   "Latitude": 105.9540743
 },
 {
   "STT": 39,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": "14 An Ninh, Yên Phụ, Yên Phong",
   "Longtitude": 21.1987165,
   "Latitude": 105.9277296
 },
 {
   "STT": 40,
   "Name": "Quầy thuốc Chi nhánh dược Quế Võ",
   "address": "Thái Bảo, Nam Sơn, Thành phố Bắc Ninh",
   "Longtitude": 21.1455414,
   "Latitude": 106.1088452
 },
 {
   "STT": 41,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Sơn Đông, Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.141189,
   "Latitude": 106.0931683
 },
 {
   "STT": 42,
   "Name": "Quầy thuốc Tư nhân",
   "address": "Thôn Nội Phú, Thị trấn Gia Bình, Huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0540878,
   "Latitude": 106.1786787
 },
 {
   "STT": 43,
   "Name": "Quầy thuốc Quỳnh Hương",
   "address": "Ngô Xá, Long Châu, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1917917,
   "Latitude": 105.9938582
 },
 {
   "STT": 44,
   "Name": "Quầy thuốc Hạnh Châu",
   "address": "Thôn Đại Thượng, Đại Đồng, Huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.0837543,
   "Latitude": 105.991829
 },
 {
   "STT": 45,
   "Name": "Quầy thuốc",
   "address": "Khu1 Thị trấn Phố Mới, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1489952,
   "Latitude": 106.1551531
 },
 {
   "STT": 46,
   "Name": "Quầy thuốc Nguyễn Thu Thủy",
   "address": "Bệnh viện đa khoa Tiên Du, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1331318,
   "Latitude": 106.0270324
 },
 {
   "STT": 47,
   "Name": "Quầy thuốc Nguyễn Thu Thủy",
   "address": "Bệnh viện đa khoa Tiên Du, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1331318,
   "Latitude": 106.0270324
 },
 {
   "STT": 48,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": "Chi Long, Long Châu,Yên Phong, Bắc Ninh",
   "Longtitude": 21.1915793,
   "Latitude": 105.9971665
 },
 {
   "STT": 49,
   "Name": "Quầy thuốc",
   "address": "Dốc Đông Du, Đào Viên, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1184411,
   "Latitude": 106.1905816
 },
 {
   "STT": 50,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": "Đông Yên, Đông Phong, Yên Phong",
   "Longtitude": 21.1883842,
   "Latitude": 106.0103857
 },
 {
   "STT": 51,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "48 Phố mới, Thị trấn Hồ, Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0417305,
   "Latitude": 106.0921484
 },
 {
   "STT": 52,
   "Name": "Nhà thuốc Bá Pha",
   "address": "442 Nguyễn Trãi, Phường   Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1684546,
   "Latitude": 106.0638486
 },
 {
   "STT": 53,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Giang Liễu, Phương Liễu, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1618475,
   "Latitude": 106.1272201
 },
 {
   "STT": 54,
   "Name": "Quầy thuốc Việt Hùng",
   "address": "Khu Bệnh viện Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0234276,
   "Latitude": 106.2081247
 },
 {
   "STT": 55,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "Mão Điền, Huyện Thuận Thành, Bắc ninh",
   "Longtitude": 21.065017,
   "Latitude": 106.12575
 },
 {
   "STT": 56,
   "Name": "Đại lý CND Tiên Du",
   "address": "Phúc Nghiêm, Phật Tích, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1078377,
   "Latitude": 106.0272819
 },
 {
   "STT": 57,
   "Name": "Quầy thuốc",
   "address": "Thôn Trúc Ổ, xã Mộ Đạo, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1182167,
   "Latitude": 106.13743
 },
 {
   "STT": 58,
   "Name": "Quầy thuốc CTCPDP ALPHA",
   "address": "221 Nguyễn Văn Cừ, Hòa Đình, Võ Cường, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1728969,
   "Latitude": 106.0529727
 },
 {
   "STT": 59,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": "Ngô Xá, Long Châu, Yên Phong",
   "Longtitude": 21.1917917,
   "Latitude": 105.9938582
 },
 {
   "STT": 60,
   "Name": "Quầy thuốc Tân Dược",
   "address": "Đường 286 , xã Hoà Tiến, Huyện Yên Phong,. Tỉnh Bắc Ninh",
   "Longtitude": 21.2125298,
   "Latitude": 105.9248543
 },
 {
   "STT": 61,
   "Name": "Đại lý  thuốc CND Tiên Du",
   "address": "Duệ Bao, Thị trấn Lim, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1178311,
   "Latitude": 106.0287512
 },
 {
   "STT": 62,
   "Name": "Nhà thuốc Long Hưng",
   "address": "Số 442/13 khu giãn dân Bồ Sơn, Võ Cường, Thành phố Tỉnh Bắc Ninh",
   "Longtitude": 21.169068,
   "Latitude": 106.060344
 },
 {
   "STT": 63,
   "Name": "Đại lý CND Tiên Du",
   "address": "Thôn Duệ Đông, Thị trấn Lim, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1374264,
   "Latitude": 106.0143757
 },
 {
   "STT": 64,
   "Name": "Quầy thuốc Hồng Mai",
   "address": "Số 121 Lê Văn Thịnh, Thị trấn Gia Bình, Huyện Gia Bình, Tỉnh Bắc NinHuyện ",
   "Longtitude": 21.0568949,
   "Latitude": 106.1758792
 },
 {
   "STT": 65,
   "Name": "Quầy thuốc số 6 Chi nhánh Gia Bình",
   "address": "Thị trấn Gia Bình, Gia Bình, Bắc Ninh",
   "Longtitude": 21.0540878,
   "Latitude": 106.1786787
 },
 {
   "STT": 66,
   "Name": "Quầy thuốc số 38 CTCPDP Bắc Ninh",
   "address": "Số 68 Phố Vũ, Phường   Đại Phúc, Thành phố Bắc Ninh",
   "Longtitude": 21.1738006,
   "Latitude": 106.0787142
 },
 {
   "STT": 67,
   "Name": "Quầy số 26 CTCPDP Bắc Ninh",
   "address": "632 Đường Thiên Đức, Phường   Vạn An, Thành phố Bắc Ninh",
   "Longtitude": 21.1904589,
   "Latitude": 106.0546732
 },
 {
   "STT": 68,
   "Name": "Đại lý Long Quân số 13",
   "address": "Tahnh Bình, Xuân Lâm, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0236868,
   "Latitude": 106.0140586
 },
 {
   "STT": 69,
   "Name": "Quầy số 9 CTCPDP - Kỳ Thiên",
   "address": "Khu 9, Phường   Đại Phúc, Thành phố Bắc Ninh, Tỉnh, Bắc Ninh",
   "Longtitude": 21.1724345,
   "Latitude": 106.085328
 },
 {
   "STT": 70,
   "Name": "Đại lý Chi nhánh Gia Bình",
   "address": "Đoan Bái, Đại Bái, Gia Bình, Bắc Ninh",
   "Longtitude": 21.0480383,
   "Latitude": 106.1441264
 },
 {
   "STT": 71,
   "Name": "Đại lý Quốc Tế Kinh Bắc",
   "address": "Giang Liễu, Phương Liễu, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1618475,
   "Latitude": 106.1272201
 },
 {
   "STT": 72,
   "Name": "Đại lý CTCP Quốc tế Kinh Bắc",
   "address": "Thôn Nội Phú, Thị trấn Gia Bình, Huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0540878,
   "Latitude": 106.1786787
 },
 {
   "STT": 73,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Chợ Cầu Tự, Ngọc Xá, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1279192,
   "Latitude": 106.2227968
 },
 {
   "STT": 74,
   "Name": "Đại lý CND Yên Phòng",
   "address": "Thôn Ấp Đồn, xã Yên Trung, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2062766,
   "Latitude": 105.9984151
 },
 {
   "STT": 75,
   "Name": "Quầy thuốc số 27 CTCPDP Bắc Ninh",
   "address": "Số 2A, Đường Đấu Mã, K2 Phường   Thị Cầu, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1909577,
   "Latitude": 106.086493
 },
 {
   "STT": 76,
   "Name": "Quầy thuốc Hoa",
   "address": "Phượng Giáo, Thị trấn Thứa, Huyện Lương Tài, Bắc Ninh",
   "Longtitude": 21.0216092,
   "Latitude": 106.2043232
 },
 {
   "STT": 77,
   "Name": "Quầy thuốc Linh Bích",
   "address": "Phượng Giáo, Thị trấn Thứa, Huyện Lương Tài, Bắc Ninh",
   "Longtitude": 21.0216092,
   "Latitude": 106.2043232
 },
 {
   "STT": 78,
   "Name": "Quầy thuốc AN Phát CTCPDP Alpha Việt Nam",
   "address": "Số 60 Lý Anh Tông, Phường   Võ Cường, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1696448,
   "Latitude": 106.0537752
 },
 {
   "STT": 79,
   "Name": "Quầy thuốc số 5 CTCPDP Đại Phúc Bắc Ninh",
   "address": "Bệnh viện đa khoa Huyện Lương Tài, Bắc Ninh",
   "Longtitude": 21.0234276,
   "Latitude": 106.2081247
 },
 {
   "STT": 80,
   "Name": "Quầy số 37 CTCPDP Bắc Ninh",
   "address": "Khu Lãm Làng, Phường  Vân Dương, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1511312,
   "Latitude": 106.1044863
 },
 {
   "STT": 81,
   "Name": "Đại Lý số 9 CTCPTMDP Long Quân",
   "address": "Long Châu, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1881286,
   "Latitude": 105.9905529
 },
 {
   "STT": 82,
   "Name": "Quầy thuốc Thiên Ân",
   "address": "Long Châu, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1881286,
   "Latitude": 105.9905529
 },
 {
   "STT": 83,
   "Name": "Quầy thuốc Minh Tuyết",
   "address": "Ngô Xá, Long Châu, Yên Phong",
   "Longtitude": 21.1917917,
   "Latitude": 105.9938582
 },
 {
   "STT": 84,
   "Name": "Quầy thuốc CTCPDPBN",
   "address": "162 Đường Bình Than, Thành phố Bắc Ninh, Tỉnh Bắc ninh",
   "Longtitude": 21.1706731,
   "Latitude": 106.0743024
 },
 {
   "STT": 85,
   "Name": "Quầy thuốc Ngọc Bảo",
   "address": "Ngô Xá, Long Châu, Yên Phong",
   "Longtitude": 21.1917917,
   "Latitude": 105.9938582
 },
 {
   "STT": 86,
   "Name": "Nhà thuốc Bệnh viện ĐK Từ Sơn",
   "address": " Số 292 Phố Mới,, Phường  Đồng Nguyên,Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1319011,
   "Latitude": 105.9729255
 },
 {
   "STT": 87,
   "Name": "Quầy thuốc số 2 CTCPDPBN",
   "address": "463 Nguyễn Trãi,Ninh Xá, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1679856,
   "Latitude": 106.0641402
 },
 {
   "STT": 88,
   "Name": "Quầy thuốc số 2 CTCPDPBN",
   "address": "463 Nguyễn Trãi,Ninh Xá, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1679856,
   "Latitude": 106.0641402
 },
 {
   "STT": 89,
   "Name": "Quầy thuốc số 31-CTCPDPBN",
   "address": "Khu 5, Phường  Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1672299,
   "Latitude": 106.0774283
 },
 {
   "STT": 90,
   "Name": "Quầy thuốc Lê Thị Hường",
   "address": "Bất Lự, Hoàn Sơn, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1130553,
   "Latitude": 105.9956945
 },
 {
   "STT": 91,
   "Name": "Quầy thuốc Bùi Hải Yến",
   "address": "Do Nha, Phương Liễu, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1732871,
   "Latitude": 106.1079848
 },
 {
   "STT": 92,
   "Name": "Quầy thuốc An Phát",
   "address": "Ô Cách , xã Đông Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 93,
   "Name": "Quầy thuốc Thái Hòa",
   "address": "Khu 1, Thị trấn Phố Mới, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1489952,
   "Latitude": 106.1551531
 },
 {
   "STT": 94,
   "Name": "Quầy thuốc Hiếu Nhung",
   "address": "Chợ Tiêu, Tương Giang, Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1378051,
   "Latitude": 105.9825157
 },
 {
   "STT": 95,
   "Name": "Quầy thuốc Tiến Vinh",
   "address": "Ngọc Trì, Bình Định, Lương Tài, Bắc Ninh",
   "Longtitude": 20.9945159,
   "Latitude": 106.1588288
 },
 {
   "STT": 96,
   "Name": "  Thanh Cầu",
   "address": "Thôn Núi Móng, xã Hoàn Sơn, Huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.2023365,
   "Latitude": 105.925047
 },
 {
   "STT": 97,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Khu 5 Thị trấn Phố Mới, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.150356,
   "Latitude": 106.1542948
 },
 {
   "STT": 98,
   "Name": " Đại lý Chi nhánh Yên Phong",
   "address": "Thôn Ấp Đồn, Yên Trung, Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.205393,
   "Latitude": 105.998216
 },
 {
   "STT": 99,
   "Name": "Quầy thuốc phòng khám đa khoa Hữu Phúc  số 2",
   "address": "QL 18 Thôn Giang Liễu, xã Phương Liễu, huện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.156771,
   "Latitude": 106.127256
 },
 {
   "STT": 100,
   "Name": "QuẦY thuốc Bạch Mai",
   "address": "Thôn Đỉnh, Thị trấn phố Mới, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1489952,
   "Latitude": 106.1551531
 },
 {
   "STT": 101,
   "Name": "Quầy thuốc Nguyễn Thị Chung ",
   "address": "Khu phố Thanh Bình, Phường  Đồng Kỵ, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1389299,
   "Latitude": 105.9450188
 },
 {
   "STT": 102,
   "Name": "Quầy thuốc Thùy Linh",
   "address": "Thôn Liên Thượng, xã Đại Xuân, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1935345,
   "Latitude": 106.1271357
 },
 {
   "STT": 103,
   "Name": "Quầy thuốc Tân Dược",
   "address": "Ki ôốt 115 Chợ Ngụ. Cầu Đào, Nhân Thắng, Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0638318,
   "Latitude": 106.2309361
 },
 {
   "STT": 104,
   "Name": " Đại lý CTCPTMDV Long Quân",
   "address": "Cửu Yên, Ngũ Thái,Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0207725,
   "Latitude": 106.0368326
 },
 {
   "STT": 105,
   "Name": " Đại lý CTCPTMDV Long Quân",
   "address": "Phoố Dâu, Thanh Khương, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0356025,
   "Latitude": 106.0405485
 },
 {
   "STT": 106,
   "Name": " Thanh Cầu",
   "address": "Thôn Giang Liễu, xã Phương Liễu, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1578393,
   "Latitude": 106.1255264
 },
 {
   "STT": 107,
   "Name": " Đại lý CTCPTMDV Long Quân",
   "address": "Phố Chẹm, Trạm Lộ, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0245727,
   "Latitude": 106.1073606
 },
 {
   "STT": 108,
   "Name": " Đại lý Chi nhánh Yên Phong",
   "address": "Thôn Ấp Đồn, Yên Trung, Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.205393,
   "Latitude": 105.998216
 },
 {
   "STT": 109,
   "Name": "Quầy thuốc số 12 CTCPDP Bắc Ninh ",
   "address": "Số 5 Trần Hưng Đạo, Phường   Tiền An, Thành phố Bắc Ninh",
   "Longtitude": 21.1814633,
   "Latitude": 106.063725
 },
 {
   "STT": 110,
   "Name": "Quầy thuốc số 1 thuộc  CTTNHH thiết bị y tế Tâm Thành",
   "address": "Khu 10, Đại Phúc, Thành phố Bắc Ninh",
   "Longtitude": 21.1757306,
   "Latitude": 106.0709139
 },
 {
   "STT": 111,
   "Name": "Quầy thuốc số 2",
   "address": "59 Ngyễn Gian Thanh, Phường   Suối Hoa, Thành phố Bắc Ninh",
   "Longtitude": 21.1832963,
   "Latitude": 106.0720861
 },
 {
   "STT": 112,
   "Name": "Quầy thuốc Vạn Khanh",
   "address": "Trung tâm thương mại chợ Dầu, Phường   Đông Ngàn, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1168751,
   "Latitude": 105.9611446
 },
 {
   "STT": 113,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Do Nha, Phương Liễu, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1732871,
   "Latitude": 106.1079848
 },
 {
   "STT": 114,
   "Name": "Quầy thuốc Yên Yên",
   "address": "Thôn Ngô Xá, Long Châu, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1920145,
   "Latitude": 105.9942055
 },
 {
   "STT": 115,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Thôn Hữu Bằng, xã Ngọc Xá, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1344283,
   "Latitude": 106.2220103
 },
 {
   "STT": 116,
   "Name": "Quầy thuốc An Phát",
   "address": "Âp Đồn, Yên Trung, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2068977,
   "Latitude": 105.9986327
 },
 {
   "STT": 117,
   "Name": "Quầy thuốc Bắc Ánh",
   "address": "Thôn Hoài Thượng, xã Liên Bão, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1302752,
   "Latitude": 106.0245772
 },
 {
   "STT": 118,
   "Name": "Quầy thuốc Duyên Thành",
   "address": "Số 238 Phố Đông Côi, Thị trấn Hồ, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0378323,
   "Latitude": 106.0919988
 },
 {
   "STT": 119,
   "Name": "Nhà thuốc Bình Hoa",
   "address": "Số 105 Trần Lựu, Phường  Thị Cầu, Thành phố Bắc Ninh",
   "Longtitude": 21.1967159,
   "Latitude": 106.0922992
 },
 {
   "STT": 120,
   "Name": "Quầy thuốc Bích Thắng",
   "address": "Ki ôt số 9, Đường Hai Bà Trưng, Thị trấn Lim, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1461626,
   "Latitude": 106.0233558
 },
 {
   "STT": 121,
   "Name": "Quầy thuốc Ngọc Lan",
   "address": "Thôn An Ninh, xã Yên Phụ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2023365,
   "Latitude": 105.925047
 },
 {
   "STT": 122,
   "Name": "Quầy thuốc số 1 CTCPDP - Kỳ Thiên",
   "address": "Số 11 Hai Bà Trưng, Thị trấn Lim, Huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.1438808,
   "Latitude": 106.0193109
 },
 {
   "STT": 123,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Thôn Giang Liễu, xã Phương Liễu, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1578393,
   "Latitude": 106.1255264
 },
 {
   "STT": 124,
   "Name": "Nhà thuốc Ý Yên",
   "address": "261 Đường Hoàng Quốc Việt, Thị Cầu, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1935436,
   "Latitude": 106.0870863
 },
 {
   "STT": 125,
   "Name": "Đại ly CTCPDP - Kỳ Thiên",
   "address": "Thôn Ô Cách, xã Đông Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 126,
   "Name": "Quầy thuốc Ngọc Cường",
   "address": "Thôn Liên Thượng, xã Đại Xuân, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1935345,
   "Latitude": 106.1271357
 },
 {
   "STT": 127,
   "Name": "Quầy thuốc Thu Quê ",
   "address": "Đa Vạn, Châu Khê,Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1195499,
   "Latitude": 105.9281298
 },
 {
   "STT": 128,
   "Name": "Quầy thuốc số 21",
   "address": "187 Nguyễn Cao, Phường  Ninh Xá, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1752446,
   "Latitude": 106.0600493
 },
 {
   "STT": 129,
   "Name": "Quầy thuốc số 61",
   "address": "40 Cầu Cạn Y Na, Phường  Kinh Bắc, Thành phố Bắc Ninh, Tỉnh Bắc ninh",
   "Longtitude": 21.1901869,
   "Latitude": 106.0700285
 },
 {
   "STT": 130,
   "Name": "Quầy thuốc số 14 ",
   "address": "Ki ôốt 12, Chợ Đọ Xá, Phường  Ninh Xá, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.176065,
   "Latitude": 106.0610848
 },
 {
   "STT": 131,
   "Name": "Đại lý Chi nhánh Gia Bình",
   "address": "Số 90 Cao Lỗ Vương, Thị trấn Gia Bình, Gia Bình, Bắc Ninh",
   "Longtitude": 21.0980738,
   "Latitude": 106.2828481
 },
 {
   "STT": 132,
   "Name": "Quầy thuốc Uyên Uyên",
   "address": " Đông Đạo, Thị trấn Lim, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1438082,
   "Latitude": 106.0221394
 },
 {
   "STT": 133,
   "Name": " Đại lý CTCPDP Đại Phúc Bắc Ninh",
   "address": "Phố Bùng, xã Bình Dương, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0925564,
   "Latitude": 106.2461185
 },
 {
   "STT": 134,
   "Name": "Quầy thuốc Việt Pháp",
   "address": "Thôn Điện Tiền, xã Nguyệt Đức, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0102411,
   "Latitude": 106.0662878
 },
 {
   "STT": 135,
   "Name": "Đại lýChi nhánh Gia Bình",
   "address": "Trạm  y tế Thị trấn Gia Bình, Huyện Gia Bình, Bắc Ninh",
   "Longtitude": 21.0540878,
   "Latitude": 106.1786787
 },
 {
   "STT": 136,
   "Name": "Đại lý Chi nhánh Gia Bình",
   "address": "Thôn Đoan Bái, xã Đại Bái, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0504283,
   "Latitude": 106.1419201
 },
 {
   "STT": 137,
   "Name": "Quầy thuốc Hoàng Minh",
   "address": "Số 358 Khu 6, Đường Lý Thường Kiệt, Phường   Thị Cầu, Thành phố Bắc Ninh",
   "Longtitude": 21.2042056,
   "Latitude": 106.0908083
 },
 {
   "STT": 138,
   "Name": "Quầy thuốc số 5 Chi nhánh Gia Bình",
   "address": "BV đa khoa Gia Bình",
   "Longtitude": 21.0530639,
   "Latitude": 106.181586
 },
 {
   "STT": 139,
   "Name": "Đại lý Chi nhánh Gia Bình",
   "address": "Thị trấn Gia Bình, Gia Bình",
   "Longtitude": 21.0540878,
   "Latitude": 106.1786787
 },
 {
   "STT": 140,
   "Name": "Đại lýChi nhánh Gia Bình",
   "address": "Xóm Ngoài, thôn Đại Bái, Gia Bình, Bắc Ninh",
   "Longtitude": 21.0380839,
   "Latitude": 106.1482394
 },
 {
   "STT": 141,
   "Name": "Đại lý Chi nhánh Gia Bình",
   "address": "Thị trấn Gia Bình, Gia Bình",
   "Longtitude": 21.0540878,
   "Latitude": 106.1786787
 },
 {
   "STT": 142,
   "Name": "Đại lý Chi nhánh Gia Bình",
   "address": "Xóm Sôn, thôn Đại Bái, Gia Bình, Bắc Ninh",
   "Longtitude": 21.0420994,
   "Latitude": 106.1454499
 },
 {
   "STT": 143,
   "Name": "Quầy thuốc Hương Mạnh",
   "address": " Thôn Ấp Đồn, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.205393,
   "Latitude": 105.998216
 },
 {
   "STT": 144,
   "Name": "Đại lý Chi nhánh Gia Bình",
   "address": "Phố Ngụ, Mhân Thắng, Gia Bình, Bắc Ninh",
   "Longtitude": 21.0627456,
   "Latitude": 106.2308862
 },
 {
   "STT": 145,
   "Name": "Đại lýChi nhánh Gia Bình",
   "address": "Yên Việt, Đông Cứu, Gia Bình, Bắc Ninh",
   "Longtitude": 21.066728,
   "Latitude": 106.1602991
 },
 {
   "STT": 146,
   "Name": " Đại lý Chi nhánh Yên Phong",
   "address": "Thôn Mẫn Xá, xã Long Châu, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1947753,
   "Latitude": 105.98912
 },
 {
   "STT": 147,
   "Name": " Đại lý bán thuốc CTCPDP Bắc Ninh",
   "address": "Thôn Phú mẫn, Thị trấn Chờ, Huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.1915293,
   "Latitude": 105.9552997
 },
 {
   "STT": 148,
   "Name": "Đại lýChi nhánh Gia Bình",
   "address": "Phố Núi, thôn An Quang, xã Lãng Ngâm, Gia Bình, BắcNinh",
   "Longtitude": 21.0728024,
   "Latitude": 106.1573585
 },
 {
   "STT": 149,
   "Name": "Nhà thuốc đông y gia truyền Hồng Khôi",
   "address": "Khu 5, Thị trấn Phố Mới, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1483315,
   "Latitude": 106.1563303
 },
 {
   "STT": 150,
   "Name": "Nhà thuốc MINH THUY",
   "address": "Số 686 Ngô Gia Tự,Ninh Xá, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1793993,
   "Latitude": 106.0603245
 },
 {
   "STT": 151,
   "Name": "Quầy số 6 ",
   "address": "Số 32 Nguyễn Văn Cừ, Phường   Ninh Xá, Thành phố Bắc Ninh",
   "Longtitude": 21.1767422,
   "Latitude": 106.0573997
 },
 {
   "STT": 152,
   "Name": "Quầy thuốc số 6",
   "address": "Số 16, tổ 18, khu 4, Phường  Vệ An, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1781766,
   "Latitude": 106.0710255
 },
 {
   "STT": 153,
   "Name": "Nhà thuốc số 34",
   "address": "Số 34 Phố Vũ, Phường  Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1738874,
   "Latitude": 106.0776114
 },
 {
   "STT": 154,
   "Name": "Quầy thuốc số 22",
   "address": "Số 16 Trần Lựu, Thị Cầu, Thành phố Bắc Ninh, Tỉnh Băc Ninh",
   "Longtitude": 21.1968233,
   "Latitude": 106.0891344
 },
 {
   "STT": 155,
   "Name": "Quầy thuốc Nguyễn Kim Anh ",
   "address": "Ngõ Chợ Gạo, Trần Phú, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 156,
   "Name": "Quầy thuốc Hải Linh",
   "address": "Thôn Đại Lai, xã Đại Lai, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0895137,
   "Latitude": 106.2112394
 },
 {
   "STT": 157,
   "Name": "Nhà thuốc Trường Minh",
   "address": "Số 26 Khu Yên Mẫn, Phường  Kinh Bắc, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1888172,
   "Latitude": 106.0638342
 },
 {
   "STT": 158,
   "Name": "Quầy thuốc BVĐK Yên Phong",
   "address": "Bệnh viện đa khoa Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.199086,
   "Latitude": 105.9542074
 },
 {
   "STT": 159,
   "Name": "Đại lý Quốc Tế Kinh Bắc",
   "address": "Thôn Giang Liễu, xã Phương Liễu, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1578393,
   "Latitude": 106.1255264
 },
 {
   "STT": 160,
   "Name": "Đại lý CTTNHHDP Ngọc Lân",
   "address": "Thôn Đình Cả, Nội Duệ, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1355376,
   "Latitude": 106.0071308
 },
 {
   "STT": 161,
   "Name": " Đại lý CTCPTMDV Long Quân",
   "address": "Thôn Nghĩa Vi, Hoài Thượng, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0818854,
   "Latitude": 106.1126129
 },
 {
   "STT": 162,
   "Name": " Đại lý CTCPTMDV Long Quân",
   "address": "Thôn Cả, Đông Côi, Thị trấn Hồ, Huyện Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0459495,
   "Latitude": 106.0961991
 },
 {
   "STT": 163,
   "Name": " Đại lý CTCPTMDV Long Quân",
   "address": "Xóm 3, Mão Điền, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 20.9916069,
   "Latitude": 106.1590321
 },
 {
   "STT": 164,
   "Name": " Đại lý CTCPTMDV Long Quân",
   "address": "Thôn Giữa, An Bình, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0474737,
   "Latitude": 106.1105283
 },
 {
   "STT": 165,
   "Name": "Đại lý Quốc Tế Kinh Bắc",
   "address": "Xóm Ngoài, Đại Thượng, Đại Đồng, Tiên Du, Bắc Ninh",
   "Longtitude": 21.0891496,
   "Latitude": 105.9875048
 },
 {
   "STT": 166,
   "Name": "Đại lý CND Tiên Du",
   "address": "Chợ Móng Núi, xã Hoàn Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1253797,
   "Latitude": 106.00304
 },
 {
   "STT": 167,
   "Name": "Quầy thuốc Thành An",
   "address": "Thôn Do Nha, xã Phương Liễu, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1732871,
   "Latitude": 106.1079848
 },
 {
   "STT": 168,
   "Name": "Quầy thuốc Trần Văn Hưng",
   "address": "Cầu Giưã, Yên Phụ, Yên Phong",
   "Longtitude": 21.1985614,
   "Latitude": 105.9273956
 },
 {
   "STT": 169,
   "Name": "Quầy thuốc Văn An CND Từ Sơn",
   "address": "Số 19, Ngõ 19, khu phố Trang Liệt, Phường   Trang Hạ, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1210851,
   "Latitude": 105.9536386
 },
 {
   "STT": 170,
   "Name": "Quầy số 33 CTCPDP Bắc Ninh",
   "address": "Số 56 Trần Lựu, Phường   Thị Cầu, Thành phố Bắc Ninh",
   "Longtitude": 21.1970357,
   "Latitude": 106.0899964
 },
 {
   "STT": 171,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "Bệnh viện đa khoa Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0379954,
   "Latitude": 106.0868108
 },
 {
   "STT": 172,
   "Name": "Quầy thuốc Phòng khám Y cao",
   "address": "Đường 286 Thị trấn Chờ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1958719,
   "Latitude": 105.9490464
 },
 {
   "STT": 173,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": "Đông Yên, Đông Phong, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1883842,
   "Latitude": 106.0103857
 },
 {
   "STT": 174,
   "Name": "Quầy thuốc của Chi nhánh Gia Bình",
   "address": "Nhân Thắng, Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0662907,
   "Latitude": 106.2316216
 },
 {
   "STT": 175,
   "Name": "Quầy thuốc của Chi nhánh Gia Bình",
   "address": "Nhân Thắng, Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0662907,
   "Latitude": 106.2316216
 },
 {
   "STT": 176,
   "Name": "Quầy thuốc Phúc Lộc Thọ",
   "address": "Chi Long, Long Châu, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1915793,
   "Latitude": 105.9971665
 },
 {
   "STT": 177,
   "Name": "Nhà thuốc Bệnh viện Sản Nhi ",
   "address": "Đường Bình Than, Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.167262,
   "Latitude": 106.0645425
 },
 {
   "STT": 178,
   "Name": "Quầy thuốc Hoài Thu",
   "address": "Đông thái,Đông Tiến, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2210386,
   "Latitude": 105.9721911
 },
 {
   "STT": 179,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": "Đông Bích, Đông Thọ, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1707054,
   "Latitude": 105.9384099
 },
 {
   "STT": 180,
   "Name": "Nhà thuốc Đàm Tuân",
   "address": "Châu Cầu, Châu Phong, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1293042,
   "Latitude": 106.245595
 },
 {
   "STT": 181,
   "Name": " Quang Thành",
   "address": "164 Đường Nguyễn Trãi, Phường  Ninh Xá, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1749091,
   "Latitude": 106.0605323
 },
 {
   "STT": 182,
   "Name": "Quầy thuốc Hoài Hùng",
   "address": "Bất Lự, Hoàn Sơn, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1130553,
   "Latitude": 105.9956945
 },
 {
   "STT": 183,
   "Name": "Quầy thuốc Đức Sơn",
   "address": "Thôn La Miệt, Yên Giả, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.114386,
   "Latitude": 106.1299798
 },
 {
   "STT": 184,
   "Name": "Đại lý chi nhánh Thuận Thành",
   "address": "Đa Tiện, Xuân Lâm, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0202789,
   "Latitude": 106.0118549
 },
 {
   "STT": 185,
   "Name": "Đại lý chi nhánh Thuận Thành",
   "address": "Bút Tháp, Đình Tổ, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.058099,
   "Latitude": 106.025078
 },
 {
   "STT": 186,
   "Name": "Quầy thuốc chi nhánh Thuận Thành",
   "address": "Đại Trạch, Đình Tổ, , Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0562582,
   "Latitude": 106.0441796
 },
 {
   "STT": 187,
   "Name": "Đại lý chi nhánh Thuận Thành",
   "address": "Liễu Lâm, Song Liễu, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 20.9999643,
   "Latitude": 106.0085492
 },
 {
   "STT": 188,
   "Name": "Đại lý chi nhánh Thuận Thành",
   "address": "Chợ Ngo, An Bình, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0487886,
   "Latitude": 106.11399
 },
 {
   "STT": 189,
   "Name": "Quầy thuốc Tâm An ",
   "address": "Số 742 Đường 286, Phường   Vạn An, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1921516,
   "Latitude": 106.0524486
 },
 {
   "STT": 190,
   "Name": "Đại lý chi nhánh Thuận Thành",
   "address": "Khu 3 , Mão Điền, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0637122,
   "Latitude": 106.12428
 },
 {
   "STT": 191,
   "Name": "Đại lý chi nhánh Thuận Thành",
   "address": "Đồng Ngư, Ngũ Thái, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0129341,
   "Latitude": 106.0280165
 },
 {
   "STT": 192,
   "Name": "Đại lý chi nhánh Thuận Thành",
   "address": "Liễu Khê, Song Liễu, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0047237,
   "Latitude": 106.0155278
 },
 {
   "STT": 193,
   "Name": "Quầy thuốc Minh uy 1",
   "address": "Ngõ Ngoài, Phú Mẫn, Thị trấn Chờ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1915293,
   "Latitude": 105.9552997
 },
 {
   "STT": 194,
   "Name": " Quang Thành",
   "address": "Số 81 Trần Hưng Đạo, Phường  Tiền An, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.180625,
   "Latitude": 106.0649217
 },
 {
   "STT": 195,
   "Name": "Đại lý Quốc Tế Kinh Bắc",
   "address": "Thôn Giang Liễu, xã Phương Liễu, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1578393,
   "Latitude": 106.1255264
 },
 {
   "STT": 196,
   "Name": " Thanh Cầu",
   "address": "Mao Dộc, Phượng Mao, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1543426,
   "Latitude": 106.1367842
 },
 {
   "STT": 197,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Thôn Giang Liễu, xã Phương Liễu, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1578393,
   "Latitude": 106.1255264
 },
 {
   "STT": 198,
   "Name": "Quầy thuốc Nguyễn Thị Yến ",
   "address": "Khu phố Yên Lã, Phường  Tân Hồng, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1076626,
   "Latitude": 105.971224
 },
 {
   "STT": 199,
   "Name": "Quầy thuốc Phú Thảo",
   "address": "Thôn Đoài, xã Hoàn Sơn, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1065555,
   "Latitude": 106.00304
 },
 {
   "STT": 200,
   "Name": "Nhà thuốc Hợp Lực",
   "address": "Phố Ba Huyện, Phường  Khắc Niệm, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1555583,
   "Latitude": 106.0690611
 },
 {
   "STT": 201,
   "Name": "Nhà thuốc Công Tâm",
   "address": "Tân Lập, Đình Bảng, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1015708,
   "Latitude": 105.9378513
 },
 {
   "STT": 202,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Thôn Guột, Việt Hùng, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1398296,
   "Latitude": 106.1823549
 },
 {
   "STT": 203,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": "Thôn An Ninh, xã Yên Phụ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2023365,
   "Latitude": 105.925047
 },
 {
   "STT": 204,
   "Name": "Đại lý CND Từ Sơn",
   "address": "Thôn Phù Lộc, xã Phù Chẩn, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.094228,
   "Latitude": 105.969191
 },
 {
   "STT": 205,
   "Name": " Thanh Cầu",
   "address": "Thôn Do Nha, xã Phương Liễu, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1732871,
   "Latitude": 106.1079848
 },
 {
   "STT": 206,
   "Name": "Quầy thuốc Thành Liên",
   "address": "Hữu Ái,Tân Lãng, Lương Tài, Bắc Ninh",
   "Longtitude": 21.0250026,
   "Latitude": 106.1868658
 },
 {
   "STT": 207,
   "Name": "Nhà thuốc Khánh Ly",
   "address": "Ô Cách , xã Đông Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 208,
   "Name": " Quang Thành",
   "address": "Khu Lãm Làng, Phường   Vân Dương, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1509203,
   "Latitude": 106.1037362
 },
 {
   "STT": 209,
   "Name": "Nhà thuốc Bình Dân",
   "address": "576 Nguyễn Trãi, Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.165303,
   "Latitude": 106.065156
 },
 {
   "STT": 210,
   "Name": "Quầy thuốc Minh Vượng",
   "address": "Công Hà, Hà Mãn, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0317013,
   "Latitude": 106.0360979
 },
 {
   "STT": 211,
   "Name": "Đại lý CTTNHHDP Ngọc Lân",
   "address": "Chợ thôn Đoài, xã Hoàn Sơn, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.0963721,
   "Latitude": 105.9875268
 },
 {
   "STT": 212,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": "Thị trấn Chờ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1978889,
   "Latitude": 105.9494248
 },
 {
   "STT": 213,
   "Name": "Quầy thuốc Bắc Hà",
   "address": "Đối diện Chợ Chờ, Thị trấn Chờ,Yên Phong,, Bắc Ninh",
   "Longtitude": 21.1945697,
   "Latitude": 105.9556165
 },
 {
   "STT": 214,
   "Name": "Nhà thuốc Hải Nam",
   "address": "20 B  Phố Vũ, Phường  Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1738006,
   "Latitude": 106.0787142
 },
 {
   "STT": 215,
   "Name": "Quầy thuốc số 4 ",
   "address": "Khu phố 4, Cẩm Giang, Đồng Nguyên, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1263583,
   "Latitude": 105.9685626
 },
 {
   "STT": 216,
   "Name": "Quầy thuốc Quỳnh Chi",
   "address": "Đại Thượng, Đại Đồng, Tiên Du, Bắc Ninh",
   "Longtitude": 21.0883506,
   "Latitude": 105.9890839
 },
 {
   "STT": 217,
   "Name": "Quầy thuốc số 6",
   "address": "Thôn Đông Du, xã Đào Viên, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1154094,
   "Latitude": 106.2064648
 },
 {
   "STT": 218,
   "Name": "Nhà thuốc Thanh Ngân",
   "address": "Số 62 Hồ Ngọc Lân, Phường   Kinh Bắc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1876848,
   "Latitude": 106.0661496
 },
 {
   "STT": 219,
   "Name": "Nhà thuốc Huệ Anh",
   "address": "Số 536 Nguyễn Trãi, Phường   Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1663717,
   "Latitude": 106.0647307
 },
 {
   "STT": 220,
   "Name": "Nhà thuốc Đàm Tuân 2",
   "address": "Thôn Châu Cầu, xã Châu Phong, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1293042,
   "Latitude": 106.245595
 },
 {
   "STT": 221,
   "Name": "Quầy thuốc Khánh My",
   "address": "Thôn Đông Yên, xã Đông Phong, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1891678,
   "Latitude": 106.0061096
 },
 {
   "STT": 222,
   "Name": "Quầy thuốc Nguyễn Thị Tươi",
   "address": "Mao Dộc, Phượng Mao, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1543426,
   "Latitude": 106.1367842
 },
 {
   "STT": 223,
   "Name": " Quầy CTCPTMDV Long Quân",
   "address": "Phố Hồ, Thị trấn Hồ, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0637235,
   "Latitude": 106.0867977
 },
 {
   "STT": 224,
   "Name": " Quầy CTCPTMDV Long Quân",
   "address": "Xã Đạo Tú, Song Hồ, Huyện Thuận Thành, bắc Ninh",
   "Longtitude": 21.0677549,
   "Latitude": 106.0781476
 },
 {
   "STT": 225,
   "Name": "Quầy thuốc Phạm Thị Lý ",
   "address": "Khu Lãm Làng, Phường   Vân Dương, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1509203,
   "Latitude": 106.1037362
 },
 {
   "STT": 226,
   "Name": " Quầy CTCPTMDV Long Quân",
   "address": " Phố Hồ, Thị trấn Hồ, Huyện Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0637235,
   "Latitude": 106.0867977
 },
 {
   "STT": 227,
   "Name": " Quầy CTCPTMDV Long Quân",
   "address": "Số 22 Phố Hồ, Thị trấn Hồ, Huyện Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.05191,
   "Latitude": 106.0902316
 },
 {
   "STT": 228,
   "Name": " Thanh Cầu",
   "address": "Thửa số 115, tờ bản đồ số 1 Ba Huyện, Khu Thượng Khắc Niệm, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1578753,
   "Latitude": 106.0721684
 },
 {
   "STT": 229,
   "Name": "Quầy thuốc bệnh viện đa khoa Gia Bình",
   "address": "Đông Bình, Thị trấn Gia Bình, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0540244,
   "Latitude": 106.1867663
 },
 {
   "STT": 230,
   "Name": "Quầy thuốc số 16",
   "address": "Thôn Hộ Vệ, Lạc Vệ, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1215158,
   "Latitude": 106.0754676
 },
 {
   "STT": 231,
   "Name": "Quầy thuốc CND Tiên Du",
   "address": "Thôn Đoài, Hoàn Sơn, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1065555,
   "Latitude": 106.00304
 },
 {
   "STT": 232,
   "Name": "Quầy thuốc trung tâm CND Tiên Du",
   "address": "Số 7, Đường Lý Thường Kiệt, Thị trấn Lim, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.144745,
   "Latitude": 106.0199355
 },
 {
   "STT": 233,
   "Name": "Quầy thuốc Xuân Thắng",
   "address": "26b Thị trấn Chờ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2005645,
   "Latitude": 105.9560138
 },
 {
   "STT": 234,
   "Name": "Chi nhánh dược Lương Tài",
   "address": "An Mỹ, Mỹ Hương, Lương Tài, Bắc Ninh",
   "Longtitude": 21.0328935,
   "Latitude": 106.2625114
 },
 {
   "STT": 235,
   "Name": "Chi nhánh dược Lương Tài",
   "address": "Thanh Gia, Quảng Phú, Lương Tài, bắc Ninh",
   "Longtitude": 21.0178933,
   "Latitude": 106.1639749
 },
 {
   "STT": 236,
   "Name": "Chi nhánh dược Lương Tài",
   "address": "Thôn Đông Hương, Thị trấn Thứa, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0172952,
   "Latitude": 106.2073544
 },
 {
   "STT": 237,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Văn Phong, Châu phong, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1370612,
   "Latitude": 106.2555354
 },
 {
   "STT": 238,
   "Name": "Chi nhánh dược Lương Tài",
   "address": "Thôn Giàng, Thị trấn Thứa, Lương Tài, Bắc Ninh",
   "Longtitude": 21.0178738,
   "Latitude": 106.2035805
 },
 {
   "STT": 239,
   "Name": "Chi nhánh dược Lương Tài",
   "address": "Ngọc Quan, Lâm Thao, Lương Tài, Bắc Ninh",
   "Longtitude": 20.9734027,
   "Latitude": 106.1676508
 },
 {
   "STT": 240,
   "Name": "Chi nhánh dược Lương Tài",
   "address": "Thôn Quảng Bố, Quảng Phú, Lương Tài, Băc Ninh",
   "Longtitude": 21.0348395,
   "Latitude": 106.1605453
 },
 {
   "STT": 241,
   "Name": "Chi nhánh dược Lương Tài",
   "address": "Hương Trai, Minh Tân, Lương Tài, Bắc Ninh",
   "Longtitude": 20.9930422,
   "Latitude": 106.263247
 },
 {
   "STT": 242,
   "Name": "Chi nhánh dược Lương Tài",
   "address": "Thôn Tĩnh Xá, Phú Hòa, Lương Tài, Bắc Ninh",
   "Longtitude": 21.0368222,
   "Latitude": 106.2263359
 },
 {
   "STT": 243,
   "Name": "Chi nhánh dược Lương Tài",
   "address": "Thôn Đông Hương, Thị trấn Thứa, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0172952,
   "Latitude": 106.2073544
 },
 {
   "STT": 244,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": "Cổng chợ Chờ, Thị trấn Chờ, Huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.1945697,
   "Latitude": 105.9556165
 },
 {
   "STT": 245,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Thôn Nghiêm Thôn, Thị trấn Phố Mới, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1489952,
   "Latitude": 106.1551531
 },
 {
   "STT": 246,
   "Name": "Đại lý CND Tiên Du",
   "address": "Thôn Núi Móng, xã Hoàn Sơn, Huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.2023365,
   "Latitude": 105.925047
 },
 {
   "STT": 247,
   "Name": "Đại lý CND Tiên Du",
   "address": "Thôn Tư Chi, xã Tân Chi, Tiên Du, Bắc Ninh",
   "Longtitude": 21.0958141,
   "Latitude": 106.0794652
 },
 {
   "STT": 248,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": "Yên Lãng, Yên Trung, Yên Phong, Bắc Ninh",
   "Longtitude": 21.208772,
   "Latitude": 105.9942255
 },
 {
   "STT": 249,
   "Name": "Quầy 62 thuộc Chi nhánh Yên phong",
   "address": "Phong Xá, Đông Phong, Yên Phong, bắc Ninh",
   "Longtitude": 21.1947294,
   "Latitude": 106.0158952
 },
 {
   "STT": 250,
   "Name": " Quang Thành",
   "address": "Do Nha, Phương Liễu, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1732871,
   "Latitude": 106.1079848
 },
 {
   "STT": 251,
   "Name": "Quang Thành",
   "address": "67 Huyền Quang, Ninh Xá, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1770439,
   "Latitude": 106.0635426
 },
 {
   "STT": 252,
   "Name": "Quầy thuốc Mạnh Oanh",
   "address": " Ki ốt 7, Chợ trung tâm, Thị trấn Phố Mới, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1521259,
   "Latitude": 106.1500901
 },
 {
   "STT": 253,
   "Name": "Quầy thuốc Hồng Thủy",
   "address": "Quầy thuốc Hồng Thủy Ki ốt 3, dẫy 4 Chợ Phố Mới, Huyện Quế Võ",
   "Longtitude": 21.1521259,
   "Latitude": 106.1500901
 },
 {
   "STT": 254,
   "Name": "Nhà thuốc Long Dung",
   "address": "375 Nguyễn Trãi, Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1692676,
   "Latitude": 106.0637015
 },
 {
   "STT": 255,
   "Name": "Nhà thuốc Đông Hương",
   "address": "Số 90 Trần Lựu, khu 5, Phường  Thị Cầu, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1970652,
   "Latitude": 106.0908333
 },
 {
   "STT": 256,
   "Name": "Quầy thuốc Bình An",
   "address": "Thôn Mẫn Xá, xã Long Châu, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1947753,
   "Latitude": 105.98912
 },
 {
   "STT": 257,
   "Name": "Quầy thuốc Kiên Anh ",
   "address": "Thôn Phù Lưu, xã Trung Nghĩa, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1848285,
   "Latitude": 105.9606781
 },
 {
   "STT": 258,
   "Name": "QuẦY thuốc Minh Tâm",
   "address": "Thôn Nghĩa Chỉ, xã Minh Đạo, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.08715,
   "Latitude": 106.0543885
 },
 {
   "STT": 259,
   "Name": "Quầy thuốc tư nhân Minh Tâm",
   "address": "Ngọc Khám, Gia Đông, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0325881,
   "Latitude": 106.0794491
 },
 {
   "STT": 260,
   "Name": "Quầythuốc Đoàn Kết",
   "address": "Ngọc Khám, Gia Đông, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0325881,
   "Latitude": 106.0794491
 },
 {
   "STT": 261,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "Phố Dâu, Thanh Khương, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0356025,
   "Latitude": 106.0405485
 },
 {
   "STT": 262,
   "Name": "Quầy thuốc Trươờng Thọ ",
   "address": "23 Hai Bà Trưng, Phường  Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1802307,
   "Latitude": 106.0692743
 },
 {
   "STT": 263,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "Phố Dâu, Thanh Khương, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0356025,
   "Latitude": 106.0405485
 },
 {
   "STT": 264,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "Phố Chẹm, Trạm Lộ, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0245727,
   "Latitude": 106.1073606
 },
 {
   "STT": 265,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "Phố Dâu, Thanh Khương, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0356025,
   "Latitude": 106.0405485
 },
 {
   "STT": 266,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "Phố Cống Hà, Hà Mãn, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0317013,
   "Latitude": 106.0360979
 },
 {
   "STT": 267,
   "Name": "Quầy thuốc số 38",
   "address": "Số 48 Phố Vũ, Phường  Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1890234,
   "Latitude": 106.0672746
 },
 {
   "STT": 268,
   "Name": "Quầy chi nhánh Quế Võ",
   "address": "Chợ Nội Doi, Đại Xuân, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1940697,
   "Latitude": 106.1237403
 },
 {
   "STT": 269,
   "Name": "Quầy thuốc số 5 ",
   "address": " Chợ Phủ, thôn Đỉnh, Thị trấn Phố Mới,Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1515056,
   "Latitude": 106.1618381
 },
 {
   "STT": 270,
   "Name": "Quầy thuốc số 51",
   "address": "70 Trần Hưng Đạo, Phường   Tiền An, Thành phố Bắc Ninh",
   "Longtitude": 21.1805592,
   "Latitude": 106.0649358
 },
 {
   "STT": 271,
   "Name": "Quầy thuốc Khương Thị Hải Ninh",
   "address": "K3 Thị trấn phố Mới, khuKi ốt Vật tư nông nghiệp, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1534803,
   "Latitude": 106.1536828
 },
 {
   "STT": 272,
   "Name": "Quầy thuốc Chi nhánh Quế võ",
   "address": "Chợ Chiì, xã Bồng Lai, Huyện Huyện Quế Võ, Bắc ninh",
   "Longtitude": 21.113678,
   "Latitude": 106.1591882
 },
 {
   "STT": 273,
   "Name": "Quầy chi nhánh Quế Võ",
   "address": "Thôn Bồng Lai, xã Bồng Lai, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.117692,
   "Latitude": 106.1600512
 },
 {
   "STT": 274,
   "Name": "Quầy thuốc Tân Dược",
   "address": "Thôn Hoài Thượng, xã Liên Bão, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1302752,
   "Latitude": 106.0245772
 },
 {
   "STT": 275,
   "Name": "Quang Thành",
   "address": "560 Nguyễn Trãi, Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1657046,
   "Latitude": 106.0649593
 },
 {
   "STT": 276,
   "Name": "Quầy thuốc Ngô Thị Thu Hương ",
   "address": "Thôn Long Vỹ, Phường  Đình Bảng, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.0910901,
   "Latitude": 105.9417143
 },
 {
   "STT": 277,
   "Name": "Quầy thuốc Ngọc Hải ",
   "address": "31 Chùa Dận, Phường  Đình Bảng, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1091643,
   "Latitude": 105.9426601
 },
 {
   "STT": 278,
   "Name": "Quầy thuốc Lê Thị Ngọc Anh ",
   "address": "Khu phố Phù Lưu, Phường   Đông Ngàn, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1141283,
   "Latitude": 105.9614584
 },
 {
   "STT": 279,
   "Name": "Quầy thuốc Hiếu Nghĩa ",
   "address": "Khu phố Yên Lã, Phường  Tân Hồng, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1076626,
   "Latitude": 105.971224
 },
 {
   "STT": 280,
   "Name": " Quầy thuốc Thành nam",
   "address": "Thôn Do Nha, xã Phương Liễu, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1732871,
   "Latitude": 106.1079848
 },
 {
   "STT": 281,
   "Name": "Quầy thuốc Nguyễn Thị Thanh ",
   "address": "Thôn Đông Duương, xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1350199,
   "Latitude": 106.1244357
 },
 {
   "STT": 282,
   "Name": "Quầy thuốc số 15 ",
   "address": "Số 373 khu Hòa Đình, Phường  Võ Cường, Thành phố Bắc Ninh",
   "Longtitude": 21.1684776,
   "Latitude": 106.0496172
 },
 {
   "STT": 283,
   "Name": "Nhà thuốc Thiện Nhân",
   "address": "TD số 272 , TS04, Thị trấn Phố Mới, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1534803,
   "Latitude": 106.1536828
 },
 {
   "STT": 284,
   "Name": "Quaầy thuốc Tuấn Thành",
   "address": "Yên Lãng, Yên Trung, Yên Phong, Bắc Ninh",
   "Longtitude": 21.208772,
   "Latitude": 105.9942255
 },
 {
   "STT": 285,
   "Name": "Nhà thuốc Đào Tuất",
   "address": "Số 251 Thị trấn Chờ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1978889,
   "Latitude": 105.9494248
 },
 {
   "STT": 286,
   "Name": "Nhà thuốc",
   "address": "102 Ngô Gia Tự, Ninh Xá, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1804527,
   "Latitude": 106.0620526
 },
 {
   "STT": 287,
   "Name": "Nhà thuốc Tuệ Tĩnh",
   "address": " Đươờng TS 5, KCN Tiên Sơn, xã Hoàn Sơn, Tiên Du, bắc Ninh",
   "Longtitude": 21.1044397,
   "Latitude": 106.0037747
 },
 {
   "STT": 288,
   "Name": "Quầy thuốc Chi Vi",
   "address": " Đông Đoài, xã Đại Đồng Thành, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0441321,
   "Latitude": 106.076088
 },
 {
   "STT": 289,
   "Name": "Quầy thuốc Nam Sơn",
   "address": "Thôn Sơn Đông, xã Nam Sơn, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.141189,
   "Latitude": 106.0930395
 },
 {
   "STT": 290,
   "Name": "Quầy thuốc Vũ Thị Yến",
   "address": "Đông Hương, Thị trấn Thứa, Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0172952,
   "Latitude": 106.2073544
 },
 {
   "STT": 291,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Nghiêm Thôn, Thị trấn Phố Mới, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1478579,
   "Latitude": 106.1545562
 },
 {
   "STT": 292,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Thôn Đỉnh, Thị trấn phố Mới, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1489952,
   "Latitude": 106.1551531
 },
 {
   "STT": 293,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Vệ Xá, Đức Long, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1246101,
   "Latitude": 106.271338
 },
 {
   "STT": 294,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Số 66 Khu 3, Thị trấn Phố Mới, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1534803,
   "Latitude": 106.1536828
 },
 {
   "STT": 295,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Thôn Sau, xã Bằng An, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1351048,
   "Latitude": 106.1897278
 },
 {
   "STT": 296,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Liễn Thượng, Đại Xuân, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1960769,
   "Latitude": 106.125015
 },
 {
   "STT": 297,
   "Name": "Đại lý Chi nhánh dược Quế Võ",
   "address": "Từ phong,cách bi,Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1279659,
   "Latitude": 106.1852958
 },
 {
   "STT": 298,
   "Name": "Quầy thuốc Chi nhánh Quế võ",
   "address": " Thôn Nghiêm Xá, xã Việt Hùng, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1519158,
   "Latitude": 106.1655235
 },
 {
   "STT": 299,
   "Name": "Quầy thuốc Thành Dũng",
   "address": "Thôn Đức Lâm, xã Yên Phụ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1858523,
   "Latitude": 105.9272575
 },
 {
   "STT": 300,
   "Name": "Nhà thuốc",
   "address": "31 Thiên Đức,Vệ an, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1829126,
   "Latitude": 106.0628141
 },
 {
   "STT": 301,
   "Name": " Quầy thuốc Trà My ",
   "address": "Thôn Lũng Sơn, Thị trấn Lim, Huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.1473571,
   "Latitude": 106.022874
 },
 {
   "STT": 302,
   "Name": "Quầy thuốc Mai Phúc",
   "address": "Khương Tự, xã Thanh Khương, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0396317,
   "Latitude": 106.042559
 },
 {
   "STT": 303,
   "Name": "Quầy thuốc Phan Thị Thơ",
   "address": "Khu Chu Mẫu, Phường   Vân Dương, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1649355,
   "Latitude": 106.0964516
 },
 {
   "STT": 304,
   "Name": "Quầy thuốc Chỉnh Anh",
   "address": "Thôn Yên Vỹ, xã Hòa Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1996406,
   "Latitude": 105.9173359
 },
 {
   "STT": 305,
   "Name": "QUẦY THUỐC HẠNH HOÀ",
   "address": "Thôn Núi Móng, xã Hoàn Sơn, Huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.2023365,
   "Latitude": 105.925047
 },
 {
   "STT": 306,
   "Name": "Quầy thuốc Quỳnh Hoa",
   "address": "Kho Lương thực , Thị trấn Chờ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.0528274,
   "Latitude": 106.0914872
 },
 {
   "STT": 307,
   "Name": "Quầy thuốc số 27",
   "address": "Khu Đoài, Phường  Khắc Niệm, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1314843,
   "Latitude": 106.0580066
 },
 {
   "STT": 308,
   "Name": "Quang Thành",
   "address": "Thôn Do Nha, xã Phương Liễu, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1732871,
   "Latitude": 106.1079848
 },
 {
   "STT": 309,
   "Name": "Quầy thuốc Hồng Hải ",
   "address": "Thôn Doi Sóc, xã Phù Chẩn, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.0924082,
   "Latitude": 105.9811588
 },
 {
   "STT": 310,
   "Name": "Quang Thành",
   "address": "Ki ot B4, Chợ Suois Hoa, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1832333,
   "Latitude": 106.0715333
 },
 {
   "STT": 311,
   "Name": "Quầy thuốc Đỗ Đình Long",
   "address": "Chợ Sơn, Việt Đoàn, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1096804,
   "Latitude": 106.0366384
 },
 {
   "STT": 312,
   "Name": "Quầy thuocs Tiến Đạt",
   "address": "81 Nguyễn Trãi, Ninh Xá, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1762616,
   "Latitude": 106.0601135
 },
 {
   "STT": 313,
   "Name": "Quầy thuốc Đặng Tuyết",
   "address": "Thôn Ngô Xá, Long Châu, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1920145,
   "Latitude": 105.9942055
 },
 {
   "STT": 314,
   "Name": "Quầy thuốc Bình An",
   "address": "Thôn Ô Cách, xã Đông Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 315,
   "Name": "Quầy thuốc Bảo Châu",
   "address": "Phố Mới, xã Thanh Khương, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0437995,
   "Latitude": 106.0757749
 },
 {
   "STT": 316,
   "Name": "Quầy thuốc Thiện Doan",
   "address": "Thôn Đông Hương, Thị trấn Thứa, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0172952,
   "Latitude": 106.2073544
 },
 {
   "STT": 317,
   "Name": "Quầy thuốc Tuyết Công ",
   "address": "Lãm Làng, Phường  Vân Dương, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.14966,
   "Latitude": 106.1037006
 },
 {
   "STT": 318,
   "Name": "Quầy thuốc An Khang",
   "address": "Xuân Viên, xã Hoà Long, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.2081087,
   "Latitude": 106.0510366
 },
 {
   "STT": 319,
   "Name": "Quầy thuốc Ngọc Diệp",
   "address": "Thôn Xuân Dương, xã Vạn Ninh, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.1031336,
   "Latitude": 106.2647223
 },
 {
   "STT": 320,
   "Name": "Quầy thuốc Thảo Kiên ",
   "address": "Số 73, ngõ 3, Trần Phú, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1201533,
   "Latitude": 105.9638048
 },
 {
   "STT": 321,
   "Name": "Quầy thuốc Tuấn Thành",
   "address": "Thôn Đông Du, xã Đào Viên, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1154094,
   "Latitude": 106.2064648
 },
 {
   "STT": 322,
   "Name": "Quầy thuốc Bình An",
   "address": "Thôn Trần Xá, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.209834,
   "Latitude": 105.986279
 },
 {
   "STT": 323,
   "Name": "Quầy thuốc Lê Xuân",
   "address": "Ki ôốt 6, thôn Đại Thượng, xã Đại Đồng, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.0837543,
   "Latitude": 105.991829
 },
 {
   "STT": 324,
   "Name": " Thanh Cầu",
   "address": "Châu Cầu, Châu Phong, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1293042,
   "Latitude": 106.245595
 },
 {
   "STT": 325,
   "Name": "Nhà thuốc Bệnh viện",
   "address": "Nhà C1 , nhà thuốc Bệnh viện đa khoa Tỉnh",
   "Longtitude": 20.7033236,
   "Latitude": 101.9976497
 },
 {
   "STT": 326,
   "Name": "Quaầy thuốc Dương Thị Thoa",
   "address": "Thôn Chi Hồ, xã Tân Chi, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1178311,
   "Latitude": 106.0287512
 },
 {
   "STT": 327,
   "Name": "Quầy thuốc Ngọc Anh",
   "address": "Traần Xá, Yên Trung, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2080337,
   "Latitude": 105.9868804
 },
 {
   "STT": 328,
   "Name": "Nhà thuốc Bệnh viện Da Liễu",
   "address": "Bệnh viện Phong Da Liễu, xã Hòa Long, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.2081087,
   "Latitude": 106.0510366
 },
 {
   "STT": 329,
   "Name": "Quầy thuốc Phạm Lan",
   "address": "Đạo Sử, Thị trấn Thứa, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0154239,
   "Latitude": 106.1964643
 },
 {
   "STT": 330,
   "Name": "Quầy thuốc số 35",
   "address": "Số 107 Hồ Ngọc Lân, Phường  Kinh Bắc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1876389,
   "Latitude": 106.0643611
 },
 {
   "STT": 331,
   "Name": "Quầy thuốc Nguyễn Thị Loan- CND Quế Võ",
   "address": " Đông Du, Đào Viên, Huyện Quế Võ, Bac Ninh",
   "Longtitude": 21.1184411,
   "Latitude": 106.1905816
 },
 {
   "STT": 332,
   "Name": "QuẦY thuốc Nguyễn Thị Huyền- CND Quế Võ",
   "address": "Ki oốt 10C, Chợ trung tâm, Thị trấn Phố Mới, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1521259,
   "Latitude": 106.1500901
 },
 {
   "STT": 333,
   "Name": "Quầy thuốc Nguyễn Thị Hiền- CND Quế Võ",
   "address": "Khu 4, Thị trấn Phố Mới, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1516755,
   "Latitude": 106.154881
 },
 {
   "STT": 334,
   "Name": "Quầy thuốc Đặng Thị Phương- CND Quế Võ",
   "address": "Thành Dền, Đào Viên, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1380669,
   "Latitude": 106.1819172
 },
 {
   "STT": 335,
   "Name": "Hằng Tài",
   "address": "Chợ chiều, Hòa Đình, Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1715513,
   "Latitude": 106.0522431
 },
 {
   "STT": 336,
   "Name": "Hằng Tài",
   "address": "382, Nguyễn Trãi, Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1696615,
   "Latitude": 106.0632748
 },
 {
   "STT": 337,
   "Name": "Quầy thuốc Linh Ngọc ",
   "address": "Viêm Xá, Hòa Long, Thành phố Bắc Ninh",
   "Longtitude": 21.2149481,
   "Latitude": 106.0500574
 },
 {
   "STT": 338,
   "Name": "Quầy thuốc Khoa Nguyệt",
   "address": "Khu Dương Lôi, Phường   Tân Hồng, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1134455,
   "Latitude": 105.9765977
 },
 {
   "STT": 339,
   "Name": "Quầy thuốc linh Thịnh",
   "address": "Số 9, xóm Rừng, Trang Liệt, Trang Hạ, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1210851,
   "Latitude": 105.9536386
 },
 {
   "STT": 340,
   "Name": "Quầy thuốc Gia Lâm ",
   "address": "Khu phố Tân Lập, Phường  Đình Bảng, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1015708,
   "Latitude": 105.9378513
 },
 {
   "STT": 341,
   "Name": "Quầy thuốc Thanh Trà",
   "address": "Khu phố Đình, Phường  Đình Bảng, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1086496,
   "Latitude": 105.9521059
 },
 {
   "STT": 342,
   "Name": "Quầy thuốc Giao Bình",
   "address": "Khu phố Thọ Môn, Phường  Đình Bảng, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1106794,
   "Latitude": 105.9509991
 },
 {
   "STT": 343,
   "Name": "QuẦY thuốc Tuấn Chiền",
   "address": "Thôn Đông Yên, xã Đông Phong, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1891678,
   "Latitude": 106.0061096
 },
 {
   "STT": 344,
   "Name": "Quầy thuốc Nguyễn Thị Tuyến",
   "address": "Cảnh Hưng, Tiên Du, Bắc Ninh",
   "Longtitude": 21.0767242,
   "Latitude": 106.0346285
 },
 {
   "STT": 345,
   "Name": "Quầy thuốc Nhân Hòa",
   "address": "Thôn Chính Trung, xã Yên Trung, Huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.2198182,
   "Latitude": 105.9847188
 },
 {
   "STT": 346,
   "Name": "Quầy thuốc Long Thành",
   "address": "Thôn Ô Cách, xã Đông Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 347,
   "Name": "thuốc Nguyễn Tiến Hải",
   "address": "Thôn Đại Thượng, Đại Đồng, Huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.0837543,
   "Latitude": 105.991829
 },
 {
   "STT": 348,
   "Name": "Quầy thuốc Tuyết Hương ",
   "address": "Chợ Ve, Tri Phương,Tiên Du, Bắc Ninh",
   "Longtitude": 21.083449,
   "Latitude": 106.0044783
 },
 {
   "STT": 349,
   "Name": "Quầy thuốc Cường Nguyệt",
   "address": "179 Chợ Sơn, Việt Đoàn, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1096804,
   "Latitude": 106.0366384
 },
 {
   "STT": 350,
   "Name": "Quầy thuốc Mở Hồng ",
   "address": "154 Lý Thường Kiệt, Thị trấn Lim, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.4563671,
   "Latitude": 107.7613292
 },
 {
   "STT": 351,
   "Name": "Quầy thuốc số 9 ",
   "address": "Xuân Ổ B, Phường  Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1594781,
   "Latitude": 106.0397713
 },
 {
   "STT": 352,
   "Name": "Quang Thành",
   "address": "Số 13, khu 7, Phường  Thị Cầu, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1700796,
   "Latitude": 106.0834908
 },
 {
   "STT": 353,
   "Name": "Quầy thuốc số 03 ",
   "address": "Số 26 Trần Lựu, Khu 5, Phường  Thị Cầu, Thành phố Bắc Ninh",
   "Longtitude": 21.196916,
   "Latitude": 106.0893917
 },
 {
   "STT": 354,
   "Name": "Quầy thuốc Việt số 1",
   "address": "Số 17, Bờ ngang, Phường  Đình Bảng, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1086351,
   "Latitude": 105.9554177
 },
 {
   "STT": 355,
   "Name": "Quầy tthuôc Quang Anh ",
   "address": "82 Tô Hiến Thành, Phường  Đình Bảng, Thị xã Từ Sơn, Bắc ninh",
   "Longtitude": 21.1111756,
   "Latitude": 105.9561223
 },
 {
   "STT": 356,
   "Name": "Quầy thuốc số 7",
   "address": "Số 168 Lê Phụng Hiểu, Phường   Vệ An, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1864975,
   "Latitude": 106.0582512
 },
 {
   "STT": 357,
   "Name": "Quầy thuốc Hà Định",
   "address": "Âp Đồn, Yên Trung, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2068977,
   "Latitude": 105.9986327
 },
 {
   "STT": 358,
   "Name": "Quầy thuốc Hồng Vân",
   "address": "Khu Vạn Phúc, Phường  Vạn An, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1973549,
   "Latitude": 106.0610115
 },
 {
   "STT": 359,
   "Name": "Quầy thuốc Hải Đăng",
   "address": "Xóm Chanh, Phường  Vạn An, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1960183,
   "Latitude": 106.0520698
 },
 {
   "STT": 360,
   "Name": "Quầy thuốc Thanh Long",
   "address": "Số 219 Chợ Sơn, Việt Đoàn, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1096804,
   "Latitude": 106.0366384
 },
 {
   "STT": 361,
   "Name": "Quầy thuốc Hoàng Trí",
   "address": "Thôn Nguyệt Cầu, xã Tam Giang, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1941429,
   "Latitude": 105.9582372
 },
 {
   "STT": 362,
   "Name": "QuẦY thuốc Huyền Lê",
   "address": "Số 45 Nguyễn Đăng Đạo, Thị trấn Lim, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1424048,
   "Latitude": 106.0204857
 },
 {
   "STT": 363,
   "Name": "Quầy thuốc số 28 ",
   "address": " Đường 286, Khu Đương Xá, Phường   Vạn An, Thành phố Bắc Ninh",
   "Longtitude": 21.1953727,
   "Latitude": 106.0497513
 },
 {
   "STT": 364,
   "Name": "Nhà thuốc GPP Đồng Kỵ",
   "address": "Xóm Bằng, Phường  Đồng Kỵ, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1370903,
   "Latitude": 105.9430696
 },
 {
   "STT": 365,
   "Name": "Quầy thuốc Hằng Thủy 1",
   "address": "Chu Mẫu, Phường   Vân Dương, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1656447,
   "Latitude": 106.0956164
 },
 {
   "STT": 366,
   "Name": "Quầy thuốc Anh Đức",
   "address": "Thôn Mai Động, xã Hương Mạc, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1535168,
   "Latitude": 105.9446812
 },
 {
   "STT": 367,
   "Name": "Quầy thuốc tân dực Bảo Minh",
   "address": "Thôn Vũ Dương, xã Bồng Lai, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.117692,
   "Latitude": 106.1600512
 },
 {
   "STT": 368,
   "Name": "Quầy thuốc Phạm Thị Hoa",
   "address": "Thôn Nghiêm Xá, Thị trấn Chờ, Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.188969,
   "Latitude": 105.955559
 },
 {
   "STT": 369,
   "Name": "Quầy thuốc Vương Thị Oanh",
   "address": "Hoàng Xá,Ninh Xá, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0136841,
   "Latitude": 106.1103151
 },
 {
   "STT": 370,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "Phố Khám, Gia Đông, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0437995,
   "Latitude": 106.0757749
 },
 {
   "STT": 371,
   "Name": "Nhà thuốc Hồng Thắng",
   "address": "18 Nguyễn Văn Cừ, Ninh Xá,,Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.17724,
   "Latitude": 106.0579059
 },
 {
   "STT": 372,
   "Name": "Quầy thuốc Hà linh",
   "address": "Thôn Phong Xá, xã Đông Phong, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1956876,
   "Latitude": 106.0150623
 },
 {
   "STT": 373,
   "Name": "Quầy thuốc Chi nhánh Yên Phong",
   "address": "Thôn Chi Long, xã Long Châu, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1941429,
   "Latitude": 105.9582372
 },
 {
   "STT": 374,
   "Name": "Quầy thuốc Tân Dược",
   "address": "Đông Yên, Đông Phong, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1883842,
   "Latitude": 106.0103857
 },
 {
   "STT": 375,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "Phố Hồ, Thị trấn Hồ, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0637235,
   "Latitude": 106.0867977
 },
 {
   "STT": 376,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "Khu Đ DTB, Ninh Xá, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0131464,
   "Latitude": 106.0934117
 },
 {
   "STT": 377,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "Phố Hồ, Thị trấn Hồ, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0637235,
   "Latitude": 106.0867977
 },
 {
   "STT": 378,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "Phố Khám, Gia Đông, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0437995,
   "Latitude": 106.0757749
 },
 {
   "STT": 379,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "Phố Mới, Thị trấn hồ, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0510725,
   "Latitude": 106.087998
 },
 {
   "STT": 380,
   "Name": "Quầy thuốc 26 ",
   "address": " Chợ Yên Mẫn, Phường   kinh Bắc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1871286,
   "Latitude": 106.0585653
 },
 {
   "STT": 381,
   "Name": "Nhà thuốc Bệnh viên Mắt Bắc Ninh",
   "address": "Bệnh viện Mắt Bắc Ninh, Phường  Hạp Lĩnh, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1326224,
   "Latitude": 106.076673
 },
 {
   "STT": 382,
   "Name": "Quầy thuốc chi nhánh",
   "address": "Thôn Đoông, Tam Giang, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2282197,
   "Latitude": 105.937977
 },
 {
   "STT": 383,
   "Name": "Quầy thuốc Chi nhánh Yên Phong",
   "address": "Thôn Chính Trung, xã Yên Trung, Huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.2198182,
   "Latitude": 105.9847188
 },
 {
   "STT": 384,
   "Name": " Quầy thuốc số 51",
   "address": "Thôn Yên Lãng, Yên Trung, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2092511,
   "Latitude": 105.9936476
 },
 {
   "STT": 385,
   "Name": "Quầy thuốc Thu Thanh",
   "address": "Khu Nam Hồng, Phường  Đình Bảng, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.0843,
   "Latitude": 105.9412
 },
 {
   "STT": 386,
   "Name": "Quầy thuốc Quang Anh",
   "address": "Phố Khám, Gia Đông, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0437995,
   "Latitude": 106.0757749
 },
 {
   "STT": 387,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": "Trần Xá, Yên Trung, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2080337,
   "Latitude": 105.9868804
 },
 {
   "STT": 388,
   "Name": "Quầy thuốc 55 ",
   "address": "Thôn Lương Tân, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2202724,
   "Latitude": 105.9876149
 },
 {
   "STT": 389,
   "Name": "Quầy thuốc Thu Trang",
   "address": "Ki ốt tầng 1, thuộc Công ty XNK Từ Sơn, Phường  Đông Ngàn, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1156528,
   "Latitude": 105.9611748
 },
 {
   "STT": 390,
   "Name": "Quầy thuốc Chi nhánh Yên phong",
   "address": "Thôn Đông, Tam Giang, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2282197,
   "Latitude": 105.937977
 },
 {
   "STT": 391,
   "Name": "QuẦY thuốc 62",
   "address": "Số 62 Phố mới Đồng Nguyên, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1319011,
   "Latitude": 105.9729255
 },
 {
   "STT": 392,
   "Name": "Quầy thuốc số 12",
   "address": "Thôn Đồng Thôn, xã Đông Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 393,
   "Name": "Quầy số 36",
   "address": "Thôn Đông Yên, xã Đông Phong, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1891678,
   "Latitude": 106.0061096
 },
 {
   "STT": 394,
   "Name": "Quầy thuốc Hoàng thị Thường",
   "address": "Thôn Hữu Bằng, xã Ngọc Xá, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1344283,
   "Latitude": 106.2220103
 },
 {
   "STT": 395,
   "Name": "Quầy thuốc số 11",
   "address": "Thôn Đông,Tam Giang, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2282197,
   "Latitude": 105.937977
 },
 {
   "STT": 396,
   "Name": "Quầy thuốc Thúy Hằng",
   "address": "Dốc Cầu vượt, Liên Bão, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1273696,
   "Latitude": 106.0199355
 },
 {
   "STT": 397,
   "Name": "Quầy thuốc CN PKĐK Tâm Phúc",
   "address": "Thôn Đỉnh, Thị trấn phố Mới, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1489952,
   "Latitude": 106.1551531
 },
 {
   "STT": 398,
   "Name": "Quầy thuốc số 2",
   "address": "Số 473 Ngô Gia Tự, Phường  Tiền An, Thành phố Bắc Ninh",
   "Longtitude": 21.1815259,
   "Latitude": 106.0634673
 },
 {
   "STT": 399,
   "Name": "Quầy thuốc CC",
   "address": "Số 278 Đường Nguyễn Trãi, Phường  Võ Cường, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.128636,
   "Latitude": 106.0763849
 },
 {
   "STT": 400,
   "Name": "Quầy thuốc Thúy Sơn",
   "address": "Văn Môn, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1764105,
   "Latitude": 105.9318012
 },
 {
   "STT": 401,
   "Name": "Quầy số 25",
   "address": "140 Phố Vũ, Phường   Đại Phúc, Thành phố Bắc Ninh",
   "Longtitude": 21.1738006,
   "Latitude": 106.0787142
 },
 {
   "STT": 402,
   "Name": "Quầy thuốc Trần Thị Hiên",
   "address": "Thôn Vũ Dương, xã Bồng Lai, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.117692,
   "Latitude": 106.1600512
 },
 {
   "STT": 403,
   "Name": "Quầy thuốc Trần Hồng",
   "address": "Thôn Đại Lâm, xã Tam Đa, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2106744,
   "Latitude": 106.0346163
 },
 {
   "STT": 404,
   "Name": "Nhà Thuốc Hoàn Mỹ",
   "address": "469 Nguyễn Trãi, Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1673239,
   "Latitude": 106.0646226
 },
 {
   "STT": 405,
   "Name": "Quầy thuốc Hợp Hường",
   "address": "Thôn Hương Mạc, Xã Hương Mạc, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1554786,
   "Latitude": 105.9310669
 },
 {
   "STT": 406,
   "Name": "Quầy thuốc- CND Từ Sơn",
   "address": "Số 9, lô 14, KCN Tân Hồng, Hoàn Sơn, phuowng Tân Hồng, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1132376,
   "Latitude": 105.979979
 },
 {
   "STT": 407,
   "Name": "Quầy thuốc Thùy Dương",
   "address": "Thôn Doi Sóc, xã Phù Chẩn, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.0924082,
   "Latitude": 105.9811588
 },
 {
   "STT": 408,
   "Name": "Quầy thuốc Bình Dân số 68",
   "address": "Thôn Đỉnh, Thị trấn phố Mới, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1489952,
   "Latitude": 106.1551531
 },
 {
   "STT": 409,
   "Name": "Quầy thuốc Nguyễn Thị Nhung",
   "address": "Thôn Rích, xã Phù Chẩn, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1196529,
   "Latitude": 105.9623161
 },
 {
   "STT": 410,
   "Name": "Quầy thuốc Nguyễn Thu Hiền",
   "address": "Thôn Tiêu Long, xã Tương Giang, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1369323,
   "Latitude": 105.9883494
 },
 {
   "STT": 411,
   "Name": "Quầy thuốc Thúy Nhị ",
   "address": "Chợ Dầu, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1171634,
   "Latitude": 105.961512
 },
 {
   "STT": 412,
   "Name": "Nhà thuốc Anh Thu",
   "address": "Số 36A Đường Lê Phụng Hiểu, Phường  Vệ An, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1843295,
   "Latitude": 106.0554113
 },
 {
   "STT": 413,
   "Name": " Quầy Chi nhánh Yên Phong",
   "address": "Thôn Nghiêm Xá, Thị trấn Chờ, Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.188969,
   "Latitude": 105.955559
 },
 {
   "STT": 414,
   "Name": "Quầy thuốc Nguyễn Thị Thủy",
   "address": "Thôn Do Nha, xã Phương Liễu, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1732871,
   "Latitude": 106.1079848
 },
 {
   "STT": 415,
   "Name": "Quầy thuốc Trung Hường",
   "address": "Thôn Đa Cấu, xã Nam Sơn, Thành phố Bắc Ninh",
   "Longtitude": 21.1424416,
   "Latitude": 106.1183999
 },
 {
   "STT": 416,
   "Name": "Quầy thuốc Thanh Nga",
   "address": "Số 148 Phố Vũ, Phường  Đại Phúc, Thành phố Bắc Ninh",
   "Longtitude": 21.1738006,
   "Latitude": 106.0787142
 },
 {
   "STT": 417,
   "Name": "Quầy thuốc 41",
   "address": "Thôn Nghiêm Xá, Thị trấn Chờ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1915143,
   "Latitude": 105.9519768
 },
 {
   "STT": 418,
   "Name": "Quầy thuốc Hoàng Hạnh",
   "address": "Thôn Núi Móng, xã Hoàn Sơn, Huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.1253797,
   "Latitude": 106.00304
 },
 {
   "STT": 419,
   "Name": "Nhà Thuốc MH Suối Hoa",
   "address": "B18, Chợ Suối Hoa, Phường  Suối Hoa, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1873328,
   "Latitude": 106.0716462
 },
 {
   "STT": 420,
   "Name": "Quầy thuốc Nguyễn Lan Phương ",
   "address": "Đội 3, thôn Ô Cách, xã Đông Tiến, Huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 421,
   "Name": "Quầy thuốc Minh Nguyệt",
   "address": "Khả Lễ, Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1626835,
   "Latitude": 106.0515269
 },
 {
   "STT": 422,
   "Name": "Quầy thuốc số 86",
   "address": "Thôn Ap Đồn, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.205393,
   "Latitude": 105.998216
 },
 {
   "STT": 423,
   "Name": "Quầy thuốc Tiến Phượng",
   "address": "Xóm Tây, Phường  Trang Hạ, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1288596,
   "Latitude": 105.9519986
 },
 {
   "STT": 424,
   "Name": "Quầy thuốc Tuấn Hải ",
   "address": "Phố Viềng, Phường  Đồng Nguyên, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1301102,
   "Latitude": 105.9764767
 },
 {
   "STT": 425,
   "Name": "Quầy thuốc Thuận An",
   "address": "Phoố mới Văn Quan, xã Trí Quả, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0487351,
   "Latitude": 106.029068
 },
 {
   "STT": 426,
   "Name": "Quầy thuốc số 15",
   "address": "Số 56 Nguyễn Đăng Đạo, Khu 10, Phường  Đại Phúc, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1744952,
   "Latitude": 106.0722226
 },
 {
   "STT": 427,
   "Name": "Quầy thuốc Bách Nhân",
   "address": "Thôn Đại Lan, xã Tam Đa, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2235133,
   "Latitude": 106.0321949
 },
 {
   "STT": 428,
   "Name": "Quầy thuốc Nguyễn Thị Thu",
   "address": "Thôn Phương Liễu, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1582245,
   "Latitude": 106.1303886
 },
 {
   "STT": 429,
   "Name": "Quầy thuốc Phạm Thị Nhung",
   "address": "Thôn Phấn Trung, xã Phù Lãng, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1499053,
   "Latitude": 106.2542256
 },
 {
   "STT": 430,
   "Name": "Quầy thuốc Nguyễn Thị Liên",
   "address": "Thôn Đỉnh, Thị trấn Phố Mới, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1534803,
   "Latitude": 106.1536828
 },
 {
   "STT": 431,
   "Name": "Quầy thuốc thuộc  Công ty cổ phần dược phẩm Đại Phúc Bắc Ninh",
   "address": "Nguyễn Trãi, Võ Cường, Thành phố Bắc Ninh",
   "Longtitude": 21.1708975,
   "Latitude": 106.0626898
 },
 {
   "STT": 432,
   "Name": "Quầy thuốc số1",
   "address": "Đường 925, xã Hoàn Sơn, Huyện Tiên Du, Bắc Ninh",
   "Longtitude": 21.0997641,
   "Latitude": 106.0020656
 },
 {
   "STT": 433,
   "Name": "Quầy thuốc Hùng Tâm",
   "address": "Thôn Hoài Thượng, xã Liên Bão, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1302752,
   "Latitude": 106.0245772
 },
 {
   "STT": 434,
   "Name": "Quầy thuốc Liên Hoa ",
   "address": "Phù Lưu, Phường  Đông Ngàn, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1143535,
   "Latitude": 105.9618634
 },
 {
   "STT": 435,
   "Name": "Quầy thuốc Vân Hà",
   "address": "Thôn Lạc Trung, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1949057,
   "Latitude": 105.9593153
 },
 {
   "STT": 436,
   "Name": "Quầy thuốc Thanh Phương ",
   "address": "Thôn Phú mẫn, Thị trấn Chờ, Huyện Yên Phong, Bắc Ninh",
   "Longtitude": 21.1915293,
   "Latitude": 105.9552997
 },
 {
   "STT": 437,
   "Name": "Quầy thuốc Tâm An",
   "address": "Thôn Đông Yên, xã Đông Phong, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1891678,
   "Latitude": 106.0061096
 },
 {
   "STT": 438,
   "Name": "Quầy thuốc Quang Hải",
   "address": "Thôn Trần Xá, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.209834,
   "Latitude": 105.986279
 },
 {
   "STT": 439,
   "Name": "Quầy thuốc Lan Anh",
   "address": "Thôn Dương Ổ, xã Phong Khê, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1643906,
   "Latitude": 106.0338939
 },
 {
   "STT": 440,
   "Name": "Quầy Thuốc Tâm Đức",
   "address": "Thôn Đa Cấu, xã Nam Sơn, Thành phố Bắc Ninh",
   "Longtitude": 21.1424416,
   "Latitude": 106.1183999
 },
 {
   "STT": 441,
   "Name": "Quầy thuốc số 10",
   "address": "Số nhà 34 khu 2, Phường  Thị Cầu, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1752786,
   "Latitude": 106.0746726
 },
 {
   "STT": 442,
   "Name": "Quầy thuốc số 2 ",
   "address": "Số 954 khu Đương Xá, Phường  Vạn An, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1953727,
   "Latitude": 106.0497513
 },
 {
   "STT": 443,
   "Name": "Quầy thuốc số 33 ",
   "address": "Số 33 Trần Lựu, Phường  Thị Cầu, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1968238,
   "Latitude": 106.0884165
 },
 {
   "STT": 444,
   "Name": "Quầy thuốc Thu Nguyên",
   "address": "Thôn Ấp Đồn, Yên Trung, Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.205393,
   "Latitude": 105.998216
 },
 {
   "STT": 445,
   "Name": "Quầy thuốc số 4",
   "address": "Số 206 phố Chợ Sơn, xã Việt Đoàn, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1096804,
   "Latitude": 106.0366384
 },
 {
   "STT": 446,
   "Name": "Quầy thuốc nguyễn Thị Vượng",
   "address": "Thôn Vân Khám, xã Hiên Vân, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1179312,
   "Latitude": 106.0292662
 },
 {
   "STT": 447,
   "Name": "Quầy thuốc Trần Thi Dung",
   "address": "Số 109 Chợ Sơn, Việt Đoàn, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1096804,
   "Latitude": 106.0366384
 },
 {
   "STT": 448,
   "Name": "Nhà thuốc Thang Bảng",
   "address": "22 Đấu Mã, Phường   Thị Cầu, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1928168,
   "Latitude": 106.086948
 },
 {
   "STT": 449,
   "Name": "Quầy thuốc Bảo Ngọc",
   "address": "Thôn Ấp Đồn, xã Yên Trung, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2062766,
   "Latitude": 105.9984151
 },
 {
   "STT": 450,
   "Name": "Quầy thuốc Hương Quỳnh",
   "address": "Chợ Châu Cầu, Châu Phong, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1273541,
   "Latitude": 106.2461337
 },
 {
   "STT": 451,
   "Name": "Quầy thuốc Phong Hoa",
   "address": "Khu 4, Thị trấn Phố Mới, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1516755,
   "Latitude": 106.154881
 },
 {
   "STT": 452,
   "Name": "Nhà thuốc Hải Đăng",
   "address": "Trần Phú, Đông NgànThị xã Từ SơnTỉnh Bắc Ninh",
   "Longtitude": 21.1168619,
   "Latitude": 105.957686
 },
 {
   "STT": 453,
   "Name": "Quầy thuốc Tư nhân",
   "address": "Thôn Nội Phú, Thị trấn Gia Bình, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0540878,
   "Latitude": 106.1786787
 },
 {
   "STT": 454,
   "Name": "Quầy số 9 ",
   "address": "Số 43, Trần Lựu, Phường  Thị Cầu, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.196876,
   "Latitude": 106.0886997
 },
 {
   "STT": 455,
   "Name": "Quầy thuốc chi nhánh thành phố",
   "address": "Số 10 Khu 6, Phường  Đáp cầu, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.2026299,
   "Latitude": 106.0948815
 },
 {
   "STT": 456,
   "Name": "Quầy thuốc số 48 ",
   "address": "Đường 286 , Phường  Kinh Bắc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1888172,
   "Latitude": 106.0638342
 },
 {
   "STT": 457,
   "Name": "Quầy thuốc Hương Giang",
   "address": "Chợ Vũ Ninh, Phường   Vũ Ninh, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1904816,
   "Latitude": 106.0786656
 },
 {
   "STT": 458,
   "Name": "Quầy thuốc Nhân Đức",
   "address": "Khu Lãm Làng, Phường   Vân Dương, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1509203,
   "Latitude": 106.1037362
 },
 {
   "STT": 459,
   "Name": "Công ty đại phúc quầy 03",
   "address": "Phố Và, Hạp Lĩnh, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.137099,
   "Latitude": 106.0765097
 },
 {
   "STT": 460,
   "Name": "Quầy Hằng Khoa",
   "address": "Thôn Hộ Vệ, Lạc Vệ, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1215158,
   "Latitude": 106.0754676
 },
 {
   "STT": 461,
   "Name": "Quầy thuốc Hà Thành",
   "address": "Khu phố Đa Hội, Phường  Châu Khê, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.124253,
   "Latitude": 105.9220297
 },
 {
   "STT": 462,
   "Name": "Quầy thuốc Nam Anh",
   "address": "Khu phố Trịnh Xá, Phường  Châu Khê, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1130875,
   "Latitude": 105.934124
 },
 {
   "STT": 463,
   "Name": "Nhà thuốc BVĐK Kinh Bắc",
   "address": "Nhà Thuốc BVĐK Kinh Bắc 310 Trần Hưng Đạo, Phường  Đại Phúc, Thành phố Bắc Ninh",
   "Longtitude": 21.1769019,
   "Latitude": 106.0735635
 },
 {
   "STT": 464,
   "Name": "Quầy thuốc Hoàng Lan",
   "address": "Thôn Ô Cách, xã Đông Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 465,
   "Name": "Quầy thuốc Dương Thị Nhuận",
   "address": "Chợ Tiêu, Tương Giang, Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1378051,
   "Latitude": 105.9825157
 },
 {
   "STT": 466,
   "Name": "Quầy thuốc Ngọc Hà",
   "address": "Khu1 Thị trấn Phố Mới, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1525162,
   "Latitude": 106.1541724
 },
 {
   "STT": 467,
   "Name": "Quầy thuốc Hiếu Anh 1",
   "address": "Lãm Làng, Phường  Vân Dương, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.14966,
   "Latitude": 106.1037006
 },
 {
   "STT": 468,
   "Name": "Nhà thuốc Tâm Đức",
   "address": "Phoố Dâu, xã Thanh Khương, Huyện Huyện Thuận Thành, tỉn Bắc Ninh",
   "Longtitude": 21.0356578,
   "Latitude": 106.0425835
 },
 {
   "STT": 469,
   "Name": "Quầy thuốc Hiếu Anh",
   "address": "Cổng chợ Lãm Làng, Phường  Vân Dương, Thành phố Bắc Ninh",
   "Longtitude": 21.1520422,
   "Latitude": 106.1025114
 },
 {
   "STT": 470,
   "Name": "Quầy thuốc Tân Dược",
   "address": "Số 59 Trần Phú, Phường  Đông Ngàn, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1193785,
   "Latitude": 105.9624191
 },
 {
   "STT": 471,
   "Name": "Quầy thuốc số 55",
   "address": "DĐường Tô Hiến Thành, Phường  Kinh Bắc, Thành phố Bắc Ninh",
   "Longtitude": 21.1961914,
   "Latitude": 106.064886
 },
 {
   "STT": 472,
   "Name": "Quầy thuốc Đa Hội",
   "address": "Xóm Giếng Châu Khê, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1184735,
   "Latitude": 105.9318012
 },
 {
   "STT": 473,
   "Name": "Quầy thuốc Trung Hiếu",
   "address": "Đường Nguyên Phi Ỷ Lan, Phường  Tân Hồng, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.119586,
   "Latitude": 105.9810106
 },
 {
   "STT": 474,
   "Name": "Quầy thuốc Mạnh Vân",
   "address": "Thôn Rích, xã Phù Chẩn, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1196529,
   "Latitude": 105.9623161
 },
 {
   "STT": 475,
   "Name": "Quầy thuốc số 18 ",
   "address": "Số 129 Hồ Ngọc Lân, Thị Chung, Phường   Kinh Bắc, Thành phố Bắc Ninh",
   "Longtitude": 21.1878337,
   "Latitude": 106.0620016
 },
 {
   "STT": 476,
   "Name": "Quầy thuốc Việt Quyên",
   "address": "Thôn Đông Yen, xã Đông Phong, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 477,
   "Name": "Quầy thuốc Long Phương",
   "address": "Thượng Thôn, Đông Tiến, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2158557,
   "Latitude": 105.9696205
 },
 {
   "STT": 478,
   "Name": "Quầy thuốc Thu Lanh",
   "address": "Phú Mẫn , Thị trấn Chờ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1915293,
   "Latitude": 105.9552997
 },
 {
   "STT": 479,
   "Name": "Quầy thuốc Luyến Bắc",
   "address": "Thôn Đức Lâm, xã Yên Phụ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1858523,
   "Latitude": 105.9272575
 },
 {
   "STT": 480,
   "Name": "Quầy thuốc số 38",
   "address": "Thôn Vọng Đông, Yên Trung, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 481,
   "Name": "Quầy thuốc Trang Linh",
   "address": "Thôn Phong Xá, xã Đông Phong, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1956876,
   "Latitude": 106.0150623
 },
 {
   "STT": 482,
   "Name": "Quầy thuốc Linh Giang",
   "address": "Yên Lãng, Yên Trung, Yên Phong, Bắc Ninh",
   "Longtitude": 21.208772,
   "Latitude": 105.9942255
 },
 {
   "STT": 483,
   "Name": "Quầy thuốc Thu Huyền",
   "address": "DĐông Khê, xã Song Hồ, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0626443,
   "Latitude": 106.0904721
 },
 {
   "STT": 484,
   "Name": "Quầy thuốc Tân Dược",
   "address": "Khu Lãm Làng, Phường  Vân Dương, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1509203,
   "Latitude": 106.1037362
 },
 {
   "STT": 485,
   "Name": "Quầy thuốc Văn Phong",
   "address": "Số 42 khu 3, Thị trấn Phố Mới, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1534803,
   "Latitude": 106.1536828
 },
 {
   "STT": 486,
   "Name": "Quầy thuốc Xuân Thủy",
   "address": "Giang Liễu, Phương Liễu, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1618475,
   "Latitude": 106.1272201
 },
 {
   "STT": 487,
   "Name": "Quầy thuốc Đàm Tuân 3",
   "address": "Thôn Phả Lại, xã Đức Long, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1221528,
   "Latitude": 106.2816361
 },
 {
   "STT": 488,
   "Name": "Quầy thuốc Phương Nam",
   "address": "Khu 1, Phường  Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1738874,
   "Latitude": 106.0776114
 },
 {
   "STT": 489,
   "Name": "Quầy thuốc Trần Thiị Huệ",
   "address": "Thôn Lạc Xá, xã Quế Tân, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1659561,
   "Latitude": 106.1921545
 },
 {
   "STT": 490,
   "Name": "Quầy thuốc Hiền Phước",
   "address": "Khu Laãm Làng, Phường  Vân Dương, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1509203,
   "Latitude": 106.1037362
 },
 {
   "STT": 491,
   "Name": "Quầy thuốc Anh Linh",
   "address": "Thôn Đaon Bái, xã Đại Bái, Huyện Gia Bình, Tỉnh Bă Ninh",
   "Longtitude": 21.0504283,
   "Latitude": 106.1419201
 },
 {
   "STT": 492,
   "Name": "Quầy thuốc Hằng Thủy ",
   "address": "Khu Ném Thượng, Phường  Khắc Niệm, Thành phố Bắc Ninh",
   "Longtitude": 21.1314843,
   "Latitude": 106.0580066
 },
 {
   "STT": 493,
   "Name": "Quầy thuốc Thanh Nga",
   "address": "Số 112 Chợ Sơn, Việt Đoàn, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1096804,
   "Latitude": 106.0366384
 },
 {
   "STT": 494,
   "Name": "Quầy thuốc Tueej Tam",
   "address": "Khu Lam Lang, phuong Van Duong, Thành phố Bắc Ninh",
   "Longtitude": 21.14966,
   "Latitude": 106.1037006
 },
 {
   "STT": 495,
   "Name": "Quầy thuốc Tân Thành",
   "address": "Số 17 B khu Hồ Ngọc Lân, Phường  Kinh Bắc , Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1888172,
   "Latitude": 106.0638342
 },
 {
   "STT": 496,
   "Name": "Quầy thuốc Nguyễn Thị Thùy",
   "address": "Thôn Hà Liễu, xã Phương Liễu, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1659828,
   "Latitude": 106.1367757
 },
 {
   "STT": 497,
   "Name": "Quầy thuốc Lâm Thúy",
   "address": "Chợ chiều, Văn Môn, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1692462,
   "Latitude": 105.92906
 },
 {
   "STT": 498,
   "Name": "Quầy thuốc Thanh Kháng",
   "address": "Thôn Rền, xã Cảnh Hưng, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.0846853,
   "Latitude": 106.0302688
 },
 {
   "STT": 499,
   "Name": "Quầy thuốc Tân Dược",
   "address": "Ki ốt Chợ trung tâm, Thị trấn Gia Bình, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0523011,
   "Latitude": 106.17511
 },
 {
   "STT": 500,
   "Name": "Quầy thuốc Quỳnh Anh",
   "address": "Thôn Đa Cấu, xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1424416,
   "Latitude": 106.1183999
 },
 {
   "STT": 501,
   "Name": "Quầy thuốc Hồng Nhung",
   "address": "Số 1 Cầu Cạn, Y Na, Phường  Kinh Bắc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1901869,
   "Latitude": 106.0700285
 },
 {
   "STT": 502,
   "Name": "Quầy thuốc số 28",
   "address": "Lãm Làng, Phường  Vân Dương, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.14966,
   "Latitude": 106.1037006
 },
 {
   "STT": 503,
   "Name": "Quầy thuốc Bách Lộc",
   "address": "Xóm Đông, xã Tuong Giang, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1347045,
   "Latitude": 105.9827042
 },
 {
   "STT": 504,
   "Name": "Quầy thuốc Nguyễn Thị Thủy",
   "address": "Tự Thôn, Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1412604,
   "Latitude": 106.1007608
 },
 {
   "STT": 505,
   "Name": "Quầy thuốc Trần Thị Yến",
   "address": "Khu phố Vĩnh Kiều, Phường  Đồng Nguyên, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1319011,
   "Latitude": 105.9729255
 },
 {
   "STT": 506,
   "Name": "Quầy thuốc Quỳnh Anh",
   "address": "Thôn Ô Cách, xã Đông Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 507,
   "Name": "Quầy thuốc số 2",
   "address": "Số 3 Lê Phụng Hiểu, Phường  Kinh Bắc, Thành phố Bắc Ninh",
   "Longtitude": 21.180045,
   "Latitude": 106.0578757
 },
 {
   "STT": 508,
   "Name": "Quầy thuốc Quỳnh Anh",
   "address": "Thôn Ô Cách, xã Đông Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 509,
   "Name": "Quầy thuốc Lộc Thảo",
   "address": "Vinh Hương, Thị trấn Gia Bình, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.060814,
   "Latitude": 106.1764731
 },
 {
   "STT": 510,
   "Name": "Quầy thuốc Thanh Hằng",
   "address": "Thiị trấn Chờ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1978889,
   "Latitude": 105.9494248
 },
 {
   "STT": 511,
   "Name": "Quầy thuốc Số 9",
   "address": "Khu 9 Phường  Đại Phúc, Thành phố Bắc Ninh",
   "Longtitude": 21.1724345,
   "Latitude": 106.085328
 },
 {
   "STT": 512,
   "Name": "Quầy thuốc Tú Hồng",
   "address": " Ki ốt 18A, chợ trung tâm, Thị trấn Phố Mới, Huyện Quế Võ",
   "Longtitude": 21.1521259,
   "Latitude": 106.1500901
 },
 {
   "STT": 513,
   "Name": "Quầy thuốc Phương Hoa",
   "address": "Số 221 Nguyễn Văn Cừ, Phường  Võ Cường, Thành phố Bắc Ninh, ",
   "Longtitude": 21.1728969,
   "Latitude": 106.0529727
 },
 {
   "STT": 514,
   "Name": "Nhà thuốc Thủy Lan",
   "address": "269 Trần Hưng Đạo, Tiền An, Thành phố Bắc Ninh",
   "Longtitude": 21.1793125,
   "Latitude": 106.0683478
 },
 {
   "STT": 515,
   "Name": "Quầy thuốc Phương Anh",
   "address": "Khu Xuân Ổ B, Phường  Võ Cường, Thành phố Bắc Ninh",
   "Longtitude": 21.1589111,
   "Latitude": 106.0383694
 },
 {
   "STT": 516,
   "Name": "Quầy thuốc Mạnh Dũng",
   "address": "Phố Cống Nguyễn, Phường  Hạp Lĩnh, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1309568,
   "Latitude": 106.0761376
 },
 {
   "STT": 517,
   "Name": "Quầy thuốc Hà Linh 2",
   "address": "Thôn Giang Liễu, xã Phương Liễu, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1578393,
   "Latitude": 106.1255264
 },
 {
   "STT": 518,
   "Name": "Quầy thuốc số 1",
   "address": "Số 493 Ngô Gia Tự, Phường  Tiền An, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1811124,
   "Latitude": 106.0630918
 },
 {
   "STT": 519,
   "Name": "Quầy số 25 ",
   "address": "Số 8 Vũ Kiệt, Phường   Tiền An, Thành phố Bắc Ninh",
   "Longtitude": 21.182571,
   "Latitude": 106.0658473
 },
 {
   "STT": 520,
   "Name": "Quầy thuốc 59 ",
   "address": "Số 13 Nguyễn Đăng Đạo, Phường   Đại Phúc, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.175178,
   "Latitude": 106.0723275
 },
 {
   "STT": 521,
   "Name": "Quầy thuốc số 11",
   "address": "Trung tâm CSSKSS Bắc Ninh, Thanh Phương, Vũ Ninh, Thành phố Bắc Ninh",
   "Longtitude": 21.190274,
   "Latitude": 106.080004
 },
 {
   "STT": 522,
   "Name": "Quầy thuốc số 42",
   "address": "24 Trần Lựu, Khu 5, Phường  Thị Cầu, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1963737,
   "Latitude": 106.0939761
 },
 {
   "STT": 523,
   "Name": "Quầy số 47 ",
   "address": "Chợ Vũ Ninh, Phường   Vũ Ninh, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1904816,
   "Latitude": 106.0786656
 },
 {
   "STT": 524,
   "Name": "Quầy số 45 ",
   "address": "Số 1 Hồ Ngọc Lân, Phường   Kinh Bắc, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.187648,
   "Latitude": 106.063208
 },
 {
   "STT": 525,
   "Name": "Quầy thuốc An Thuyên",
   "address": "Xuân Ổ A, Phường  Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1533181,
   "Latitude": 106.0360979
 },
 {
   "STT": 526,
   "Name": "Quầy số 39 ",
   "address": "Thôn Sơn Đông, xã Nam Sơn, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.141189,
   "Latitude": 106.0930395
 },
 {
   "STT": 527,
   "Name": "Quầy thuốc số 20",
   "address": "44 Trần Lựu, Khu 5, Phường  Thị Cầu, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.196956,
   "Latitude": 106.0896867
 },
 {
   "STT": 528,
   "Name": "Quầy số 56 ",
   "address": "Khu Chu Mẫu, Phường   Vân Dương, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1649355,
   "Latitude": 106.0964516
 },
 {
   "STT": 529,
   "Name": "Quầy thuốc Sông Cầu",
   "address": "Số 82 Hoàng Quốc Việt, Khu 1, Phường  Thị Cầu, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1935799,
   "Latitude": 106.0856562
 },
 {
   "STT": 530,
   "Name": "Quầy thuốc Thanh Vân",
   "address": "331 B Ngô Gia Tự, Phường  Suối Hoa, Thành phố Bắc Ninh",
   "Longtitude": 21.1849779,
   "Latitude": 106.0680783
 },
 {
   "STT": 531,
   "Name": "Quầy thuốc số 33 ",
   "address": "Số 56 Trần Lựu, khu 5, Phường  Thị Cầu, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1672299,
   "Latitude": 106.0774283
 },
 {
   "STT": 532,
   "Name": "Quầy thuốc CTCPDP Bắc Ninh",
   "address": "Số 182 Nguyễn Gia Thiều, Phường   Suối Hoa, Thành phố Bắc Ninh",
   "Longtitude": 21.1837357,
   "Latitude": 106.0696949
 },
 {
   "STT": 533,
   "Name": "Quầy thuốc Sáng Chuyên",
   "address": "Khu Xuân Ổ A, Phường  Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1589111,
   "Latitude": 106.0383694
 },
 {
   "STT": 534,
   "Name": "Quầy số 5",
   "address": "Số 24 Nguyễn Văn Cừ, Ninh Xá, Thành phố Bắc Ninh",
   "Longtitude": 21.1771363,
   "Latitude": 106.0577945
 },
 {
   "STT": 535,
   "Name": "Quầy thuốc số 36 ",
   "address": "Số 08 Trần Lựu, Khu 5, Phường  Thị Cầu, Thành phố Bắc Ninh",
   "Longtitude": 21.196786,
   "Latitude": 106.0889232
 },
 {
   "STT": 536,
   "Name": "Quầy thuốc số 30",
   "address": "Thôn Đa Cấu, xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1424416,
   "Latitude": 106.1183999
 },
 {
   "STT": 537,
   "Name": "Quầy thuốc Khánh linh",
   "address": "Thôn Do Nha, xã Phương Liễu, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1732871,
   "Latitude": 106.1079848
 },
 {
   "STT": 538,
   "Name": "Quầy số 44",
   "address": "Khu 10, Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1757306,
   "Latitude": 106.0709139
 },
 {
   "STT": 539,
   "Name": "QuẦY tuốc số 60",
   "address": "Số 50 Đường Đại Phúc 14, Phường   Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1702959,
   "Latitude": 106.0744479
 },
 {
   "STT": 540,
   "Name": "Quầy thuốc số 21",
   "address": "167 Nguyễn Cao, Phường  Ninh Xá, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1759307,
   "Latitude": 106.0611731
 },
 {
   "STT": 541,
   "Name": "Quầy thuốc số 61",
   "address": "Số 40 Cầu Cạn Y Na, Phường  Kinh Bắc, Thành phố Bắc Ninh",
   "Longtitude": 21.1901869,
   "Latitude": 106.0700285
 },
 {
   "STT": 542,
   "Name": "Quầy thuốc số 11",
   "address": "Số 6, khu 5, Phường   Thị Cầu, Thành phố Bắc Ninh, Bawcvs Ninh",
   "Longtitude": 21.1672299,
   "Latitude": 106.0774283
 },
 {
   "STT": 543,
   "Name": "Quầy số 29 ",
   "address": "Số 189 Nguyễn Cao, Phường  Ninh Xá, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1751767,
   "Latitude": 106.059972
 },
 {
   "STT": 544,
   "Name": "Quầy thuốc số 22",
   "address": "Khu 3 Phường  Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1728056,
   "Latitude": 106.0791944
 },
 {
   "STT": 545,
   "Name": "Quầy thuốc số 06",
   "address": "Số 16, tổ 18, khu 4, Phường  Vệ An, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1781766,
   "Latitude": 106.0710255
 },
 {
   "STT": 546,
   "Name": "Quầy thuốc số 14 ",
   "address": "Số 189 Nguyễn Cao, ki ốt 12 chợ Đọ,Phường  Ninh Xá, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.176065,
   "Latitude": 106.0610848
 },
 {
   "STT": 547,
   "Name": "Quầy thuốc số 42",
   "address": "Đương Xá, Vạn An, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.19555,
   "Latitude": 106.0457
 },
 {
   "STT": 548,
   "Name": "Quầy thuốc số 27",
   "address": "Số 2a Đường Đấu Mã, Phường  Thị Cầu, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1909577,
   "Latitude": 106.086493
 },
 {
   "STT": 549,
   "Name": "Quầy thuốc số 58 ",
   "address": "Lê Phụng Hiểu, Phường  Vệ An, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1843295,
   "Latitude": 106.0554113
 },
 {
   "STT": 550,
   "Name": "Quầy số 43 ",
   "address": " Đươờng Đấu Mã, Phường   Vũ Ninh, Thành phố Bắc Ninh",
   "Longtitude": 21.1843078,
   "Latitude": 106.0837895
 },
 {
   "STT": 551,
   "Name": "Quầy thuốc số 10",
   "address": "35 Đường Trần Lựu, Thị Cầu, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 552,
   "Name": "Quầy thuốc số 50",
   "address": "Đa Cấu, Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1424416,
   "Latitude": 106.1183999
 },
 {
   "STT": 553,
   "Name": "Quầy thuốc số 26",
   "address": "Số 632 Đường Thiên Đức, Phường  Vạn An, Thành phố Bắc Ninh",
   "Longtitude": 21.1904589,
   "Latitude": 106.0546732
 },
 {
   "STT": 554,
   "Name": "Quầy số 41 ",
   "address": "Số 487 Hoàng Quốc Việt, K2 Đáp Cầu, Thành phố Bắc Ninh",
   "Longtitude": 21.2000169,
   "Latitude": 106.0949056
 },
 {
   "STT": 555,
   "Name": "Quầy thuốc Thanh Huyền",
   "address": "Khu Lãm Làng, Phường  Vân Dương, Thành phố Bắc Ninh",
   "Longtitude": 21.1509203,
   "Latitude": 106.1037362
 },
 {
   "STT": 556,
   "Name": "Quầy thuốc Hoàng Thị Hảo ",
   "address": "Số 75 Tân Lập, Phường   Đình Bảng, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1015708,
   "Latitude": 105.9378513
 },
 {
   "STT": 557,
   "Name": "Quầy thuốc số 12 ",
   "address": "Phố Cầu Ngà, Chu Mẫu, Vân Dương, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1656447,
   "Latitude": 106.0956164
 },
 {
   "STT": 558,
   "Name": "Quầy thuốc Ánh hồng",
   "address": "Xuân Ổ A, Phường  Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1533181,
   "Latitude": 106.0360979
 },
 {
   "STT": 559,
   "Name": "Quầy thuốc số 1 Chu Mẫu",
   "address": "Khu Chu Mẫu, Phường   Vân Dương, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1649355,
   "Latitude": 106.0964516
 },
 {
   "STT": 560,
   "Name": "Quầy thuốc Việt Ngọc",
   "address": "Xóm Đông, thôn Kim Đôi, xã Kim Chân, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.21641,
   "Latitude": 105.9751288
 },
 {
   "STT": 561,
   "Name": "Quang Thành",
   "address": "Số 81 Trần Hưng Đạo,, Phường  Tiền An, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.180625,
   "Latitude": 106.0649217
 },
 {
   "STT": 562,
   "Name": " Quầy thuốc Việt Hoa",
   "address": "Số 172 Đường Bình Than, Phường  Võ Cường, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1706731,
   "Latitude": 106.0743024
 },
 {
   "STT": 563,
   "Name": "CTCPDP  Kỳ Thiên",
   "address": "18 Vũ Kiệt, Phường  Tiền An, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1822759,
   "Latitude": 106.0660672
 },
 {
   "STT": 564,
   "Name": "Quầy thuốc Kiên Thảo",
   "address": "Lê Ngọc Hân, Chợ Dầu, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.117311,
   "Latitude": 105.9631616
 },
 {
   "STT": 565,
   "Name": "Quầy thuốc Lê Thị Thu ",
   "address": "Số 33 Minh Khai, Phường   Đông Ngàn, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1207001,
   "Latitude": 105.9647201
 },
 {
   "STT": 566,
   "Name": "Quầy thuốc Lê Thị Thanh Huyền ",
   "address": "Khu phố Thọ Môn, Phường  Đình Bảng, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1106794,
   "Latitude": 105.9509991
 },
 {
   "STT": 567,
   "Name": "Quầy thuốc Trung tâm",
   "address": "Số 21 Nguyễn Văn Cừ, Phường  Ninh Xá, Thành phố Bắc Ninh",
   "Longtitude": 21.1772065,
   "Latitude": 106.0578699
 },
 {
   "STT": 568,
   "Name": "Quầy thuốc Nguyễn Thị Bình",
   "address": "Chợ Tiền, Phường  Khắc Niêm, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1376266,
   "Latitude": 106.0548851
 },
 {
   "STT": 569,
   "Name": "Quầy thuốc Bình Minh",
   "address": "315 Nguyễn Trãi, Phường  Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1706704,
   "Latitude": 106.0629154
 },
 {
   "STT": 570,
   "Name": "Quầy thuốc số 10",
   "address": "Số 68 Phố Vũ, Phường  Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1738006,
   "Latitude": 106.0787142
 },
 {
   "STT": 571,
   "Name": "Quầy thuốc Huyền Trang",
   "address": "24 Đường Đấu Mã, Phường  Thị Cầu, Thành phố Bắc Ninh",
   "Longtitude": 21.1909577,
   "Latitude": 106.086493
 },
 {
   "STT": 572,
   "Name": "Quầy thuốc Huệ Nguyệt",
   "address": "Khu Tiền Xá, Phường  Hạp Lĩnh, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.142413,
   "Latitude": 106.0736783
 },
 {
   "STT": 573,
   "Name": "Quầy thuốc Việt Đức",
   "address": "Khu Lãm Làng, Phường  Vân Dương, Thành phố Bắc Ninh",
   "Longtitude": 21.1509203,
   "Latitude": 106.1037362
 },
 {
   "STT": 574,
   "Name": "Quầy thuốc Linh Trang",
   "address": "Khu Khả Lễ 1, Phường  Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1644992,
   "Latitude": 106.0552006
 },
 {
   "STT": 575,
   "Name": "Quầy thuốc Hạp Lĩnh",
   "address": "Phố Và, Phường  Hạp Lĩnh, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1309568,
   "Latitude": 106.0761376
 },
 {
   "STT": 576,
   "Name": "Quầy thuốc Tân dược",
   "address": "Cầu Sộp, Cảnh Hưng, Tiên Du, Bắc Ninh",
   "Longtitude": 21.0846853,
   "Latitude": 106.0302688
 },
 {
   "STT": 577,
   "Name": "Quầy thuốc số 5",
   "address": "Khu Chu Mẫu, Phường   Vân Dương, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1649355,
   "Latitude": 106.0964516
 },
 {
   "STT": 578,
   "Name": "Quầy thuốc số 5 ",
   "address": "Phố Và, Hạp Lĩnh, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.137099,
   "Latitude": 106.0765097
 },
 {
   "STT": 579,
   "Name": "Quầy thuốc Trung tâm",
   "address": "Số 37 Trần Phú, Phường   Đông Ngàn, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1197853,
   "Latitude": 105.9631539
 },
 {
   "STT": 580,
   "Name": "Quầy thuốc Huyền Nhung",
   "address": "Khu Lãm Làng, Phường  Vân Dương, Thành phố Bắc Ninh",
   "Longtitude": 21.1509203,
   "Latitude": 106.1037362
 },
 {
   "STT": 581,
   "Name": "Quầy thuốc số 55",
   "address": "Số 162 Bình Than, Phường  Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1706731,
   "Latitude": 106.0743024
 },
 {
   "STT": 582,
   "Name": "Quầy thuốc tư nhân Phạm Thị Hà",
   "address": "Cường Tráng, An Thịnh, Lương Tài, Bắc Ninh",
   "Longtitude": 21.0449808,
   "Latitude": 106.2794293
 },
 {
   "STT": 583,
   "Name": "Quầy thuốc Tuyết Công",
   "address": "Thôn Sơn Đông, xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.141189,
   "Latitude": 106.0930395
 },
 {
   "STT": 584,
   "Name": "Quầy thuốc Nguyễn Thị Thủy ",
   "address": "Số 83 khu phố Thọ Môn, Phường  Đình Bảng, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1106794,
   "Latitude": 105.9509991
 },
 {
   "STT": 585,
   "Name": "Quầy thuốc Vũ Thị Thanh Lan ",
   "address": "Số 120 Khu phố Hạ, Phường   Đình Bảng, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1142835,
   "Latitude": 105.9739977
 },
 {
   "STT": 586,
   "Name": "Quầy thuốc Nguyễn Thị Thơm",
   "address": "Phù Lưu, Phường  Đông Ngàn, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1143535,
   "Latitude": 105.9618634
 },
 {
   "STT": 587,
   "Name": "Quầy thuốc Ngọc Hằng",
   "address": "Khu phố Xuân Đài, Phường  Đình Bảng, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1079702,
   "Latitude": 105.9478643
 },
 {
   "STT": 588,
   "Name": "Quầy thuốc Hải Hưng",
   "address": "Đường Bà Chúa Kho, Cô Mễ, Phường  Vũ Ninh, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.2058178,
   "Latitude": 106.0844887
 },
 {
   "STT": 589,
   "Name": "Quầy thuốc Phương Thảo",
   "address": "17C, Khu Chùa Dận, phương Đình Bảng, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1091643,
   "Latitude": 105.9426601
 },
 {
   "STT": 590,
   "Name": "Quầy thuốc Phương Thảo",
   "address": "Thôn Rích, xã Phù Chẩn, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1196529,
   "Latitude": 105.9623161
 },
 {
   "STT": 591,
   "Name": "Quầy thuốc số 18",
   "address": "Phố Lê Văn Duyệt, Phường  Vũ Ninh, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1896383,
   "Latitude": 106.0771498
 },
 {
   "STT": 592,
   "Name": "Quầy thuốc Nhuyễn Thị Hòa ",
   "address": "35 Lê Hồng Phong,Phường   Đông Ngàn, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1156528,
   "Latitude": 105.9611748
 },
 {
   "STT": 593,
   "Name": "Quầy thuốc Lý Thị Thu Hường ",
   "address": "Khu phố 3, Phường   Đồng Nguyên, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1319011,
   "Latitude": 105.9729255
 },
 {
   "STT": 594,
   "Name": "Quầy thuốc Dương Thị Thu Hiền ",
   "address": "Xóm Trại 5, khu phố Đa Hội, Phường  Châu Khê, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1246772,
   "Latitude": 105.9217577
 },
 {
   "STT": 595,
   "Name": "Quầy thuốc Trương Thị Yến ",
   "address": "Đường Ngô Gia Tự, xã Tam Sơn, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.147234,
   "Latitude": 105.9586086
 },
 {
   "STT": 596,
   "Name": "Quaầy thuốc Ngọc Thảo",
   "address": "Khu phố Thọ Môn, Phường  Đình Bảng, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1106794,
   "Latitude": 105.9509991
 },
 {
   "STT": 597,
   "Name": "Quầy thuốc Quỳnh Anh",
   "address": "Khu Bach hóa chợ Gạo, Phường  Đông Ngàn, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1198931,
   "Latitude": 105.9622303
 },
 {
   "STT": 598,
   "Name": "Quầy thuốc Minh Tuấn",
   "address": "Khu phố Chùa Dận, Phường  Đình Bảng, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1091643,
   "Latitude": 105.9426601
 },
 {
   "STT": 599,
   "Name": "Nhà thuốc Bảo Nam",
   "address": "Số 4 Trần Lựu, Phường  Thị Cầu, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1967755,
   "Latitude": 106.088864
 },
 {
   "STT": 600,
   "Name": "Quầy thuốc Ngân Anh",
   "address": "Số 16 Trần Lựu, khu 5, Phường  Thị Cầu, Thành phố Bắc Ninh",
   "Longtitude": 21.1963958,
   "Latitude": 106.0937315
 },
 {
   "STT": 601,
   "Name": "Quầy thuốc Thúy Quỳnh",
   "address": "Khu Sơn, Phường  Khắc Niệm, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1330408,
   "Latitude": 106.0735704
 },
 {
   "STT": 602,
   "Name": "Quầy thuốc Lê Phương Lan ",
   "address": "Khu phố Phù Lưu, Phường   Đông Ngàn, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1141283,
   "Latitude": 105.9614584
 },
 {
   "STT": 603,
   "Name": "Quaầy thuốc Ngô Thúy Nhàn",
   "address": "Số 679 Đường 286, Phường  Vạn An, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1932236,
   "Latitude": 106.050624
 },
 {
   "STT": 604,
   "Name": " Quầy số 19 ",
   "address": "Số 530 Nguyễn Trãi, Võ Cường, Thành phố Bắc Ninh",
   "Longtitude": 21.1664488,
   "Latitude": 106.0646832
 },
 {
   "STT": 605,
   "Name": " Quầy số 1 ",
   "address": "95 Đường Thành Bắc, Khu Ga, Phường   Ninh Xá, Thành phố Bắc Ninh",
   "Longtitude": 21.1790278,
   "Latitude": 106.057225
 },
 {
   "STT": 606,
   "Name": "Quầy thuốc Bảo Lan",
   "address": "Khu Laãm Làng, Phường  Vân Dương, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1509203,
   "Latitude": 106.1037362
 },
 {
   "STT": 607,
   "Name": "Quầy thuốc Ngô Thị Thơ ",
   "address": "Khu phố Vĩnh Kiều, Phường   Đồng Nguyên, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1319011,
   "Latitude": 105.9729255
 },
 {
   "STT": 608,
   "Name": "Quầy thuốc Tùng Dương",
   "address": "Lạc Nhuế, Thụy Hòa, Yên Trung, Yên Phong",
   "Longtitude": 21.205903,
   "Latitude": 106.0089165
 },
 {
   "STT": 609,
   "Name": "Quầy thuốc Hải Thiên",
   "address": "Thôn Yên Lãng, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc nInh",
   "Longtitude": 21.2092511,
   "Latitude": 105.9936476
 },
 {
   "STT": 610,
   "Name": "Quầy thuốc Kim Thu",
   "address": "Thôn Tiêu Long, xã Tương Giang, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1369323,
   "Latitude": 105.9883494
 },
 {
   "STT": 611,
   "Name": "Quầy thuốc Phương Nam",
   "address": "Thôn Hoài Thượng, xã Liên Bão, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1302752,
   "Latitude": 106.0245772
 },
 {
   "STT": 612,
   "Name": "Quầy thuốc Gia Bảo",
   "address": "Thôn Núi Móng, xã Hoàn Sơn, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1253797,
   "Latitude": 106.00304
 },
 {
   "STT": 613,
   "Name": "Nhà thuốc Quang Việt",
   "address": "Phố Chờ, Thị trấn Chờ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1978889,
   "Latitude": 105.9494248
 },
 {
   "STT": 614,
   "Name": "Quầy thuốc Đỗ Thị Tươi",
   "address": "Thôn Trúc Ổ, xã Mộ Đạo, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1182167,
   "Latitude": 106.13743
 },
 {
   "STT": 615,
   "Name": "Quầy thuốc Thiên Ân",
   "address": "Khu 1, Thị trấn Phố Mới, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1489952,
   "Latitude": 106.1551531
 },
 {
   "STT": 616,
   "Name": "Quầy thuốc Tân Dược",
   "address": "Thôn Mao Dộc, xã Phượng Mao, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1543426,
   "Latitude": 106.1367842
 },
 {
   "STT": 617,
   "Name": "Quầy thuốc Hải Nam",
   "address": "Số 119 Nguyễn Trãi,Phường   Ninh Xá, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1754706,
   "Latitude": 106.0605712
 },
 {
   "STT": 618,
   "Name": "Nhà thuốc tư nhân",
   "address": "Đường Gia Bình, Thị trấn Gia Bình, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0547198,
   "Latitude": 106.1719899
 },
 {
   "STT": 619,
   "Name": "Quầy thuốc Khánh Ly",
   "address": "Thôn Đồng Xép, xã Hoàn Sơn, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.110453,
   "Latitude": 105.9867918
 },
 {
   "STT": 620,
   "Name": "Quầy thuốc Én Nhạn",
   "address": "Số 6 khu 3, Thị trấn Phố Mới, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1534803,
   "Latitude": 106.1536828
 },
 {
   "STT": 621,
   "Name": "Nhà thuốc Trần Anh",
   "address": "Số 84 khu phố Thượng, phuopwngf Đình Bảng, Thị xã Tu Sơn, Bắc Ninh",
   "Longtitude": 21.1142835,
   "Latitude": 105.9739977
 },
 {
   "STT": 622,
   "Name": "Quầy thuốc Quân Thuận",
   "address": "Ki ốt Chợ Ngụ, Cầu Đào, Nhân Thắng, Gia Bình",
   "Longtitude": 21.0638318,
   "Latitude": 106.2309361
 },
 {
   "STT": 623,
   "Name": "Quầy thuốc Minh Thu",
   "address": "Ki ot 23 chợ Thị trấn Gia Bình, Huyện Gia Bình, Tỉnh Bắc NinHuyện ",
   "Longtitude": 21.0527482,
   "Latitude": 106.1746859
 },
 {
   "STT": 624,
   "Name": "Quầy thuốc Mùi Phượng",
   "address": "Thôn Cao Thọ, xã Vạn Ninh, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0939382,
   "Latitude": 106.2595694
 },
 {
   "STT": 625,
   "Name": "Nhà thuốc BVĐK Kinh Bắc cơ sở 2",
   "address": "Khu 5, Phường  Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1672299,
   "Latitude": 106.0774283
 },
 {
   "STT": 626,
   "Name": "Nhà thuốc Bệnh viên QY 110",
   "address": "Viện Quân Y 110, Phường  Thị Cầu, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1979876,
   "Latitude": 106.0901203
 },
 {
   "STT": 627,
   "Name": "Quầy thuốc Dương Thị Phố",
   "address": "Thôn Phương Càu, xã Phương Liễu, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1634934,
   "Latitude": 106.1228104
 },
 {
   "STT": 628,
   "Name": "Quầy thuốc Phúc Hậu",
   "address": "Ấp Đồn, Yên Trung, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2068977,
   "Latitude": 105.9986327
 },
 {
   "STT": 629,
   "Name": "Quầy thuốc Bùi Thị Quỳnh",
   "address": "Thôn Yên Lãng, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc nInh",
   "Longtitude": 21.2092511,
   "Latitude": 105.9936476
 },
 {
   "STT": 630,
   "Name": "Quầy thuốc Minh Huệ",
   "address": "Thôn Vũ Dương, xã Bồng Lai, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.117692,
   "Latitude": 106.1600512
 },
 {
   "STT": 631,
   "Name": "Quầy thuốc Nguyễn Thị Vân Anh",
   "address": "Thôn Quế Tân, xã Quế Tân, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1659561,
   "Latitude": 106.1921545
 },
 {
   "STT": 632,
   "Name": "Quầy thuốc Minh Huệ",
   "address": "Thôn Vũ Dương, xã Bồng Lai, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.117692,
   "Latitude": 106.1600512
 },
 {
   "STT": 633,
   "Name": "Quầy thuốc Kim Ngân",
   "address": "Thôn Đồng Xép, xã Hoàn Sơn, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.110453,
   "Latitude": 105.9867918
 },
 {
   "STT": 634,
   "Name": "Quầy thuốc Nguyễn Thị Thu Hà",
   "address": "Thôn Dương Húc, xã Đại Đồng, Huyện Tiên Du, Tỉnh Bắc Ninh ",
   "Longtitude": 21.0963721,
   "Latitude": 105.9875268
 },
 {
   "STT": 635,
   "Name": "Quầy thuốc Ngân Hà",
   "address": "93 Hai Bà Trưng, Thị trấn Lim, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1453417,
   "Latitude": 106.0217196
 },
 {
   "STT": 636,
   "Name": "Quầy thuốc Tâm Cường",
   "address": "Thôn Hương Mạc, xã Hương Mạc, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1584646,
   "Latitude": 105.9376756
 },
 {
   "STT": 637,
   "Name": "Quầy thuốc Thanh Nga",
   "address": "Thôn Núi Móng, xã Hoàn Sơn, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1253797,
   "Latitude": 106.00304
 },
 {
   "STT": 638,
   "Name": "Quầy thuốc Cường Hoa",
   "address": "Phấn Động, Tam Đa, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2267942,
   "Latitude": 106.0324245
 },
 {
   "STT": 639,
   "Name": "Quầy thuốc Tâm Lụa",
   "address": "Thôn Hương Mạc, xã Hương Mạc, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1584646,
   "Latitude": 105.9376756
 },
 {
   "STT": 640,
   "Name": "Quầy thuốc Đaăng Hiếu",
   "address": "Thôn Đỉnh, Thị trấn Phố MỚi, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1489952,
   "Latitude": 106.1551531
 },
 {
   "STT": 641,
   "Name": "Quầy thuốc LOAN HiỀU",
   "address": "Thôn Bồng Lai, xã Bồng Lai, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.117692,
   "Latitude": 106.1600512
 },
 {
   "STT": 642,
   "Name": "Quầy thuốc - chi nhánh thành phố",
   "address": "Số 10 Khu 6 Thị cầu",
   "Longtitude": 21.1969959,
   "Latitude": 106.0904471
 },
 {
   "STT": 643,
   "Name": "Quầy Nguyễn Thu Thủy",
   "address": "Bệnh viện đa khoa Tiên Du, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1331318,
   "Latitude": 106.0270324
 },
 {
   "STT": 644,
   "Name": "Quầy thuốc Chi nhánh Quế võ",
   "address": "Thôn Guột, Việt Hùng, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1398296,
   "Latitude": 106.1823549
 },
 {
   "STT": 645,
   "Name": "Quầy thuốc Anh Quân",
   "address": "Đông Yên, Đông Phong, Yên Phong",
   "Longtitude": 21.1883842,
   "Latitude": 106.0103857
 },
 {
   "STT": 646,
   "Name": "Quầy thuoốc Quang Thu",
   "address": "Phú Mãn, Thị trấn Chờ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1915293,
   "Latitude": 105.9552997
 },
 {
   "STT": 647,
   "Name": "Quầy thuốc Đỗ Thị Dung",
   "address": "An Ninh, Yên Phụ, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2023365,
   "Latitude": 105.925047
 },
 {
   "STT": 648,
   "Name": "Thanh Cầu",
   "address": "Thôn Lựa, xã Vietj Hùng, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1478183,
   "Latitude": 106.1672991
 },
 {
   "STT": 649,
   "Name": " Thanh Cầu",
   "address": "32 Chợ Yên, Đường Hồ Ngọc Lân, Phường   Kinh Bắc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1871286,
   "Latitude": 106.0585653
 },
 {
   "STT": 650,
   "Name": "QuẦY thuốc Ngọc Mai",
   "address": "Đông Yên, Đông Phong, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1883842,
   "Latitude": 106.0103857
 },
 {
   "STT": 651,
   "Name": "Quầy thuốc Nhật Hương",
   "address": "Đông Bích, Đông Thọ, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1707054,
   "Latitude": 105.9384099
 },
 {
   "STT": 652,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "Chùa Thầm, Xuân Lâm, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0236868,
   "Latitude": 106.0140586
 },
 {
   "STT": 653,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": "Ngô Xá, Long Châu, Yên Phong",
   "Longtitude": 21.1917917,
   "Latitude": 105.9938582
 },
 {
   "STT": 654,
   "Name": "Quầy thuốc Việt Thương",
   "address": "Ngã tư Thị trấn Chờ, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1970939,
   "Latitude": 105.9561647
 },
 {
   "STT": 655,
   "Name": "Quầy thuốc Dũng Lỹ",
   "address": "Cầu Nguyệt, Tam giang, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2248095,
   "Latitude": 105.9406128
 },
 {
   "STT": 656,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "Hoàng Xá,Ninh Xá, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0136841,
   "Latitude": 106.1103151
 },
 {
   "STT": 657,
   "Name": "Quầy thuốc Phương Thảo",
   "address": "Thôn Chi Long, xã Long Châu, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1941429,
   "Latitude": 105.9582372
 },
 {
   "STT": 658,
   "Name": "Đặng Thị Thắm",
   "address": "Thôn Đức Lâm, Yên Phụ, Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1858523,
   "Latitude": 105.9272575
 },
 {
   "STT": 659,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": "Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.202957,
   "Latitude": 105.9581624
 },
 {
   "STT": 660,
   "Name": "Chi nhánh Lương Tài",
   "address": "Chợ Đò, thôn Thanh Lâm, xã An Thịnh, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0437202,
   "Latitude": 106.2761277
 },
 {
   "STT": 661,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": "Thôn Thiểm Xuyên, xã Thụy Hòa, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2209811,
   "Latitude": 106.0205553
 },
 {
   "STT": 662,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": "Xóm Đình, thôn Yên Hậu, xã Hòa Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2029596,
   "Latitude": 105.9193188
 },
 {
   "STT": 663,
   "Name": "Quầy thuốc số 28",
   "address": "Thôn Chân Lạc, xã Dũng Liệt, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2469267,
   "Latitude": 105.9957504
 },
 {
   "STT": 664,
   "Name": "Chi nhánh dược Lương Tài",
   "address": "Thôn Quảng Bố, Quảng Phú, Lương Tài, Băc Ninh",
   "Longtitude": 21.0348395,
   "Latitude": 106.1605453
 },
 {
   "STT": 665,
   "Name": "Chi nhánh Lương Tài",
   "address": "Thôn Lương Xá, xã Phú Lương, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0007468,
   "Latitude": 106.205262
 },
 {
   "STT": 666,
   "Name": "Chi nhánh Lương Tài",
   "address": "Thôn NHất Trai, xã Minh Tân, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0134309,
   "Latitude": 106.2650964
 },
 {
   "STT": 667,
   "Name": "Chi nhánh Lương Tài",
   "address": "Thôn Kim Thao, xã Lâm Thao, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 20.9855183,
   "Latitude": 106.1877791
 },
 {
   "STT": 668,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": "Thôn Trần Xá, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.209834,
   "Latitude": 105.986279
 },
 {
   "STT": 669,
   "Name": "Chi nhánh dược Lương Tài",
   "address": "Tử Nê, Tân Lãng, Lương Tài, Bắc Ninh",
   "Longtitude": 21.0342759,
   "Latitude": 106.1889721
 },
 {
   "STT": 670,
   "Name": "Quầy thuốc Thu Huương",
   "address": "Thôn Bất Lự, Hoàn Sơn, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1130875,
   "Latitude": 105.9958363
 },
 {
   "STT": 671,
   "Name": "Nhà thuốc Nhân Hiền",
   "address": "Số 98 Nguyễn Văn Cừ, Phường  Ninh Xá, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1752445,
   "Latitude": 106.0555108
 },
 {
   "STT": 672,
   "Name": "Đại lý Nguyễn Thị Tư",
   "address": "Âp Đồn, Yên Trung, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2068977,
   "Latitude": 105.9986327
 },
 {
   "STT": 673,
   "Name": "Quầy thuốc đông y- thuốc từ dược liệu",
   "address": "Phố Và, Phường   Hạp Lĩnh, Thành phố Bắc Ninh",
   "Longtitude": 21.1391877,
   "Latitude": 106.0753369
 },
 {
   "STT": 674,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "Đa Thiện, Xuân Lâm, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0208191,
   "Latitude": 106.0115495
 },
 {
   "STT": 675,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "Phố Khám, Gia Đông, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0437995,
   "Latitude": 106.0757749
 },
 {
   "STT": 676,
   "Name": "Chi nhánh dược Thuận Thành",
   "address": "Phố Dâu, Thanh Khương, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0356025,
   "Latitude": 106.0405485
 },
 {
   "STT": 677,
   "Name": "Quầy thuốc số 3 ",
   "address": "Ki ốt Chợ Hòa Đình, Phường  Ninh Xá, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1715513,
   "Latitude": 106.0522431
 },
 {
   "STT": 678,
   "Name": "Đại lý Chi nhánh Yên phong",
   "address": "Thị trấn Chờ, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1978889,
   "Latitude": 105.9494248
 },
 {
   "STT": 679,
   "Name": "Quầy thuốc Nhân Dân",
   "address": "Khu 2, Phường  Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1752786,
   "Latitude": 106.0746726
 },
 {
   "STT": 680,
   "Name": "Quầy thuốc Đăng Hiếu",
   "address": "Thôn Đỉnh, Thị trấn Phố Mới, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1534803,
   "Latitude": 106.1536828
 },
 {
   "STT": 681,
   "Name": "Quầy thuốc Phạm Thị Lan",
   "address": "Khu 3, Thị trấn Phố Mới, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1534226,
   "Latitude": 106.1518371
 },
 {
   "STT": 682,
   "Name": "Quầy thuốc- Chi nhánh dược Quế Võ",
   "address": "Khu1, cổng phía đông Chợ Thị trấn Phố Mới, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1534803,
   "Latitude": 106.1536828
 },
 {
   "STT": 683,
   "Name": "Quầy thuốc Du Chi",
   "address": "Chợ Phủ, Thị trấn Phố Mới, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1515056,
   "Latitude": 106.1618381
 },
 {
   "STT": 684,
   "Name": "Quầy thuốc Gia Đình",
   "address": "Đường 24 Thôn Mao Trung, xã Phượng Mao, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1504392,
   "Latitude": 106.1448601
 },
 {
   "STT": 685,
   "Name": "Quầy thuốc Tần Lụa",
   "address": "Thôn Hương Mạc, xã Hương Mạc, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1554786,
   "Latitude": 105.9310669
 },
 {
   "STT": 686,
   "Name": "Quầy thuốc Tâm Cường",
   "address": "Thôn Hương Mạc, xã Hương Mạc, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1554786,
   "Latitude": 105.9310669
 },
 {
   "STT": 687,
   "Name": "Nhà thuốc Hà Bắc",
   "address": "Số 242 Trần Hưng Đạo, Phường  Tiền An, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1791162,
   "Latitude": 106.068371
 },
 {
   "STT": 688,
   "Name": "Quầy thuốc Tân Dược Nguyễn Thị Quang",
   "address": "Thôn Lương, xã Tri Phương, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.0816438,
   "Latitude": 106.006533
 },
 {
   "STT": 689,
   "Name": "Quầy thuốc Hoàn Sơn",
   "address": "Đồng Xép, Hoàn Sơn, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1104575,
   "Latitude": 105.9868102
 },
 {
   "STT": 690,
   "Name": "Nhà thuốc Phúc Hiền",
   "address": "Đường Nguyễn Văn Cừ, Xuân Ổ A, Võ Cường,Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1548186,
   "Latitude": 106.0338253
 },
 {
   "STT": 691,
   "Name": "Nhà thuốc Trường Thọ",
   "address": "170 Đường Bình Than, Phường  Võ Cường, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1706731,
   "Latitude": 106.0743024
 },
 {
   "STT": 692,
   "Name": "Nhà thuốc Đại Dương Xanh",
   "address": "Số 229, Nguyễn Trãi, Phường  Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1733164,
   "Latitude": 106.0616465
 },
 {
   "STT": 693,
   "Name": "Quầy thuốc Thái Bảo",
   "address": "Thôn Thái Bảo, xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1455414,
   "Latitude": 106.1088452
 },
 {
   "STT": 694,
   "Name": "Quầy thuốc Nghĩa Đạo Quân Hậu",
   "address": "Phố Vàng, xã Nghĩa Đạo, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0062123,
   "Latitude": 106.1200812
 },
 {
   "STT": 695,
   "Name": "Quầy thuốc Nam Phong",
   "address": "Thôn Thái Bảo, xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1455414,
   "Latitude": 106.1088452
 },
 {
   "STT": 696,
   "Name": "Quầy thuốc Hoan Luyến",
   "address": "Chợ Nội, Liễn Thượng, xã Đại Xuân, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1960769,
   "Latitude": 106.125015
 },
 {
   "STT": 697,
   "Name": "Quầy thuốc Hồng Điển",
   "address": "Khu 4, Thị trấn Phố Mới, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1516755,
   "Latitude": 106.154881
 },
 {
   "STT": 698,
   "Name": "Quầy thuốc Ngọc Anh",
   "address": "Thôn An Tập, xã Yên Phụ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1951605,
   "Latitude": 105.9237002
 },
 {
   "STT": 699,
   "Name": "Quầy thuốc Đức Phát",
   "address": "Thôn Đa Cấu, xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1424416,
   "Latitude": 106.1183999
 },
 {
   "STT": 700,
   "Name": "Quầy thuốc Huy Thơ",
   "address": "Thôn Thái Bảo, xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1455414,
   "Latitude": 106.1088452
 },
 {
   "STT": 701,
   "Name": "Nhà thuốc Tư Nhân",
   "address": "Số 155 Minh Khai, Phường  Đông Ngàn, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1219824,
   "Latitude": 105.9669778
 },
 {
   "STT": 702,
   "Name": "Nhà thuốc Nhân Dân",
   "address": "Khu phố Tân Thành, Phường  Đồng Kỵ, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1117507,
   "Latitude": 105.9874778
 },
 {
   "STT": 703,
   "Name": "Quầy thuốc Minh Khang",
   "address": "Thôn Trừng Xá, xã Trừng Xá, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.02427,
   "Latitude": 106.2397114
 },
 {
   "STT": 704,
   "Name": "Nhà thuốc Bình An",
   "address": "Khu Lãm Trại, Phường  Vân Dương, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1540096,
   "Latitude": 106.0937449
 },
 {
   "STT": 705,
   "Name": "Quầy thuốc Vũ Tuyết",
   "address": "Thôn Sơn Đông, xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.141189,
   "Latitude": 106.0930395
 },
 {
   "STT": 706,
   "Name": "Quầy thuốc Nhân Dân số 2",
   "address": "Thôn Kim Bảng, xã Hương Mạc, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1554786,
   "Latitude": 105.9310669
 },
 {
   "STT": 707,
   "Name": "Nhà thuốc Việt Đức",
   "address": "Khu đô thị Quy Chế Từ Sơn, Đường Lê Quang Đạo, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1207687,
   "Latitude": 105.9843659
 },
 {
   "STT": 708,
   "Name": "Quầy thuốc Phương Thúy",
   "address": "Thôn Đoài, xã Hoàn Sơn, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1065555,
   "Latitude": 106.00304
 },
 {
   "STT": 709,
   "Name": "Nhà thuốc Tiến Minh",
   "address": "Dương Ổ, Phong Khê, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1643906,
   "Latitude": 106.0338939
 },
 {
   "STT": 710,
   "Name": "Quầy thuốc số 19- Chi nhánh Yên Phong",
   "address": "Thôn Lạc Trung, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1949057,
   "Latitude": 105.9593153
 },
 {
   "STT": 711,
   "Name": "Quầy thuốc số 26- Chi nhánh Yên Phong",
   "address": "Thôn Thọ Đức, xã Tam Đa, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2430985,
   "Latitude": 106.0272254
 },
 {
   "STT": 712,
   "Name": "Quầy thuốc Hải Đăng- Chi nhánh Yên Phong",
   "address": "Thôn Nguyệt Cầu, xã Tam Giang, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1941429,
   "Latitude": 105.9582372
 },
 {
   "STT": 713,
   "Name": "Quầy thuốc Thúy Vịnh- Chi nhánh Yên Phong",
   "address": "Thôn Đoài, xã Tam Giang, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2276437,
   "Latitude": 105.9450735
 },
 {
   "STT": 714,
   "Name": "Quầy thuốc Minh Hạnh- Chi nhánh Yên Phong",
   "address": "Chợ chiều, Văn Môn, Yên Phong, Bắc Ninh",
   "Longtitude": 21.1692462,
   "Latitude": 105.92906
 },
 {
   "STT": 715,
   "Name": "Quầy thuốc Ngọc Huy",
   "address": "Tam Á, Gia Đông, Huyện Thuận Thành, Bắc Ninh",
   "Longtitude": 21.0314807,
   "Latitude": 106.0684267
 },
 {
   "STT": 716,
   "Name": "Nhà thuốc Thanh Hà",
   "address": "Khu 1, Đường Đấu Mã, Phường  Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1769048,
   "Latitude": 106.0798192
 },
 {
   "STT": 717,
   "Name": "Quầy thuốc Phúc Oanh",
   "address": "Thôn Đa Cấu, xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1424416,
   "Latitude": 106.1183999
 },
 {
   "STT": 718,
   "Name": "Quầy thuốc Anh Thơ",
   "address": "Ngã tư Dâu, phố Mới Công Hà, xã Hà Mãn, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0317013,
   "Latitude": 106.0360979
 },
 {
   "STT": 719,
   "Name": "Quầy trung tâm số 1",
   "address": "Số 70 Thị trấn Thưa, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0222876,
   "Latitude": 106.2080897
 },
 {
   "STT": 720,
   "Name": "Quầy thuốc Gia Đình",
   "address": "Thôn Thượng, xã Cảnh Hưng, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.0846853,
   "Latitude": 106.0302688
 },
 {
   "STT": 721,
   "Name": "Quầy thuốc Bích Ngọc",
   "address": "Thôn Mẫn Xá, xã Long Châu, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1947753,
   "Latitude": 105.98912
 },
 {
   "STT": 722,
   "Name": "Quầy thuốc Huyền Tuấn",
   "address": "Thôn Bất Lự, Hoàn Sơn, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1130875,
   "Latitude": 105.9958363
 },
 {
   "STT": 723,
   "Name": "Quầy thuốc Phương Giang",
   "address": "Thôn Quảng Bố, xã Quảng Phú, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0348395,
   "Latitude": 106.1605453
 },
 {
   "STT": 724,
   "Name": "Nhà thuốc Hoàn Mai",
   "address": "Khu 5, Đường Nguyễn Đăng Đạo, Phường  Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1697077,
   "Latitude": 106.0741212
 },
 {
   "STT": 725,
   "Name": "Quầy thuốc Lonh Chi ",
   "address": "Thôn Thái Bảo, xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1455414,
   "Latitude": 106.1088452
 },
 {
   "STT": 726,
   "Name": "Quầy thuốc phòng khám Y Cao Lim",
   "address": "Số 134 Hai Bà Trưng, Thị trấn Lim, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1457134,
   "Latitude": 106.0227435
 },
 {
   "STT": 727,
   "Name": "quầy thuốc Y Cao Hà Nội",
   "address": "Chợ chiều, xã Văn Môn, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1692462,
   "Latitude": 105.92906
 },
 {
   "STT": 728,
   "Name": "Nhà thuốc Hưng Loan",
   "address": "Số 187 khu phố Tân Lập, Phường  Đình Bảng, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.0964639,
   "Latitude": 105.9517614
 },
 {
   "STT": 729,
   "Name": "Nhà thuốc Linh Thương",
   "address": "Phoố Tam Lư, Phường  Đồng Nguyên, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1219004,
   "Latitude": 105.9803482
 },
 {
   "STT": 730,
   "Name": "Nhà thuốc Kinh Bắc",
   "address": "Số nhà 143 Đường Nguyễn Cao, Phường  Ninh Xá, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1757477,
   "Latitude": 106.0607958
 },
 {
   "STT": 731,
   "Name": "Quầy thuốc Sinh Hùng",
   "address": "Thôn Dương Húc, xã Đại Đồng, Huyện Tiên Du, Tỉnh Bắc Ninh ",
   "Longtitude": 21.0963721,
   "Latitude": 105.9875268
 },
 {
   "STT": 732,
   "Name": "Quầy thuốc Tuân Vân",
   "address": "Xóm Tây, Thôn Lũng Giang, Thị trấn Lim, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.144745,
   "Latitude": 106.0199355
 },
 {
   "STT": 733,
   "Name": "Quầy thuốc số 9",
   "address": "Thôn Tháp Dương, xã Trung Kệnh, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0422095,
   "Latitude": 106.2868881
 },
 {
   "STT": 734,
   "Name": "Quầy thuốc số 10",
   "address": "Thôn Tháp Dương, xã Trung Kệnh, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0422095,
   "Latitude": 106.2868881
 },
 {
   "STT": 735,
   "Name": "Quầy thuốc số 25",
   "address": "Taân Dân, Thị trấn Thứa, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.018034,
   "Latitude": 106.2034088
 },
 {
   "STT": 736,
   "Name": "Quầy thuốc số 29",
   "address": "Thôn Giàng, Thị trấn Thứa, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0222876,
   "Latitude": 106.2080897
 },
 {
   "STT": 737,
   "Name": "Quầy thuốc số 31",
   "address": "Ngọc Cục, Tân Lãng, Lương Tài, Bắc Ninh",
   "Longtitude": 21.0221284,
   "Latitude": 106.1948542
 },
 {
   "STT": 738,
   "Name": "Quầy thuốc Trung Hương",
   "address": "Thôn Nghiêm Xá, Việt Hùng, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1519158,
   "Latitude": 106.1655235
 },
 {
   "STT": 739,
   "Name": "Quầy thuốc Tuấn Thảo",
   "address": "Thôn Thái Boả, xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1455414,
   "Latitude": 106.1088452
 },
 {
   "STT": 740,
   "Name": "Quầy thuốc Sơn Hiền",
   "address": "Chợ Húc, xã Đại Đồng, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.0963721,
   "Latitude": 105.9875268
 },
 {
   "STT": 741,
   "Name": "Quầy thuốc số 40",
   "address": "Thôn Ngô Xá, xã Long Châu, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1917615,
   "Latitude": 105.993935
 },
 {
   "STT": 742,
   "Name": "Quầy thuốc số 66",
   "address": "Thôn Phong Xá, xã Đông Phong, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1956876,
   "Latitude": 106.0150623
 },
 {
   "STT": 743,
   "Name": "Quầy thuốc Trương Thị Minh Huê",
   "address": "Thôn Trần Xá, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.209834,
   "Latitude": 105.986279
 },
 {
   "STT": 744,
   "Name": "Quầy thuốc Hương Duyên",
   "address": "Thôn Ô Cách, xã Đông Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 745,
   "Name": "Quầy thuốc Nguyễn Thị Chinh",
   "address": "Chợ chiều, xã Văn Môn, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1692462,
   "Latitude": 105.92906
 },
 {
   "STT": 746,
   "Name": "Nhà thuốc Minh Ngọc",
   "address": "Thôn Ô Cách, xã Đông Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 747,
   "Name": "Quầy thuốc Nguyễn Thị Thanh Hải",
   "address": "Bệnh viện đa khoa Huyện Huyện Quế Võ, thôn Đỉnh, Thị trấn Phố Mới, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1544801,
   "Latitude": 106.15933
 },
 {
   "STT": 748,
   "Name": "Quầy thuốc số 50",
   "address": "Thôn Trần Xá, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.209834,
   "Latitude": 105.986279
 },
 {
   "STT": 749,
   "Name": "Quầy thuốc Bích Liên",
   "address": "Thôn Đa Cấu, xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1424416,
   "Latitude": 106.1183999
 },
 {
   "STT": 750,
   "Name": "Quầy thuốc Anh Thư",
   "address": "Số 350 phố Đông Côi, Thị trấn Hồ, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0378323,
   "Latitude": 106.0919988
 },
 {
   "STT": 751,
   "Name": "Quầy thuốc Nguyễn Thị Ngọc",
   "address": "Thôn Hương Mạc, xã Hương Mạc, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1584646,
   "Latitude": 105.9376756
 },
 {
   "STT": 752,
   "Name": "Quầy thuốc Minh Long",
   "address": "Thôn Đa Cấu, xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1424416,
   "Latitude": 106.1183999
 },
 {
   "STT": 753,
   "Name": "Quầy thuốc Bộ Hà",
   "address": "Thôn Đại Lâm, xã Tam Đa, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2106744,
   "Latitude": 106.0346163
 },
 {
   "STT": 754,
   "Name": "Nhà thuốc Hồng Vân",
   "address": "Số 21 Nguyễn Trãi, Phường  Ninh Xá, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1776116,
   "Latitude": 106.0594302
 },
 {
   "STT": 755,
   "Name": "Nhà thuốc Thuỳ Linh",
   "address": "Khu 10 Phường  Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1765686,
   "Latitude": 106.0732663
 },
 {
   "STT": 756,
   "Name": "Nhà thuốc Thành Trung",
   "address": "Số 10 Đường Đấu Mã, Phường  Thị Cầu, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.190104,
   "Latitude": 106.086368
 },
 {
   "STT": 757,
   "Name": "Quầy thuốc Hương Hoàng",
   "address": "Thôn Đông, xã Hoàn Sơn, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1032348,
   "Latitude": 106.0074474
 },
 {
   "STT": 758,
   "Name": "Quầy thuốc Tâm Phúc",
   "address": "Thôn Phong Xá, xã Đông Phong, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1956876,
   "Latitude": 106.0150623
 },
 {
   "STT": 759,
   "Name": "Quầy thuốc số 2",
   "address": "Số 16 Đường Bình Than, Phường  Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1728813,
   "Latitude": 106.074447
 },
 {
   "STT": 760,
   "Name": "Quầy thuốc số 1",
   "address": "Số 11, Đường Hai Bà Trưng, Thị trấn LiM, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1461626,
   "Latitude": 106.0233558
 },
 {
   "STT": 761,
   "Name": "Nhà thuốc Bảo Khánh",
   "address": "Số 51B Đường Bà Chúa Kho, Phường  Vũ Ninh, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.2058178,
   "Latitude": 106.0844887
 },
 {
   "STT": 762,
   "Name": "Quầy thuốc Thanh Loan",
   "address": "Thôn Mao Dộc, xã Phượng Mao, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1543426,
   "Latitude": 106.1367842
 },
 {
   "STT": 763,
   "Name": "Quầy thuốc Đức Duy",
   "address": "Thôn Giáo, xã Tri Phương, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.0816438,
   "Latitude": 106.006533
 },
 {
   "STT": 764,
   "Name": "Nhà tuốc số 4",
   "address": "Khu Chu Mẫu, Phường  Vân Dương, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1649355,
   "Latitude": 106.0964516
 },
 {
   "STT": 765,
   "Name": "Nhà thuốc Dũng Hương",
   "address": "Khu 10, Phường  Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1765686,
   "Latitude": 106.0732663
 },
 {
   "STT": 766,
   "Name": "Nhà tthuốc phòng khám đa khoa Tâm Đức",
   "address": "Khu Xuân Đài, Phường  Đình Bảng, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.0962237,
   "Latitude": 105.9523622
 },
 {
   "STT": 767,
   "Name": "Quầy thuốc tư nhân Minh Đức",
   "address": "Thôn Giang Liễu, xã Phương Liễu, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1582245,
   "Latitude": 106.1303886
 },
 {
   "STT": 768,
   "Name": "Quầy thuốc Huyền Quang",
   "address": "Thôn Giang Liễu, xã Phương Liễu, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1582245,
   "Latitude": 106.1303886
 },
 {
   "STT": 769,
   "Name": "Nhà thuốc Minh Linh",
   "address": "Tâng 1, toà nhà Cát Tường, Đường Lê Thái Tổ, Phường  Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1673393,
   "Latitude": 106.0537881
 },
 {
   "STT": 770,
   "Name": "Quầy thuốc Nguyên Hồng",
   "address": "Thôn Đoài, xã Hoàn Sơn, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1065555,
   "Latitude": 106.00304
 },
 {
   "STT": 771,
   "Name": "Quầy thuốc Nguyễn Phương Thảo",
   "address": "Làng Bất Lự, xã Hoàn Sơn, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1128264,
   "Latitude": 105.9905529
 },
 {
   "STT": 772,
   "Name": "Quầy thuốc số 33 ",
   "address": "Thôn Mẫn Xá, xã Long Châu, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1947753,
   "Latitude": 105.98912
 },
 {
   "STT": 773,
   "Name": "Quầy thuốc Minh Hiếu",
   "address": "Thôn Ấp Đồn, xã Yên Trung, Yên Phong, Bắc Ninh",
   "Longtitude": 21.2062766,
   "Latitude": 105.9984151
 },
 {
   "STT": 774,
   "Name": "Quầy thuốc Quỳnh Lịch",
   "address": "Chợ chiều, xã Văn Môn, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1692462,
   "Latitude": 105.92906
 },
 {
   "STT": 775,
   "Name": "Quầy thuốc Thuyết Bắc",
   "address": "Thôn Ô Cách, xã Đông Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 776,
   "Name": "Quầy thuốc số 3",
   "address": "Thôn Ô Cách, xã Đông Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 777,
   "Name": "Nhà thuốc phòng khám đa khoa Thành Đô",
   "address": "Số 248, Đường Trần Hưng Đạo, Phường  Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1790489,
   "Latitude": 106.0687277
 },
 {
   "STT": 778,
   "Name": "Quầy thuốc Linh Giang 2",
   "address": "Thôn Yên Lãng, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc nInh",
   "Longtitude": 21.2092511,
   "Latitude": 105.9936476
 },
 {
   "STT": 779,
   "Name": "Quầy thuốc Linh Giang 3",
   "address": "Thôn Trần Xá, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.209834,
   "Latitude": 105.986279
 },
 {
   "STT": 780,
   "Name": "Nhà thuốc trực thuộc công ty cổ phần siêu thị thuốc Viêt",
   "address": "Số 245 Trần Phú, Phường  Đông Ngàn, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1172268,
   "Latitude": 105.9584785
 },
 {
   "STT": 781,
   "Name": "Quầy thuốc Lan Anh",
   "address": "Thôn Đình Cả, Nội Duệ, Tiên Du, Bắc Ninh",
   "Longtitude": 21.1355376,
   "Latitude": 106.0071308
 },
 {
   "STT": 782,
   "Name": "Quầy thuốc Thảo My",
   "address": "Thôn Chi Long, xã Long Châu, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1941429,
   "Latitude": 105.9582372
 },
 {
   "STT": 783,
   "Name": "Quầy thuốc Thuyỳ Linh",
   "address": "Thôn Do Nha, xã Phương Liễu, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1732871,
   "Latitude": 106.1079848
 },
 {
   "STT": 784,
   "Name": "Quầy thuốc Gia Bảo",
   "address": "Thôn Giang Liễu, xã Phương Liễu, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1582245,
   "Latitude": 106.1303886
 },
 {
   "STT": 785,
   "Name": "Quầy thuốc Tường Lan",
   "address": "Ngã tư đại Thượng, xã Đại Đồng, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.0898656,
   "Latitude": 105.9887552
 },
 {
   "STT": 786,
   "Name": "Quầy thuốc Nguyêễn Hữu Trực",
   "address": "Thôn Từ Phong, xã Cách Bi, huyên Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1345006,
   "Latitude": 106.1897922
 },
 {
   "STT": 787,
   "Name": "Quầy thuốc Nguyễn Thị Huyền",
   "address": "Thôn Quế Ổ, xã Chi Lăng, Huyện Quế Võ , Bắc Ninh",
   "Longtitude": 21.0962004,
   "Latitude": 106.1343867
 },
 {
   "STT": 788,
   "Name": "Quầy thuốc Vũ Thị Hiền",
   "address": "Thôn Tu Phong, xa Cach Bi, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1330297,
   "Latitude": 106.18716
 },
 {
   "STT": 789,
   "Name": "Quầy thuốc Hà Thị Ngọc",
   "address": "Khu 3, Thị trấn Phố Mới, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1491141,
   "Latitude": 106.1551809
 },
 {
   "STT": 790,
   "Name": "Quầy thuốc Hằng Hải",
   "address": "Thôn Doãn Thượng, xã Xuân Lâm, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0127072,
   "Latitude": 106.012305
 },
 {
   "STT": 791,
   "Name": "Quầy thuốc Thái Khang",
   "address": "Thôn Đa Tiện, xã Xuân Lâm, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0208191,
   "Latitude": 106.0115495
 },
 {
   "STT": 792,
   "Name": "Nhà thuốc Hồng Duyên",
   "address": "Số 41A khu 7, Phường  Thị Cầu, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1700796,
   "Latitude": 106.0834908
 },
 {
   "STT": 793,
   "Name": "Nhà thuốc Hưng Liễu",
   "address": "Số 239 Lý Thường Kiệt, Phường  Thị Cầu, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.2042564,
   "Latitude": 106.0908266
 },
 {
   "STT": 794,
   "Name": "Quầy thuốc Hồng Ngọc",
   "address": "Thôn Trần Xá, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.209834,
   "Latitude": 105.986279
 },
 {
   "STT": 795,
   "Name": "Quầy thuốc Hương Giang",
   "address": "Thôn Ấp Đồn, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.205393,
   "Latitude": 105.998216
 },
 {
   "STT": 796,
   "Name": "Quầy thuốc Mão Điền",
   "address": "Xóm Bàng, xã Mão Điền, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0636761,
   "Latitude": 106.1217707
 },
 {
   "STT": 797,
   "Name": "Nhà thuốc Quyền Hằng",
   "address": "Đường Hoàng Tích Trí, khu Phúc Sơn, Phường  Vũ Ninh, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1921366,
   "Latitude": 106.0731889
 },
 {
   "STT": 798,
   "Name": "Nhà thuốc PHARMILY",
   "address": "Lô 9, khu phố Xuân Thụ, Phường  Đông Ngàn, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1237614,
   "Latitude": 105.970748
 },
 {
   "STT": 799,
   "Name": "Quầy thuốc Đỗ Thị Dung",
   "address": "Thôn An Ninh, xã Yên Phụ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2023365,
   "Latitude": 105.925047
 },
 {
   "STT": 800,
   "Name": "Quầy thuốc Cường Thuỷ",
   "address": "Ki oốt số 7, chợ Nghiêm Xá, Thị trấn Chờ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1872844,
   "Latitude": 105.9552809
 },
 {
   "STT": 801,
   "Name": "Quầy thuốc Đức Ngọc",
   "address": "Thôn Nguyệt Cầu, xã Tam Giang, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1941429,
   "Latitude": 105.9582372
 },
 {
   "STT": 802,
   "Name": "Quầy thuốc Hải Thơm",
   "address": "Thôn Trần Xá, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.209834,
   "Latitude": 105.986279
 },
 {
   "STT": 803,
   "Name": "Quầy thuốc Cường Hà",
   "address": "Số 274 phố mới, Thị trấn Chờ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1978889,
   "Latitude": 105.9494248
 },
 {
   "STT": 804,
   "Name": "Nhà thuốc Bằng Ngần",
   "address": "Khu phố Đa Hội, Phường  Châu Khê, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.124253,
   "Latitude": 105.9220297
 },
 {
   "STT": 805,
   "Name": "Quầy thuốc Hương Đông",
   "address": "Thôn Trần Xá, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.209834,
   "Latitude": 105.986279
 },
 {
   "STT": 806,
   "Name": "Quầy thuốc số 49",
   "address": "Thôn Chi Long, xã Long Châu, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1941429,
   "Latitude": 105.9582372
 },
 {
   "STT": 807,
   "Name": "Quầy thuốc phòng khám KCN Yên Phong",
   "address": "Lô CN 20, KCN Yên Phong, xã Long Châu, Huyện Yên Phong, Tỉnh Bắc NInh",
   "Longtitude": 21.1923746,
   "Latitude": 106.0054761
 },
 {
   "STT": 808,
   "Name": "Quầy thuốc Tân Dược",
   "address": "Thôn Đại Trung, xã Đại Đồng, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.0837543,
   "Latitude": 105.991829
 },
 {
   "STT": 809,
   "Name": "Quầy thuốc Vạn Bảo Tín",
   "address": "Số 91 Đường Lý Thường Kiệt, Thị trấn Lim, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.144745,
   "Latitude": 106.0199355
 },
 {
   "STT": 810,
   "Name": "Quầy thuốc Thu Hương",
   "address": "Thôn Đông Thái, xã Đông Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 811,
   "Name": "Quầy thuốc Tỉnh Điền",
   "address": "Thôn Lạc Trung, xã Dũng Liệt, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2428084,
   "Latitude": 105.9922976
 },
 {
   "STT": 812,
   "Name": "Nhà thuốc Hồ Trang",
   "address": "Khu Lãm Làng, Phường  Vân Dương, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1509203,
   "Latitude": 106.1037362
 },
 {
   "STT": 813,
   "Name": "Quầy thuốc Minh Thư",
   "address": "Thôn Mao Dộc, xã Phượng Mao, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1543426,
   "Latitude": 106.1367842
 },
 {
   "STT": 814,
   "Name": "Quầy thuốc Thuỳ Trang",
   "address": "Thôn Mao Dộc, xã Phượng Mao, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1543426,
   "Latitude": 106.1367842
 },
 {
   "STT": 815,
   "Name": "Nhà thuốc Thu Huyền",
   "address": "Số 43 Lý Anh Tông, khu Hoà Đình, Phường  Võ Cường, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1717027,
   "Latitude": 106.0519895
 },
 {
   "STT": 816,
   "Name": "Quầy thuốc Minh Quân",
   "address": "Thôn Cung Kiệm, xã Nhân Hoà, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1810721,
   "Latitude": 106.142059
 },
 {
   "STT": 817,
   "Name": "Quầy thuốc Đăng Khoa",
   "address": "Xóm Dư, thôn Phù Lộc, xã Phù Chẩn, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.0943639,
   "Latitude": 105.9683121
 },
 {
   "STT": 818,
   "Name": "Nhà thuốc Hồng Tiệp",
   "address": "Khu Lãm Làng, Phường  Vân Dương, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1509203,
   "Latitude": 106.1037362
 },
 {
   "STT": 819,
   "Name": "Nhà thuốc Tuyết Mai",
   "address": "Khu Lãm Làng, Phường  Vân Dương, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1509203,
   "Latitude": 106.1037362
 },
 {
   "STT": 820,
   "Name": "Nhà thuốc Tiến Ngọc",
   "address": "Khu phố Thanh Bình, Phường  Đồng Kỵ, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1389299,
   "Latitude": 105.9450188
 },
 {
   "STT": 821,
   "Name": "Nhà thuốc Thu Hiền",
   "address": "Xóm 5, Châm Khê,Phường  Phong Khê, Thành phố Bắc Ninh, Bắc Ninh",
   "Longtitude": 21.1796648,
   "Latitude": 106.0331592
 },
 {
   "STT": 822,
   "Name": "Nhà thuốc Sao Kinh Bắc",
   "address": "Số nhà 9,  lô 14 KCN Tân Hồng, Hoàn Sơn, Phường   Tân Hồng, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1132376,
   "Latitude": 105.979979
 },
 {
   "STT": 823,
   "Name": "Quầy thuốc Ngọc Oanh",
   "address": "Thôn Thái Bảo, xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1455414,
   "Latitude": 106.1088452
 },
 {
   "STT": 824,
   "Name": "Quầy thuốc Trần Thoa",
   "address": "Thôn Quả Cảm, xã Hoà Long, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.2081087,
   "Latitude": 106.0510366
 },
 {
   "STT": 825,
   "Name": "Quầy thuốc Phương Anh",
   "address": "Phố Khám, Gia Đông, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0371315,
   "Latitude": 106.0857822
 },
 {
   "STT": 826,
   "Name": "Quầy thuốc Tâm Đức",
   "address": "Chợ Ấp Đồn, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2068977,
   "Latitude": 105.9986327
 },
 {
   "STT": 827,
   "Name": "Quầy thuốc Phúc Hậu 2",
   "address": "Thôn Trần Xá, xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.209834,
   "Latitude": 105.986279
 },
 {
   "STT": 828,
   "Name": "Quầy thuốc tư nhân Ngọc Quyên",
   "address": "Thôn Do Nha, xã Phương Liễu, Huyện Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1732871,
   "Latitude": 106.1079848
 },
 {
   "STT": 829,
   "Name": "Quầy thuốc Thu Thuỷ",
   "address": "Thôn Mao Dộc, xã Phượng Mao, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1543426,
   "Latitude": 106.1367842
 },
 {
   "STT": 830,
   "Name": "Quầy thuốc Hà Chi ",
   "address": "Thôn Đông Yên, xã Đông Phong, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1891678,
   "Latitude": 106.0061096
 },
 {
   "STT": 831,
   "Name": "Quầy thuốc Phúc Hậu",
   "address": "Thôn Ô Cách, xã Đông Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 832,
   "Name": "Quầy thuốc Quảng Dương",
   "address": "Thôn Găng, xã Đào Viên, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1151085,
   "Latitude": 106.2132371
 },
 {
   "STT": 833,
   "Name": "Nhà thuốc Tâm An",
   "address": "Số 472 Đường 286, Phường  Vạn An, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1880885,
   "Latitude": 106.0579386
 },
 {
   "STT": 834,
   "Name": "Nhà thuốc Nhân Dân",
   "address": "Chợ Cóc, khu 10, Phường  Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1757306,
   "Latitude": 106.0709139
 },
 {
   "STT": 835,
   "Name": "Nhà thuốc 181",
   "address": "Số 181, Lý Thái Tông, Phường  Suối Hoa, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1810054,
   "Latitude": 106.0756105
 },
 {
   "STT": 836,
   "Name": "Quầy thuốc Lê Phương",
   "address": "Thôn Do Nha, xã Phương Liễu, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1732871,
   "Latitude": 106.1079848
 },
 {
   "STT": 837,
   "Name": "Cơ Sở Chuyên Bán Lẻ Dược Liệu, thuốc Dược Liệu, thuốc Cổ Truyền",
   "address": "Số 70, Thị trấn Thứa, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0222876,
   "Latitude": 106.2080897
 },
 {
   "STT": 838,
   "Name": "Quầy thuốc Hà Anh",
   "address": "Số 139 phố Hồ, Thị trấn Hồ, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0637235,
   "Latitude": 106.0867977
 },
 {
   "STT": 839,
   "Name": "Quầy thuốc Thu Hiền",
   "address": "Thôn Chi Long, xã Long Châu, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1941429,
   "Latitude": 105.9582372
 },
 {
   "STT": 840,
   "Name": "Quầy thuốc Thuận Tâm",
   "address": "Thôn Chi Long, xã Long Châu, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1941429,
   "Latitude": 105.9582372
 },
 {
   "STT": 841,
   "Name": "Quầy thuốc Long Lịch",
   "address": "Phố Chờ, Thị trấn Chờ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1978889,
   "Latitude": 105.9494248
 },
 {
   "STT": 842,
   "Name": "Nhà thuốc Hoàng Dung",
   "address": "Khu Trà Xuyên, Phường  Khúc Xuyên, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1830123,
   "Latitude": 106.0434449
 },
 {
   "STT": 843,
   "Name": "Quầy thuốc Trạm y tế xã Tương Giang",
   "address": "Trạm Y tế xã Tương Giang, thôn Tiêu Long, xã Tương Giang, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1369323,
   "Latitude": 105.9883494
 },
 {
   "STT": 844,
   "Name": "Quầy thuốc Trạm y tế xã Tam Sơn",
   "address": "Trạm Y tế xã Tam Sơn, thôn Tam Sơn, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.1446421,
   "Latitude": 105.9746519
 },
 {
   "STT": 845,
   "Name": "Quầy thuốc Trạm y tế xã Phù Chẩn",
   "address": "Trạm Y tế xã Phù Chẩn, thôn Doi Sóc, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.0943411,
   "Latitude": 105.9694004
 },
 {
   "STT": 846,
   "Name": "Quầy thuốc trạm y tế xã Phù Khê",
   "address": "Trạm Y tế xã Phù Khê, thôn Đông, xã Phù Khê, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.141562,
   "Latitude": 105.9352948
 },
 {
   "STT": 847,
   "Name": "Nhf thuốc Vinh Hà",
   "address": "Thôn Đại Tự, xã Thanh Khương, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0437995,
   "Latitude": 106.0757749
 },
 {
   "STT": 848,
   "Name": "Nhà thuốc Vietcare",
   "address": "Số 1, Đường Bình Than, Phường  Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1759293,
   "Latitude": 106.0735345
 },
 {
   "STT": 849,
   "Name": "Nhà thuốc Hải Vân",
   "address": "Khu Thượng, Phường  Khắc Niệm, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1318446,
   "Latitude": 106.0580066
 },
 {
   "STT": 850,
   "Name": "Quầy thuốc Thu Huong",
   "address": "Phố Khám, Gia Đông, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0371315,
   "Latitude": 106.0857822
 },
 {
   "STT": 851,
   "Name": "Quầy thuốc Hồng Phúc",
   "address": "Thôn Đa Cấu, xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1424416,
   "Latitude": 106.1183999
 },
 {
   "STT": 852,
   "Name": "Quầy thuoốc Minh Ngọc",
   "address": "Thôn Thái Bảo, xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1455414,
   "Latitude": 106.1088452
 },
 {
   "STT": 853,
   "Name": "Quầy thuốc Hoa Tuyết",
   "address": "Chợ Bựu, xã Liên Bão. Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1292205,
   "Latitude": 106.0266387
 },
 {
   "STT": 854,
   "Name": "Quầy thuốc tân dược Chợ Sơn số 20",
   "address": "Chợ Sơn, xã Việt Đoàn, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1096804,
   "Latitude": 106.0366384
 },
 {
   "STT": 855,
   "Name": "Quầy thuốc tư nhân Thu Thương",
   "address": "Thôn Đại Sơn, xã Hoàn Sơn, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1085696,
   "Latitude": 105.9971636
 },
 {
   "STT": 856,
   "Name": "Quầy thuốc Âu Việt",
   "address": "Thôn Đại Thượng, xã Đại Đồng, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.0837543,
   "Latitude": 105.991829
 },
 {
   "STT": 857,
   "Name": "Quầy thuốc Hương Anh",
   "address": "Thôn Dương Húc, xã Đại Đồng, Huyện Tiên Du, Tỉnh Bắc Ninh ",
   "Longtitude": 21.0963721,
   "Latitude": 105.9875268
 },
 {
   "STT": 858,
   "Name": "Quầy thuốc  PKĐK Thuận An",
   "address": "Thôn Cầu Đào, xã Nhân Thắng, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0695235,
   "Latitude": 106.2322569
 },
 {
   "STT": 859,
   "Name": "Quầy thuốc Hiền Tươi",
   "address": "Thôn Trừng Xá, xã Trừng Xá, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.02427,
   "Latitude": 106.2397114
 },
 {
   "STT": 860,
   "Name": "Nhà thuốc Hương Duyên",
   "address": "Khu Lãm Trại, Phường  Vân Dương, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1540096,
   "Latitude": 106.0937449
 },
 {
   "STT": 861,
   "Name": "Nhà thuốc số 1 Bình Hằng",
   "address": "Khu Lãm Trại, Phường  Vân Dương, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1540096,
   "Latitude": 106.0937449
 },
 {
   "STT": 862,
   "Name": "Quầy thuốc Hà Phương",
   "address": "Đội 2, thôn Viêm Xá, xã Hòa Long, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1644992,
   "Latitude": 106.0552006
 },
 {
   "STT": 863,
   "Name": "Nhà thuốc Nhân Dân",
   "address": "Số 1, khu phố Trang Liệt, Phường  Trang Hạ, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1288596,
   "Latitude": 105.9519986
 },
 {
   "STT": 864,
   "Name": "Quầy thuốc Hạnh Phúc",
   "address": "Thôn Mao Dộc, xã Phượng Mao, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1543426,
   "Latitude": 106.1367842
 },
 {
   "STT": 865,
   "Name": "Quầy thuốc Bình Mai",
   "address": "Thôn Sơn Đông, xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.141189,
   "Latitude": 106.0930395
 },
 {
   "STT": 866,
   "Name": "Quầy thuốc Tiến Bào",
   "address": "Thôn Tiến Bào, xã Phù Khê, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1419508,
   "Latitude": 105.9333616
 },
 {
   "STT": 867,
   "Name": "Quầy thuốc Trạm Y tế Hương Mạc",
   "address": "Thôn Kim Thiều, xã Hương Mạc, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1539654,
   "Latitude": 105.924839
 },
 {
   "STT": 868,
   "Name": "Quầy thuốc Thắng Quỳnh",
   "address": "Chợ Đình Giỏ, xã Tam Sơn, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.144054,
   "Latitude": 105.9770751
 },
 {
   "STT": 869,
   "Name": "Nhà thuốc Hiên Vân",
   "address": "Phoố Kiều, xã Hiên Vân, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1178311,
   "Latitude": 106.0287512
 },
 {
   "STT": 870,
   "Name": "Quầy thuốc Bích Ngọc",
   "address": "Thôn Phù Lộc, xã Phù Chẩn, Thị xã Từ Sơn, Bắc Ninh",
   "Longtitude": 21.094228,
   "Latitude": 105.969191
 },
 {
   "STT": 871,
   "Name": "Nhà thuốc Hữu Phúc",
   "address": "Phố DĐông Yên, xã Đông Phong, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 872,
   "Name": "Nhà thuốc Trung Thành THT",
   "address": "Khu phố Tân Thành, Phường  Đồng Kỵ, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1117507,
   "Latitude": 105.9874778
 },
 {
   "STT": 873,
   "Name": "Quầy thuốc Gia Linh",
   "address": "Đương 24, Mao Trung, Phượng Mao, Huyện Quế Võ, Bắc Ninh",
   "Longtitude": 21.1504392,
   "Latitude": 106.1448601
 },
 {
   "STT": 874,
   "Name": "Quầy thuốc Hà Vy",
   "address": "Thôn Viêm Xá, xã Hòa Long, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1817317,
   "Latitude": 106.075732
 },
 {
   "STT": 875,
   "Name": "Nhà thuốc An Phát",
   "address": "Số 64, Lý Anh Tông, Phường  Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1697116,
   "Latitude": 106.0537262
 },
 {
   "STT": 876,
   "Name": "Quầy thuốc Tâm An",
   "address": "Thôn Giang Liễu, xã Phương Liễu, Huyện Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1582245,
   "Latitude": 106.1303886
 },
 {
   "STT": 877,
   "Name": "Nhà thuốc Hưng Anh",
   "address": "Khu Laãm Trại, Phường  Vân Dương, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1540096,
   "Latitude": 106.0937449
 },
 {
   "STT": 878,
   "Name": "Nhà thuốc Bắc Ninh",
   "address": "Số 513 Ngô Gia Tự, Phường  Tiền An, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.180925,
   "Latitude": 106.0628559
 },
 {
   "STT": 879,
   "Name": "Quầy thuốc Nguyêễn Thị Giang",
   "address": "Ấp Đông Côi, Thị trấn Hồ, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0404213,
   "Latitude": 106.0882675
 },
 {
   "STT": 880,
   "Name": "Quầy thuốc Nguyễn Thị Thu",
   "address": "Thôn Liễu Lâm, xã Song Liễu, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0011888,
   "Latitude": 106.0098269
 },
 {
   "STT": 881,
   "Name": "Quầy thuốc số 16 ",
   "address": "Thôn Đông Ngoại, xã Nghĩa Đạo, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0021287,
   "Latitude": 106.1205521
 },
 {
   "STT": 882,
   "Name": "Quầy thuốc Nguyễn Thị Hòa",
   "address": "Ki ốt 19 chợ trung tâm, thgij trấn Hồ, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0588959,
   "Latitude": 106.0901749
 },
 {
   "STT": 883,
   "Name": "Quầy thuốc Phưng Tú",
   "address": "Số 207 Phố Hồ, Thị trấn Hồ, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.05191,
   "Latitude": 106.0902316
 },
 {
   "STT": 884,
   "Name": "Quầy thuốc số 1",
   "address": "Số 20 Phố Hồ, Thị trấn Hồ, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0651539,
   "Latitude": 106.0847807
 },
 {
   "STT": 885,
   "Name": "Quầy thuốc Hà Thị Lương",
   "address": "Thôn Á Lữ, xã Đại Đồng, Huyện Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.069083,
   "Latitude": 106.04712
 },
 {
   "STT": 886,
   "Name": "Quầy thuốc Hiền Anh",
   "address": "Số 280, Đường Hàn Thuyên, Thị trấn Thứa, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0246575,
   "Latitude": 106.2112105
 },
 {
   "STT": 887,
   "Name": "Quầy số 15",
   "address": "Số 63 Vũ Giới, Thị trấn Thứa, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0169135,
   "Latitude": 106.2040727
 }
];